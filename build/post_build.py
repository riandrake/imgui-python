import glob
import os
import shutil

if os.path.exists('../bin/ImGuiPython/ImGui.pyd'):
    for file_path in glob.glob('../Tools/Apps/*'):
        if os.path.isdir(file_path):
            print('Copying ImGui.pyd to: ' + file_path)
            shutil.copy('../bin/ImGuiPython/ImGui.pyd', file_path + '/ImGui.pyd')

