#pragma once

#include <functional>
#include <memory>
#include <stddef.h>

class Finally
{
public:
	using Callback_t = std::function<void()>;

	Finally( Callback_t callback = nullptr ) : callback( callback ) {}
	~Finally() { if( callback ) callback(); }

	explicit operator bool() { return callback != nullptr; }

private:
	Callback_t callback;
};

inline std::unique_ptr<Finally> make_finally( Finally::Callback_t callback )
{
	return std::make_unique<Finally>( callback );
}