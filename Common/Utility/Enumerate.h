#pragma once


template<class Container>
class enumerate
{
public:
	enumerate( Container& container ) :container( container ) {}

	class Iterator
	{
	public:
		Iterator( Container& container, const unsigned idx )
			: container( container ), idx( idx ) {}

		auto operator*()
		{
			if constexpr( std::is_const_v<Container> )
				return std::pair<unsigned, const typename Container::value_type&>( idx, container[idx] );
			else
				return std::pair<unsigned, typename Container::value_type&>( idx, container[idx] );
		}

		bool operator!=( const Iterator& other ) const { return idx != other.idx; }
		void operator++() { idx += 1; }

		Container& container;
		unsigned idx = 0;
	};

	Iterator begin() const { return Iterator( container, 0 ); }
	Iterator end() const { return Iterator( container, container.size() ); }

	Container& container;
};