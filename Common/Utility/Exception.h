#pragma once

#include <stdexcept>
#include <string>

class RuntimeError : public std::runtime_error
{
public:
	RuntimeError( const char* format, ... );

	const char* what() const;

private:
	std::string error;
};