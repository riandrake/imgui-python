#pragma once

#include <Common/Include/fmt/format.h>

#define SPDLOG_FMT_EXTERNAL
#include <Common/Include/spdlog/spdlog.h>

class Log

{
public:
	Log( const char* program_name );
	~Log();

	template< typename... Args> static void Trace( const char* format, const Args& ...args ) { if( logger ) logger->   trace( format, args... ); }
	template< typename... Args> static void Debug( const char* format, const Args& ...args ) { if( logger ) logger->   debug( format, args... ); }
	template< typename... Args> static void  Info( const char* format, const Args& ...args ) { if( logger ) logger->    info( format, args... ); }
	template< typename... Args> static void  Warn( const char* format, const Args& ...args ) { if( logger ) logger->    warn( format, args... ); }
	template< typename... Args> static void Error( const char* format, const Args& ...args ) { if( logger ) logger->   error( format, args... ); }
	template< typename... Args> static void  Crit( const char* format, const Args& ...args ) { if( logger ) logger->critical( format, args... ); }

private:
	inline static std::shared_ptr<spdlog::logger> logger = nullptr;
};