#pragma once

#ifndef NOMINMAX
#	define NOMINMAX
#endif

#include <windows.h>
#include <string>

namespace File
{
	std::string OpenFileDialog( bool must_exist = false, HWND window = nullptr, const std::string& title = "Open File", const std::string& filter = "Text Files\0*.txt\0Any File\0*.*\0" );
}