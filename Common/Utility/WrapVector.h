#pragma once

#include <vector>

template<typename T>
class WrapVector;

template<typename T>
struct WrapIterator
{
    const WrapVector<T>& p;
	unsigned i;

	WrapIterator( const WrapVector<T>* p, unsigned i ) : p( *p ), i( i ) {}
	bool operator!=( WrapIterator rhs ) { return i != rhs.i; }
	const T& operator*() { return p[i]; }
	void operator++() { ++i; }
};

template<typename T>
class WrapVector
{
public:
	WrapVector( const unsigned max )
		: max( max )
	{
		container.reserve( max );
	}

	template <class... _Valty>
    void emplace_back(_Valty&&... _Val)
	{
		if( container.size() < max )
			container.emplace_back( _STD forward<_Valty>( _Val )... );
		else
		{
			container[offset % max] = { _STD forward<_Valty>( _Val )... };
			offset++;
		}
    }

    auto begin() const noexcept { return WrapIterator<T>( this, offset ); }
    auto end() const noexcept { return WrapIterator<T>( this, offset + size() ); }

	void clear() {
		offset = 0;
		container.clear();
	}

	auto size() const { return container.size(); }

	const auto& operator[]( const unsigned i ) const { return container[( i + offset ) % max]; }

private:
	std::vector<T> container;
	unsigned offset = 0;
	unsigned max;
};