#include "Exception.h"

#include <stdarg.h>

#include <fmt/format.h>

RuntimeError::RuntimeError( const char* format, ... )
	: std::runtime_error( nullptr )
{
    va_list args;
	va_start( args, format );
	error = fmt::format( format, args );
	va_end( args );
}

const char* RuntimeError::what() const
{
	return error.c_str();
}