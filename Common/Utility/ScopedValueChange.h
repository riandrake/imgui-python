#pragma once

template<typename T>
class ScopedValueChange
{
public:
	ScopedValueChange( T& val_ref, const T& val_temp )
		: val_ref( val_ref ), val_backup( val_ref )
	{
		val_ref = val_temp;
	}

	~ScopedValueChange()
	{
		val_ref = val_backup;
	}

	explicit operator bool() const { return true; }

private:
	T& val_ref;
	const T val_backup;
};