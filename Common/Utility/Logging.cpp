#include "Logging.h"

#include <filesystem>

#include <spdlog/sinks/basic_file_sink.h>

Log::Log( const char* program_name )
{
	assert( !logger );
	if( logger )
	{
		Error( "Tried to create another log instance!" );
		return;
	}

	std::filesystem::create_directory( "logs" );
	logger = spdlog::basic_logger_mt( program_name, fmt::format( "logs/{}.txt", program_name ) );

	spdlog::set_pattern( "[%^+++%$] [%H:%M:%S %z] [thread %t] %v" );

#ifdef _DEBUG
	spdlog::set_level( spdlog::level::debug ); //Set global log level to info
#else
	spdlog::set_level( spdlog::level::info ); //Set global log level to info
#endif

	Debug( "-- Logging Started --" );
}

Log::~Log()
{
	Debug( "-- Logging Finished --" );
	spdlog::drop_all();
	logger = nullptr;
}