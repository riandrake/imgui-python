#include "FileIO.h"

namespace File
{
	std::string File::OpenFileDialog( bool must_exist, HWND window, const std::string& title, const std::string& filter )
	{
		char filename[MAX_PATH];

		OPENFILENAMEA ofn;
		ZeroMemory( &filename, sizeof( filename ) );
		ZeroMemory( &ofn, sizeof( ofn ) );
		ofn.lStructSize = sizeof( ofn );
		ofn.hwndOwner = window;
		ofn.lpstrFilter = filter.c_str();
		ofn.lpstrFile = filename;
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrTitle = title.c_str();
		ofn.Flags = OFN_DONTADDTORECENT | ( must_exist ? OFN_FILEMUSTEXIST : 0 );

		if( !GetOpenFileNameA( &ofn ) )
			return "";

		return filename;
	}
}