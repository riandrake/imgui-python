#pragma once

#include <Windows.h>

#include "BaseApp.h"

namespace Main
{
	int WINAPI DoMain( App::Base& app, HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int showCmd, bool show_window );
}
