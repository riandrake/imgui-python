#pragma once

#include "BaseApp.h"

#include <Common/ImGui/Modules/ImGuiModule.h>

#ifndef _WINDLL
PYBIND11_EMBEDDED_MODULE( ImGui, mod )
{
	Python::Modules::ImGuiModule::Initialise( mod );
}
#endif

namespace App
{
	void Base::Initialise( const std::string_view& args )
	{
		OnInitialise( args );
	}

	void Base::PostInitialise()
	{
		OnPostInitialise();
	}

	void Base::Destroy()
	{
		OnDestroy();
	}

	void Base::Render()
	{
		OnRender();
	}

	void Base::FrameMove( const float elapsed_time )
	{
		OnFrameMove( elapsed_time );
	}
}