#pragma once

#include <Windows.h>
#include <string>

namespace App
{
	class Base
	{
	public:
		void Initialise( const std::string_view& args );
		void PostInitialise();
		void Destroy();
		void Render();
		void FrameMove( const float elapsed_time );

		bool IsOpen() const { return open; }

		virtual const char* ProgramName() const { return "ImGuiPythonProgram"; }
		virtual const char* WindowName() const { return "ImGuiPythonApplication"; }

		virtual bool MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) { return false; }
		virtual void OnInitialise( const std::string_view& args ) {}
		virtual void OnPostInitialise() {}
		virtual void OnDestroy() {}
		virtual void OnFrameMove( const float elapsed_time ) {}
		virtual void OnRender() {}

		void Close() { open = false; } 

		bool open = true;
	};
}