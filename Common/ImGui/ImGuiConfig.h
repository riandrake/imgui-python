#pragma once

#include <cmath>

#define IMGUI_DEFINE_MATH_OPERATORS

#define IM_VEC2_CLASS_EXTRA \
	float    dot( const ImVec2& other ) const { return other.x * x + other.y * y; } \
	float    sqrlen() const { return  dot(*this); } \
	float    len() const { return std::sqrtf( sqrlen() );  } \
	ImVec2   normalized() const { const float l = len(); return ImVec2( x / l, y / l ); }