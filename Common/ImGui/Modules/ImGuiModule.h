#pragma once

#include <Common/Python/Module.h>

namespace Python::Modules::ImGuiModule
{
	void Initialise( pybind11::module& mod );
}