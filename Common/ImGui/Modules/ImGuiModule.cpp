#include "ImGuiModule.h"

#include "Common/ImGui/ImGuiEx.h"

#include <sstream>
#include <stdarg.h>
#include <tuple>

using std::function;
using std::string;

#define DOC( ... )

namespace Python::Modules
{
	/*
		brief = "Starts a new ImGui window."
		return = "Returns 'True' if you should render the window, do not otherwise."

		[parameters]
			name = "The name of the window, and the unique hash used to identify the window"
			on_close = "Event signaling the window should close. When specified, a 'X' button is added to the window allowing the user to close it, triggering this event."
			flags = "Modify the window behaviour by piping together flag values with the '|' operator."
	*/
	GENERATE bool Begin( const string& name, function<void()> on_close = nullptr,
		const ImGuiWindowFlags flags = 0 )
	{
		if( on_close == nullptr )
			return ImGui::Begin( name.c_str(), nullptr, flags );

		bool open = true;
		bool result = ImGui::Begin( name.c_str(), &open, flags );
		if( !open )
			on_close();
		return result;

	}
	/*
		brief = "Render some unformatted text to the screen"

		[parameters]
			text = "The text to be rendered to the screen"
	*/
	GENERATE void Text( const string& text )
	{
		ImGui::TextUnformatted( text.c_str() );
	}
	/*
		brief = "Render some coloured text to the screen."

		[parameters]
			col = "Colour of the text. The format of the colour should be RGBA, in the range of 0 .. 1"
			text = "The text to be rendered to the screen."
	*/
	GENERATE void TextColored( const ImVec4& col, const string& text )
	{
		ImGui::TextColored( col, text.c_str() );
	}
	GENERATE void TextDisabled( const string& text )
	{
		ImGui::TextDisabled( text.c_str() );
	}
	GENERATE void TextWrapped( const string& text )
	{
		ImGui::TextWrapped( text.c_str() );
	}
	GENERATE void LabelText( const string& text )
	{
		ImGui::TextWrapped( text.c_str() );
	}
	GENERATE void BulletText( const string& text )
	{
		ImGui::TextWrapped( text.c_str() );
	}
	/*
		brief = "Render the 'Demo' window, containing a large variety of ImGui functionality for you to browse and learn."

		[parameters]
			on_close = "Callback for if you would like the user to be able to close this window."
	*/
	GENERATE void ShowDemoWindow( function<void()> on_close = nullptr )
	{
		std::vector<int> hi;
		bool open = true;
		ImGui::ShowDemoWindow( &open );
		if( !open && on_close )
			on_close();
	}
	/*
		brief = "Render the 'About' window, containing information about the current ImGui build."

		[parameters]
			on_close = "Callback for if you would like the user to be able to close this window."
	*/
	GENERATE void ShowAboutWindow( function<void()> on_close = nullptr )
	{
		bool open = true;
		ImGui::ShowAboutWindow( &open );
		if( !open && on_close )
			on_close();
	}
	/*
		brief = "Render the 'Metrics' window, containing a mass of debugging information about the current state of the application. Use this window to understand why your application is behaving strangely."

		[parameters]
			on_close = "Callback for if you would like the user to be able to close this window."
	*/
	GENERATE void ShowMetricsWindow( function<void()> on_close = nullptr )
	{
		bool open = true;
		ImGui::ShowMetricsWindow( &open );
		if( !open && on_close )
			on_close();
	}
	/*
		brief = "Render a checkbox to the screen."
		return = "Returns true when the checkbox was clicked by the user. Implies that the check value will change from true->false or false->true."

		[parameters]
			label = "The name of the checkbox, and the unique hash. Use the '##' operator to append additional information hidden from the user."
			checked = "Whether or not the checkbox should be rendered as 'checked' or 'unchecked'"
			on_check = "Callback function, triggered when the user clicks on the checkbox."
	*/
	GENERATE bool Checkbox( const string& label, bool checked, function<void( bool )> on_check = false )
	{
		if( ImGui::Checkbox( label.c_str(), &checked ) )
		{
			if( on_check )
				on_check( checked );
			return true;
		}

		return false;
	}
	GENERATE bool RadioButton( const string& label, const bool selected )
	{
		return ImGui::RadioButton( label.c_str(), selected );
	}
	GENERATE bool BeginChild( const string& str_id, const ImVec2& size = ImVec2( 0, 0 ),
		const bool border = false, ImGuiWindowFlags flags = 0 )
	{
		return ImGui::BeginChild( str_id.c_str(), size, border, flags );
	}
	GENERATE py::object GetIO()
	{
		return py::cast( &ImGui::GetIO() );
	}
	GENERATE py::object GetCurrentContext()
	{
		return py::cast( ImGui::GetCurrentContext() );
	}
	GENERATE py::object GetMainViewport()
	{
		return py::cast( ImGui::GetMainViewport() );
	}
	GENERATE py::object GetMonitor( const int idx = 0 )
	{
		return py::cast( ImGui::GetPlatformIO().Monitors[idx] );
	}
	GENERATE py::object GetStyle()
	{
		return py::cast( &ImGui::GetStyle() );
	}
	GENERATE void PushStyleColor( const ImGuiCol idx, const ImU32 col )
	{
		ImGui::PushStyleColor( idx, col );
	}
	GENERATE py::object GetStyleColorsLight()
	{
		ImGuiStyle dst;
		ImGui::StyleColorsLight( &dst );
		return py::cast( dst );
	}
	GENERATE py::object GetStyleColorsDark()
	{
		ImGuiStyle dst;
		ImGui::StyleColorsDark( &dst );
		return py::cast( dst );
	}
	GENERATE py::object GetStyleColorsClassic()
	{
		ImGuiStyle dst;
		ImGui::StyleColorsClassic( &dst );
		return py::cast( dst );
	}
	GENERATE void SetStyleColorsLight()
	{
		ImGui::StyleColorsLight();
	}
	GENERATE void SetStyleColorsDark()
	{
		ImGui::StyleColorsDark();
	}
	GENERATE void SetStyleColorsClassic()
	{
		ImGui::StyleColorsClassic();
	}
	GENERATE void PushStyleColor_2( const ImGuiCol idx, const ImVec4 col )
	{
		ImGui::PushStyleColor( idx, col );
	}
	GENERATE void PushStyleVar( const ImGuiStyleVar idx, const float val )
	{
		ImGui::PushStyleVar( idx, val );
	}
	GENERATE void PushStyleVar_2( const ImGuiStyleVar idx, const ImVec2& val )
	{
		ImGui::PushStyleVar( idx, val );
	}
	GENERATE ImU32 GetColorU32( const ImGuiCol idx, const float alpha_mul = 1.0f )
	{
		return ImGui::GetColorU32( idx, alpha_mul );
	}
	GENERATE ImU32 GetColorU32_2( const ImVec4& col )
	{
		return ImGui::GetColorU32( col );
	}
	GENERATE ImU32 GetColorU32_3( const ImU32 col )
	{
		return ImGui::GetColorU32( col );
	}
	GENERATE void PushID( const string& id )
	{
		ImGui::PushID( id.c_str() );
	}
	GENERATE void PushID_2( const int id )
	{
		ImGui::PushID( id );
	}
	GENERATE ImGuiID GetID( const string& id )
	{
		return ImGui::GetID( id.c_str() );
	}
	GENERATE void SetNextWindowSizeConstraints( const ImVec2& size_min, const ImVec2& size_max )
	{
		ImGui::SetNextWindowSizeConstraints( size_min, size_max );
	}
	GENERATE void SetWindowFocus()
	{
		ImGui::SetWindowFocus();
	}
	GENERATE void SetWindowFocus_2( const string& name )
	{
		ImGui::SetWindowFocus( name.c_str() );
	}
	GENERATE void SetWindowPos( const ImVec2& pos, const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowPos( pos, cond );
	}
	GENERATE void SetWindowPos_2( const string& name, const ImVec2& pos, const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowPos( name.c_str(), pos, cond );
	}
	GENERATE void SetWindowSize( const ImVec2& size, const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowSize( size, cond );
	}
	GENERATE void SetWindowSize_2( const string& name, const ImVec2& size, const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowSize( name.c_str(), size, cond );
	}
	GENERATE void SetWindowCollapsed( const bool collapsed, const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowCollapsed( collapsed, cond );
	}
	GENERATE void SetWindowCollapsed_2( const string& name, const bool collapsed,
		const ImGuiCond cond = 0 )
	{
		ImGui::SetWindowCollapsed( name.c_str(), collapsed, cond );
	}
	GENERATE bool TreeNode( const string& name )
	{
		return ImGui::TreeNode( name.c_str() );
	}
	GENERATE bool TreeNode_2( const string& name, const std::string& format )
	{
		return ImGui::TreeNode( name.c_str(), format.c_str() );
	}
	GENERATE bool TreeNode_3( const void* ptr, const std::string& format )
	{
		return ImGui::TreeNode( ptr, format.c_str() );
	}
	GENERATE bool TreeNode_4( const ImGuiID id, const std::string& format )
	{
		return ImGui::TreeNode( (void*)(intptr_t)id, format.c_str() );
	}
	GENERATE void TreePush( const string& name )
	{
		ImGui::TreePush( name.c_str() );
	}
	/*
		brief = "Start a new collapsing section. Use CollapsingHeader over TreeNode when you wish to make the column header appear very bold to the user."
		return = "Returns true when you should render information under this header."

		[parameters]
			label = "The name of the header, and the unique hash that describes the widget. Use the '##' operator to append additional hash information you wish to hide from the user."
			flags = "Modify the behaviour of the CollapsingHeader."
	*/
	GENERATE bool CollapsingHeader( const string& label, const ImGuiTreeNodeFlags flags = 0 )
	{
		return ImGui::CollapsingHeader( label.c_str(), flags );
	}
	/*
		brief = "Start a new collapsing section. This overload of CollapsingHeader comes with an 'x' button for the user to close the section. Use this to create dynamic lists that the user can add/delete to at will."
		return = "Returns true when you should render information under this header."

		[parameters]
			label = "The name of the header, and the unique hash that describes the widget. Use the '##' operator to append additional hash information you wish to hide from the user."
			closed_callback = "Callback function. This event will be triggered when the user tries to close this header."
			flags = "Modify the behaviour of the CollapsingHeader."
	*/
	GENERATE bool CollapsingHeader_2( const string& label, function<void()> closed_callback,
		const ImGuiTreeNodeFlags flags = 0 )
	{
		bool open = true;
		const bool result = ImGui::CollapsingHeader( label.c_str(), &open, flags );

		if( !open && closed_callback )
			closed_callback();

		return result;
	}
	GENERATE bool Selectable( const string& label, const bool selected = false,
		const ImGuiSelectableFlags flags = 0,
		const ImVec2 & size_arg = ImVec2( 0, 0 ) )
	{
		return ImGui::Selectable( label.c_str(), selected, flags, size_arg );
	}
	GENERATE bool ListBoxHeader( const string& label, const ImVec2& size_arg = ImVec2( 0, 0 ) )
	{
		return ImGui::ListBoxHeader( label.c_str(), size_arg );
	}
	GENERATE bool InputText( const string& label, const string& text, const ImGuiInputTextFlags flags = 0,
		function<void( const string& )> text_changed = nullptr )
	{
		string buffer = text;
		buffer.reserve( buffer.size() + 256 );
		if( ImGui::InputText( label.c_str(),
			(char*)buffer.data(),
			buffer.capacity(),
			flags,
			nullptr,
			nullptr ) )
		{
			if( text_changed )
				text_changed( buffer.c_str() );
			return true;
		}
		return false;
	}
	GENERATE bool InputTextMultiline( const string& label, const string& text,
		const ImVec2& size = ImVec2( 0, 0 ),
		const ImGuiInputTextFlags flags = 0,
		function<void( const string& )> text_changed = nullptr )
	{
		string buffer = text;
		buffer.reserve( buffer.size() + 256 );
		if( ImGui::InputTextMultiline( label.c_str(),
			(char*)buffer.data(),
			buffer.capacity(),
			size,
			flags,
			nullptr,
			nullptr ) )
		{
			if( text_changed )
				text_changed( buffer.c_str() );
			return true;
		}
		return false;
	}
	GENERATE bool InputTextWithHint( const string& label, const string& hint, const string& text,
		const ImGuiInputTextFlags flags = 0,
		function<void( const string& )> text_changed = nullptr )
	{
		string buffer = text;
		buffer.reserve( buffer.size() + 256 );
		if( ImGui::InputTextWithHint( label.c_str(),
			hint.c_str(),
			(char*)buffer.data(),
			buffer.capacity(),
			flags,
			nullptr,
			nullptr ) )
		{
			if( text_changed )
				text_changed( buffer.c_str() );
			return true;
		}
		return false;
	}
	GENERATE bool InputFloat( const string& label, float v, float step = 0.0f, float step_fast = 0.0f,
		const string & format = "%.3f", const ImGuiInputTextFlags flags = 0,
		function<void( float )> value_changed = nullptr )
	{
		if( ImGui::InputFloat( label.c_str(), &v, step, step_fast, format.c_str(), flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputFloat2( const string& label, float v[2], const string& format = "%.3f",
		const ImGuiInputTextFlags flags = 0,
		function<void( float[2] )> value_changed = nullptr )
	{
		if( ImGui::InputFloat2( label.c_str(), v, format.c_str(), flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputFloat3( const string& label, float v[3], const string& format = "%.3f",
		const ImGuiInputTextFlags flags = 0,
		function<void( float[3] )> value_changed = nullptr )
	{
		if( ImGui::InputFloat3( label.c_str(), v, format.c_str(), flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputFloat4( const string& label, float v[4], const string& format = "%.3f",
		const ImGuiInputTextFlags flags = 0,
		function<void( float[4] )> value_changed = nullptr )
	{
		if( ImGui::InputFloat4( label.c_str(), v, format.c_str(), flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputInt( const string& label, int v, const int step = 1, const int step_fast = 100,
		const ImGuiInputTextFlags flags = 0,
		function<void( int )> value_changed = nullptr )
	{
		if( ImGui::InputInt( label.c_str(), &v, step, step_fast, flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputInt2( const string& label, int v[2], const ImGuiInputTextFlags flags = 0,
		function<void( int[2] )> value_changed = nullptr )
	{
		if( ImGui::InputInt2( label.c_str(), v, flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputInt3( const string& label, int v[3], const ImGuiInputTextFlags flags = 0,
		function<void( int[3] )> value_changed = nullptr )
	{
		if( ImGui::InputInt3( label.c_str(), v, flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool InputInt4( const string& label, int v[4], const ImGuiInputTextFlags flags = 0,
		function<void( int[4] )> value_changed = nullptr )
	{
		if( ImGui::InputInt4( label.c_str(), v, flags ) )
		{
			if( value_changed )
				value_changed( v );
			return true;
		}
		return false;
	}
	GENERATE bool ColorPicker3( const string& label, ImVec4 col, const ImGuiColorEditFlags flags = 0,
		function<void( ImVec4 )> value_changed = nullptr )
	{
		if( ImGui::ColorPicker3( label.c_str(), &col.x, flags ) )
		{
			if( value_changed )
				value_changed( col );
			return true;
		}
		return false;
	}
	GENERATE bool ColorPicker4( const string& label, ImVec4 col, const ImGuiColorEditFlags flags = 0,
		function<void( ImVec4 )> value_changed = nullptr )
	{
		if( ImGui::ColorPicker4( label.c_str(), &col.x, flags ) )
		{
			if( value_changed )
				value_changed( col );
			return true;
		}
		return false;
	}
	GENERATE bool ColorEdit3( const string& label, ImVec4 col, const ImGuiColorEditFlags flags = 0,
		function<void( ImVec4 )> value_changed = nullptr )
	{
		if( ImGui::ColorEdit3( label.c_str(), &col.x, flags ) )
		{
			if( value_changed )
				value_changed( col );
			return true;
		}
		return false;
	}
	GENERATE bool ColorEdit4( const string& label, ImVec4 col, const ImGuiColorEditFlags flags = 0,
		function<void( ImVec4 )> value_changed = nullptr )
	{
		if( ImGui::ColorEdit4( label.c_str(), &col.x, flags ) )
		{
			if( value_changed )
				value_changed( col );
			return true;
		}
		return false;
	}
	GENERATE bool IsPopupOpen( const string& str_id )
	{
		return ImGui::IsPopupOpen( str_id.c_str() );
	}
	GENERATE const char* GetVersion()
	{
		return ImGui::GetVersion();
	}
	GENERATE int GetVersionNumber()
	{
		return IMGUI_VERSION_NUM;
	}
	/*
		brief = "Call when you are ready to finish rendering your ImGui window. Call this even if Begin(...) returned false!"
	*/
	GENERATE void End()
	{
		ImGui::End();
	}
	/*
		brief = "Call when you are ready to finish rendering your ImGui child window. Call this even if BeginChild(...) returned false!"
	*/
	GENERATE void EndChild()
	{
		ImGui::EndChild();
	}
	GENERATE bool IsWindowAppearing()
	{
		return ImGui::IsWindowAppearing();
	}
	GENERATE bool IsWindowCollapsed()
	{
		return ImGui::IsWindowCollapsed();
	}
	GENERATE bool IsWindowFocused()
	{
		return ImGui::IsWindowFocused();
	}
	GENERATE bool IsWindowHovered()
	{
		return ImGui::IsWindowHovered();
	}
	GENERATE ImDrawList* GetWindowDrawList()
	{
		return ImGui::GetWindowDrawList();
	}
	GENERATE float GetWindowDpiScale()
	{
		return ImGui::GetWindowDpiScale();
	}
	GENERATE ImGuiViewport* GetWindowViewport()
	{
		return ImGui::GetWindowViewport();
	}
	GENERATE ImVec2 GetWindowPos()
	{
		return ImGui::GetWindowPos();
	}
	GENERATE ImVec2 GetWindowSize()
	{
		return ImGui::GetWindowSize();
	}
	GENERATE float GetWindowWidth()
	{
		return ImGui::GetWindowWidth();
	}
	GENERATE float GetWindowHeight()
	{
		return ImGui::GetWindowHeight();
	}
	GENERATE ImVec2 GetContentRegionMax()
	{
		return ImGui::GetContentRegionMax();
	}
	GENERATE ImVec2 GetContentRegionAvail()
	{
		return ImGui::GetContentRegionAvail();
	}
	GENERATE float GetContentRegionAvailWidth()
	{
		return ImGui::GetContentRegionAvailWidth();
	}
	GENERATE ImVec2 GetWindowContentRegionMin()
	{
		return ImGui::GetWindowContentRegionMin();
	}
	GENERATE ImVec2 GetWindowContentRegionMax()
	{
		return ImGui::GetWindowContentRegionMax();
	}
	GENERATE float GetWindowContentRegionWidth()
	{
		return ImGui::GetWindowContentRegionWidth();
	}
	GENERATE void SetNextWindowPos( const ImVec2& pos, const ImGuiCond cond = 0,
		const ImVec2 & pivot = ImVec2( 0, 0 ) )
	{
		ImGui::SetNextWindowPos( pos, cond, pivot );
	}
	GENERATE void SetNextWindowSize( const ImVec2& size, const ImGuiCond cond = 0 )
	{
		ImGui::SetNextWindowSize( size, cond );
	}
	GENERATE void SetNextWindowContentSize( const ImVec2& size )
	{
		ImGui::SetNextWindowContentSize( size );
	}
	GENERATE void SetNextWindowCollapsed( const bool collapsed, const ImGuiCond cond = 0 )
	{
		ImGui::SetNextWindowCollapsed( cond );
	}
	GENERATE void SetNextWindowFocus()
	{
		ImGui::SetNextWindowFocus();
	}
	GENERATE void SetNextWindowBgAlpha( const float alpha )
	{
		ImGui::SetNextWindowBgAlpha( alpha );
	}
	GENERATE void SetNextWindowViewport( const ImGuiID id )
	{
		ImGui::SetNextWindowViewport( id );
	}
	GENERATE void SetWindowFontScale( const float scale )
	{
		ImGui::SetWindowFontScale( scale );
	}
	GENERATE float GetScrollX()
	{
		return ImGui::GetScrollX();
	}
	GENERATE float GetScrollY()
	{
		return ImGui::GetScrollY();
	}
	GENERATE float GetScrollMaxX()
	{
		return ImGui::GetScrollMaxX();
	}
	GENERATE float GetScrollMaxY()
	{
		return ImGui::GetScrollMaxY();
	}
	GENERATE void SetScrollX( const float scroll_x )
	{
		ImGui::SetScrollX( scroll_x );
	}
	GENERATE void SetScrollY( const float scroll_y )
	{
		ImGui::SetScrollY( scroll_y );
	}
	GENERATE void SetScrollHereY( const float centre_y_ratio = 0.5f )
	{
		ImGui::SetScrollHereY( centre_y_ratio );
	}
	GENERATE void SetScrollFromPosY( const float local_y, const float centre_y_ratio = 0.5f )
	{
		ImGui::SetScrollFromPosY( local_y, centre_y_ratio );
	}
	GENERATE void PushFont( ImFont* font )
	{
		ImGui::PushFont( font );
	}
	GENERATE void PopFont()
	{
		ImGui::PopFont();
	}
	GENERATE void PopStyleColor( const int count = 1 )
	{
		ImGui::PopStyleColor( count );
	}
	GENERATE void PopStyleVar( const int count = 1 )
	{
		ImGui::PopStyleVar( count );
	}
	GENERATE const ImVec4& GetStyleColorVec4( const ImGuiCond cond )
	{
		return ImGui::GetStyleColorVec4( cond );
	}
	GENERATE float GetFontSize()
	{
		return ImGui::GetFontSize();
	}
	GENERATE ImVec2 GetFontTexUvWhitePixel()
	{
		return ImGui::GetFontTexUvWhitePixel();
	}
	GENERATE void PushItemWidth( const float width )
	{
		ImGui::PushItemWidth( width );
	}
	GENERATE void PopItemWidth()
	{
		ImGui::PopItemWidth();
	}
	GENERATE float CalcItemWidth()
	{
		return ImGui::CalcItemWidth();
	}
	GENERATE void PushTextWrapPos( const float wrap_pos_x = 0.0f )
	{
		ImGui::PushTextWrapPos( wrap_pos_x );
	}
	GENERATE void PopTextWrapPos()
	{
		ImGui::PopTextWrapPos();
	}
	GENERATE void PushAllowKeyboardFocus( const bool allow_keyboard_focus )
	{
		ImGui::PushAllowKeyboardFocus( allow_keyboard_focus );
	}
	GENERATE void PopAllowKeyboardFocus()
	{
		ImGui::PopAllowKeyboardFocus();
	}
	GENERATE void PushButtonRepeat( const bool repeat )
	{
		ImGui::PushButtonRepeat( repeat );
	}
	GENERATE void PopButtonRepeat()
	{
		ImGui::PopButtonRepeat();
	}
	GENERATE void Separator()
	{
		ImGui::Separator();
	}
	GENERATE void SameLine( const float local_pos_x = 0.0f, const float spacing_w = -1.0f )
	{
		ImGui::SameLine( local_pos_x, spacing_w );
	}
	GENERATE void NewLine()
	{
		ImGui::NewLine();
	}
	GENERATE void Spacing()
	{
		ImGui::Spacing();
	}
	GENERATE void Dummy( const ImVec2& size )
	{
		ImGui::Dummy( size );
	}
	GENERATE void Indent( const float indent_w = 0.0f )
	{
		ImGui::Indent( indent_w );
	}
	GENERATE void Unindent( const float indent_w = 0.0f )
	{
		ImGui::Unindent( indent_w );
	}
	GENERATE void BeginGroup()
	{
		ImGui::BeginGroup();
	}
	GENERATE void EndGroup()
	{
		ImGui::EndGroup();
	}
	GENERATE ImVec2 GetCursorPos()
	{
		return ImGui::GetCursorPos();
	}
	GENERATE float GetCursorPosX()
	{
		return ImGui::GetCursorPosX();
	}
	GENERATE float GetCursorPosY()
	{
		return ImGui::GetCursorPosY();
	}
	GENERATE void SetCursorPos( const ImVec2& local_pos )
	{
		ImGui::SetCursorPos( local_pos );
	}
	GENERATE void SetCursorPosX( const float x )
	{
		ImGui::SetCursorPosX( x );
	}
	GENERATE void SetCursorPosY( const float y )
	{
		ImGui::SetCursorPosY( y );
	}
	GENERATE ImVec2 GetCursorStartPos()
	{
		return ImGui::GetCursorStartPos();
	}
	GENERATE ImVec2 GetCursorScreenPos()
	{
		return ImGui::GetCursorScreenPos();
	}
	GENERATE void SetCursorScreenPos( const ImVec2& pos )
	{
		ImGui::SetCursorScreenPos( pos );
	}
	GENERATE void AlignTextToFramePadding()
	{
		ImGui::AlignTextToFramePadding();
	}
	GENERATE float GetTextLineHeight()
	{
		return ImGui::GetTextLineHeight();
	}
	GENERATE float GetTextLineHeightWithSpacing()
	{
		return ImGui::GetTextLineHeightWithSpacing();
	}
	GENERATE float GetFrameHeight()
	{
		return ImGui::GetFrameHeight();
	}
	GENERATE float GetFrameHeightWithSpacing()
	{
		return ImGui::GetFrameHeightWithSpacing();
	}
	GENERATE void PopID()
	{
		ImGui::PopID();
	}
	/*
		brief = "Render a labelled button to the screen."
		return = "Returns true when the button was clicked by the user."

		[parameters]
			label = "The name of the button, and the unique hash describing the button. Use the '##' operator to append additional hash information hidden from the user."
			size_arg = "Custom size for the button. Use zero on an axis to let ImGui handle the size of an axis for you."
	*/
	GENERATE bool Button( const string& label, const ImVec2& size_arg = ImVec2( 0, 0 ) )
	{
		return ImGui::Button( label.c_str(), size_arg );
	}
	GENERATE bool SmallButton( const string& label )
	{
		return ImGui::SmallButton( label.c_str() );
	}
	GENERATE bool InvisibleButton( const string& label, const ImVec2& size_arg )
	{
		return ImGui::InvisibleButton( label.c_str(), size_arg );
	}
	GENERATE bool ArrowButton( const string& label, const ImGuiDir dir )
	{
		return ImGui::ArrowButton( label.c_str(), dir );
	}
	GENERATE void Image( const ImTextureID id, const ImVec2& size, const ImVec2& uv0 = ImVec2( 0, 0 ),
		const ImVec2& uv1 = ImVec2( 1, 1 ),
		const ImVec4& tint_col = ImVec4( 1, 1, 1, 1 ),
		const ImVec4& border_col = ImVec4( 0, 0, 0, 0 ) )
	{
		ImGui::Image( id, size, uv0, uv1, tint_col, border_col );
	}
	GENERATE bool ImageButton( const ImTextureID id, const ImVec2& size,
		const ImVec2& uv0 = ImVec2( 0, 0 ), const ImVec2& uv1 = ImVec2( 1, 1 ),
		const int frame_padding = -1, const ImVec4 & bg_col = ImVec4( 0, 0, 0, 0 ),
		const ImVec4 & tint_col = ImVec4( 1, 1, 1, 1 ) )
	{
		return ImGui::ImageButton( id, size, uv0, uv1, frame_padding, bg_col, tint_col );
	}
	GENERATE void ProgressBar( const float fraction, const ImVec2& size_arg = ImVec2( -1, 0 ),
		const char* overlay = nullptr )
	{
		ImGui::ProgressBar( fraction, size_arg, overlay );
	}
	GENERATE void Bullet()
	{
		ImGui::Bullet();
	}
	GENERATE bool BeginCombo( const string& label, const string& preview,
		const ImGuiComboFlags flags = 0 )
	{
		return ImGui::BeginCombo( label.c_str(), preview.c_str(), flags );
	}
	GENERATE void EndCombo()
	{
		ImGui::EndCombo();
	}
	GENERATE bool DragFloat( const string& label, float v, function<void( float )> on_changed_callback,
		const float v_speed = 1.0f, const float v_min = 0.0f,
		const float v_max = 0.0f, const string & format = "%.3f",
		const float power = 1.0f )
	{
		if( ImGui::DragFloat( label.c_str(), &v, v_speed, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragFloat2( const string& label, float v[2],
		function<void( float[2] )> on_changed_callback, const float v_speed = 1.0f,
		const float v_min = 0.0f, const float v_max = 0.0f,
		const string & format = "%.3f", const float power = 1.0f )
	{
		if( ImGui::DragFloat2( label.c_str(), v, v_speed, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragFloat3( const string& label, float v[3],
		function<void( float[3] )> on_changed_callback, const float v_speed = 1.0f,
		const float v_min = 0.0f, const float v_max = 0.0f,
		const string & format = "%.3f", const float power = 1.0f )
	{
		if( ImGui::DragFloat3( label.c_str(), v, v_speed, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragFloat4( const string& label, float v[4],
		function<void( float[4] )> on_changed_callback, const float v_speed = 1.0f,
		const float v_min = 0.0f, const float v_max = 0.0f,
		const string & format = "%.3f", const float power = 1.0f )
	{
		if( ImGui::DragFloat4( label.c_str(), v, v_speed, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragFloatRange2( const string& label, float v_current_min, float v_current_max,
		function<void( float, float )> on_changed_callback,
		const float v_speed = 1.0f, const float v_min = 0.0f,
		const float v_max = 0.0f, const string & format = "%.3f",
		const char* format_max = nullptr, const float power = 1.0f )
	{
		if( ImGui::DragFloatRange2( label.c_str(),
			&v_current_min,
			&v_current_max,
			v_speed,
			v_min,
			v_max,
			format.c_str(),
			format_max,
			power ) )
		{
			on_changed_callback( v_current_min, v_current_max );
			return true;
		}
		return false;
	}
	GENERATE bool DragInt( const string& label, int v, function<void( int )> on_changed_callback,
		const float v_speed = 1.0f, const int v_min = 0, const int v_max = 0,
		const string & format = "%d" )
	{
		if( ImGui::DragInt( label.c_str(), &v, v_speed, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragInt2( const string& label, int v[2], function<void( int[2] )> on_changed_callback,
		const float v_speed = 1.0f, const int v_min = 0, const int v_max = 0,
		const string & format = "%d" )
	{
		if( ImGui::DragInt2( label.c_str(), v, v_speed, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragInt3( const string& label, int v[3], function<void( int[3] )> on_changed_callback,
		const float v_speed = 1.0f, const int v_min = 0.0f, const int v_max = 0.0f,
		const string & format = "%d" )
	{
		if( ImGui::DragInt3( label.c_str(), v, v_speed, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragInt4( const string& label, int v[4], function<void( int[4] )> on_changed_callback,
		const float v_speed = 1.0f, const int v_min = 0.0f, const int v_max = 0.0f,
		const string & format = "%d" )
	{
		if( ImGui::DragInt4( label.c_str(), v, v_speed, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool DragIntRange2( const string& label, int v_current_min, int v_current_max,
		function<void( int, int )> on_changed_callback,
		const float v_speed = 1.0f, const int v_min = 0.0f,
		const int v_max = 0.0f, const string & format = "%d",
		const char* format_max = nullptr )
	{
		if( ImGui::DragIntRange2( label.c_str(),
			&v_current_min,
			&v_current_max,
			v_speed,
			v_min,
			v_max,
			format.c_str(),
			format_max ) )
		{
			on_changed_callback( v_current_min, v_current_max );
			return true;
		}
		return false;
	}
	GENERATE bool SliderFloat( const string& label, float v, const float v_min, const float v_max,
		function<void( float )> on_changed_callback, const string& format = "%.3f",
		const float power = 1.0f )
	{
		if( ImGui::SliderFloat( label.c_str(), &v, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool SliderFloat2( const string& label, float v[2], const float v_min, const float v_max,
		const string& format = "%.3f", const float power = 1.0f )
	{
		if( ImGui::SliderFloat2( label.c_str(), v, v_min, v_max, format.c_str(), power ) )
		{
			return true;
		}
		return false;
	}
	GENERATE bool SliderFloat3( const string& label, float v[3], const float v_min, const float v_max,
		const string& format = "%.3f", const float power = 1.0f )
	{
		// TODO
		return ImGui::SliderFloat3( label.c_str(), v, v_min, v_max, format.c_str(), power );
	}
	GENERATE bool SliderFloat4( const string& label, float v[4],
		function<void( float[4] )> on_changed_callback, const float v_min,
		const float v_max, const string& format = "%.3f",
		const float power = 1.0f )
	{
		if( ImGui::SliderFloat4( label.c_str(), v, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	/*
		brief = "Create a slider widget with the intention of allowing the user to customize an angle value."
		return = "Returns true when the user has interacted with this widget, changing the angle value."

		[parameters]
			label = "The name of the slider, and the unique hash."
			v_rad = "The current angle value, in radians."
			on_changed_callback = "Callback event. Triggered when the user changes the value."
			v_degrees_min = "Minimum angle, in degrees."
			v_degrees_max = "Maximum angle, in degrees."
			format = "Use this to customize the format of the text displayed on the slider."
	*/
	GENERATE bool SliderAngle( const string& label, float v_rad,
		function<void( float )> on_changed_callback,
		const float v_degrees_min = -360.0f, const float v_degrees_max = 360.0f,
		const string & format = "%.0f deg" )
	{
		if( ImGui::SliderAngle( label.c_str(), &v_rad, v_degrees_min, v_degrees_max, format.c_str() ) )
		{
			on_changed_callback( v_rad );
			return true;
		}
		return false;
	}
	GENERATE bool SliderInt( const string& label, int v, function<void( int )> on_changed_callback,
		const int v_min, const int v_max, const string& format = "%d" )
	{
		if( ImGui::SliderInt( label.c_str(), &v, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool SliderInt2( const string& label, int v[2], function<void( int[2] )> on_changed_callback,
		const int v_min, const int v_max, const string& format = "%d" )
	{
		if( ImGui::SliderInt2( label.c_str(), v, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool SliderInt3( const string& label, int v[3], function<void( int[3] )> on_changed_callback,
		const int v_min, const int v_max, const string& format = "%d" )
	{
		if( ImGui::SliderInt3( label.c_str(), v, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool SliderInt4( const string& label, int v[4], function<void( int[4] )> on_changed_callback,
		const int v_min, const int v_max, const string& format = "%d" )
	{
		if( ImGui::SliderInt4( label.c_str(), v, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool VSliderFloat( const string& label, const ImVec2& size, float v,
		function<void( float )> on_changed_callback, float v_min,
		const float v_max, const string& format = "%.3f",
		const float power = 1.0f )
	{
		if( ImGui::VSliderFloat( label.c_str(), size, &v, v_min, v_max, format.c_str(), power ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool VSliderInt( const string& label, const ImVec2& size, int v,
		function<void( int )> on_changed_callback, const int v_min, const int v_max,
		const string& format = "%d" )
	{
		if( ImGui::VSliderInt( label.c_str(), size, &v, v_min, v_max, format.c_str() ) )
		{
			on_changed_callback( v );
			return true;
		}
		return false;
	}
	GENERATE bool ColorButton( const string& desc_id, const ImVec4& col,
		const ImGuiColorEditFlags flags = 0, const ImVec2 size = ImVec2( 0, 0 ) )
	{
		return ImGui::ColorButton( desc_id.c_str(), col, flags, size );
	}
	GENERATE void SetColorEditOptions( const ImGuiColorEditFlags flags )
	{
		ImGui::SetColorEditOptions( flags );
	}
	GENERATE void TreePop()
	{
		ImGui::TreePop();
	}
	GENERATE void TreeAdvanceToLabelPos()
	{
		ImGui::TreeAdvanceToLabelPos();
	}
	GENERATE float GetTreeNodeToLabelSpacing()
	{
		return ImGui::GetTreeNodeToLabelSpacing();
	}
	GENERATE void SetNextTreeNodeOpen( bool is_open, const ImGuiCond cond = 0 )
	{
		ImGui::SetNextTreeNodeOpen( is_open, cond );
	}
	GENERATE void ListBoxFooter()
	{
		ImGui::ListBoxFooter();
	}
	GENERATE bool BeginMainMenuBar()
	{
		return ImGui::BeginMainMenuBar();
	}
	GENERATE void EndMainMenuBar()
	{
		ImGui::EndMainMenuBar();
	}
	GENERATE bool BeginMenuBar()
	{
		return ImGui::BeginMenuBar();
	}
	GENERATE void EndMenuBar()
	{
		ImGui::EndMenuBar();
	}
	GENERATE bool BeginMenu( const string& label, const bool enabled = true )
	{
		return ImGui::BeginMenu( label.c_str(), enabled );
	}
	GENERATE void EndMenu()
	{
		ImGui::EndMenu();
	}
	GENERATE void BeginTooltip()
	{
		ImGui::BeginTooltip();
	}
	GENERATE void EndTooltip()
	{
		ImGui::EndTooltip();
	}
	GENERATE void OpenPopup( const string& id )
	{
		ImGui::OpenPopup( id.c_str() );
	}
	GENERATE bool BeginPopup( const string& str_id, const ImGuiWindowFlags flags = 0 )
	{
		return ImGui::BeginPopup( str_id.c_str(), flags | ImGuiWindowFlags_Popup );
	}
	GENERATE bool BeginPopupContextItem( const char* str_id = nullptr, const int mouse_button = 1 )
	{
		return ImGui::BeginPopupContextItem( str_id, mouse_button );
	}
	GENERATE bool BeginPopupContextWindow( const char* str_id = nullptr, const int mouse_button = 1,
		const bool also_over_items = true )
	{
		return ImGui::BeginPopupContextWindow( str_id, mouse_button, also_over_items );
	}
	GENERATE bool BeginPopupContextVoid( const char* str_id = nullptr, const int mouse_button = 1 )
	{
		return ImGui::BeginPopupContextVoid( str_id, mouse_button );
	}
	GENERATE bool BeginPopupModal( const string& name, function<void()> closed_callback = nullptr,
		const ImGuiWindowFlags flags = 0 )
	{
		if( closed_callback )
		{
			bool open = true;
			const auto result =
				ImGui::BeginPopupModal( name.c_str(), &open, flags | ImGuiWindowFlags_Popup );

			if( !open )
				closed_callback();

			return result;
		}
		return ImGui::BeginPopupModal( name.c_str(), nullptr, flags | ImGuiWindowFlags_Popup );
	}
	GENERATE void EndPopup()
	{
		ImGui::EndPopup();
	}
	GENERATE bool OpenPopupOnItemClick( const char* str_id = nullptr, const int mouse_button = 1 )
	{
		return ImGui::OpenPopupOnItemClick( str_id, mouse_button );
	}
	GENERATE void CloseCurrentPopup()
	{
		ImGui::CloseCurrentPopup();
	}
	/*
		brief = "Create a table of rows/columns. Use NextColumn() to step through columns. Rows are created implicitly with NextColumn(). Use Columns(1) to return to normal."

		[parameters]
			columns_count = "The number of columns in the table."
			id = "Used to hash the columns, if you need to differentiate one set of columns from another."
			border = "Whether or not you should render a border to the columns."
	*/
	GENERATE void Columns( const int columns_count = 1, const char* id = nullptr,
		const bool border = true )
	{
		ImGui::Columns( columns_count, id, border );
	}
	GENERATE void NextColumn()
	{
		ImGui::NextColumn();
	}
	GENERATE int GetColumnIndex()
	{
		return ImGui::GetColumnIndex();
	}
	GENERATE float GetColumnWidth()
	{
		return ImGui::GetColumnWidth();
	}
	GENERATE void SetColumnWidth( const int column_index, const float width )
	{
		ImGui::SetColumnWidth( column_index, width );
	}
	GENERATE float GetColumnOffset()
	{
		return ImGui::GetColumnOffset();
	}
	GENERATE void SetColumnOffset( const int column_index, const float offset )
	{
		ImGui::SetColumnOffset( column_index, offset );
	}
	GENERATE int GetColumnsCount()
	{
		return ImGui::GetColumnsCount();
	}
	GENERATE bool BeginTabBar( const string& str_id, const ImGuiTabBarFlags flags = 0 )
	{
		return ImGui::BeginTabBar( str_id.c_str(), flags );
	}
	GENERATE void EndTabBar()
	{
		ImGui::EndTabBar();
	}
	GENERATE void EndTabItem()
	{
		ImGui::EndTabItem();
	}
	GENERATE void SetTabItemClosed( const string& label )
	{
		ImGui::SetTabItemClosed( label.c_str() );
	}
	GENERATE void DockSpace( const ImGuiID id, const ImVec2& size_arg = ImVec2( 0, 0 ),
		const ImGuiDockNodeFlags flags = 0,
		const ImGuiWindowClass * window_class = nullptr )
	{
		ImGui::DockSpace( id, size_arg, flags, window_class );
	}
	GENERATE ImGuiID DockSpaceOverViewport( ImGuiViewport* viewport = nullptr,
		const ImGuiDockNodeFlags dockspace_flags = 0,
		const ImGuiWindowClass * window_class = nullptr )
	{
		return ImGui::DockSpaceOverViewport( viewport, dockspace_flags, window_class );
	}
	GENERATE void SetNextWindowDockID( const ImGuiID id, const ImGuiCond cond = 0 )
	{
		ImGui::SetNextWindowDockID( id, cond );
	}
	GENERATE void SetNextWindowClass( const ImGuiWindowClass* window_class )
	{
		ImGui::SetNextWindowClass( window_class );
	}
	GENERATE ImGuiID GetWindowDockID()
	{
		return ImGui::GetWindowDockID();
	}
	GENERATE bool IsWindowDocked()
	{
		return ImGui::IsWindowDocked();
	}
	GENERATE void LogToTTY( int auto_open_depth = -1 )
	{
		ImGui::LogToTTY( auto_open_depth );
	}
	GENERATE void LogToFile( int auto_open_depth = -1, const char* filename = NULL )
	{
		ImGui::LogToFile( auto_open_depth, filename );
	}
	GENERATE void LogToClipboard( int auto_open_depth = -1 )
	{
		ImGui::LogToClipboard( auto_open_depth );
	}
	GENERATE void LogFinish()
	{
		ImGui::LogFinish();
	}
	GENERATE void LogButtons()
	{
		ImGui::LogButtons();
	}
	GENERATE void LogText( const char* text )
	{
		ImGui::LogText( text );
	}
	GENERATE bool BeginDragDropSource( const ImGuiDragDropFlags flags = 0 )
	{
		return ImGui::BeginDragDropSource( flags );
	}
	GENERATE bool SetDragDropPayload( const string& type, const void* data, const size_t data_size,
		const ImGuiCond cond = 0 )
	{
		// TODO
		return ImGui::SetDragDropPayload( type.c_str(), data, data_size, cond );
	}
	GENERATE void EndDragDropSource()
	{
		ImGui::EndDragDropSource();
	}
	GENERATE bool BeginDragDropTarget()
	{
		return ImGui::BeginDragDropTarget();
	}
	GENERATE const ImGuiPayload* AcceptDragDropPayload( const string& type,
		const ImGuiDragDropFlags flags = 0 )
	{
		return ImGui::AcceptDragDropPayload( type.c_str(), flags );
	}
	GENERATE void EndDragDropTarget()
	{
		ImGui::EndDragDropTarget();
	}
	GENERATE const ImGuiPayload* GetDragDropPayload()
	{
		return ImGui::GetDragDropPayload();
	}
	GENERATE void PushClipRect( const ImVec2& cr_min, const ImVec2& cr_max,
		const bool intersect_with_current_clip_rect )
	{
		ImGui::PushClipRect( cr_min, cr_max, intersect_with_current_clip_rect );
	}
	GENERATE void PopClipRect()
	{
		ImGui::PopClipRect();
	}
	GENERATE void SetItemDefaultFocus()
	{
		ImGui::SetItemDefaultFocus();
	}
	GENERATE void SetKeyboardFocusHere( const int offset = 0 )
	{
		ImGui::SetKeyboardFocusHere( offset );
	}
	GENERATE bool IsItemHovered( const ImGuiHoveredFlags flags = 0 )
	{
		return ImGui::IsItemHovered( flags );
	}
	GENERATE bool IsItemActive()
	{
		return ImGui::IsItemActive();
	}
	GENERATE bool IsItemFocused()
	{
		return ImGui::IsItemFocused();
	}
	GENERATE bool IsItemClicked()
	{
		return ImGui::IsItemClicked();
	}
	GENERATE bool IsItemVisible()
	{
		return ImGui::IsItemVisible();
	}
	GENERATE bool IsItemEdited()
	{
		return ImGui::IsItemEdited();
	}
	GENERATE bool IsItemActivated()
	{
		return ImGui::IsItemActivated();
	}
	GENERATE bool IsItemDeactivated()
	{
		return ImGui::IsItemDeactivated();
	}
	GENERATE bool IsItemDeactivatedAfterEdit()
	{
		return ImGui::IsItemDeactivatedAfterEdit();
	}
	GENERATE bool IsAnyItemHovered()
	{
		return ImGui::IsAnyItemHovered();
	}
	GENERATE bool IsAnyItemActive()
	{
		return ImGui::IsAnyItemActive();
	}
	GENERATE bool IsAnyItemFocused()
	{
		return ImGui::IsAnyItemFocused();
	}
	GENERATE ImVec2 GetItemRectMin()
	{
		return ImGui::GetItemRectMin();
	}
	GENERATE ImVec2 GetItemRectMax()
	{
		return ImGui::GetItemRectMax();
	}
	GENERATE ImVec2 GetItemRectSize()
	{
		return ImGui::GetItemRectSize();
	}
	GENERATE void SetItemAllowOverlap()
	{
		ImGui::SetItemAllowOverlap();
	}
	GENERATE double GetTime()
	{
		return ImGui::GetTime();
	}
	GENERATE int GetFrameCount()
	{
		return ImGui::GetFrameCount();
	}
	GENERATE const char* GetStyleColorName( const ImGuiCol idx )
	{
		return ImGui::GetStyleColorName( idx );
	}
	GENERATE ImVec2 CalcTextSize( const string& text, const bool hide_text_after_double_hash = false,
		const float wrap_width = -1.0f )
	{
		return ImGui::CalcTextSize( text.c_str(),
			text.c_str() + text.size(),
			hide_text_after_double_hash,
			wrap_width );
	}
	GENERATE bool BeginChildFrame( const std::string& id, const ImVec2& size = ImVec2( 0, 0 ),
		const ImGuiWindowFlags extra_flags = 0 )
	{
		return ImGui::BeginChildFrame( ImGui::GetID( id.c_str() ), size, extra_flags );
	}
	GENERATE bool BeginChildFrame_2( const ImGuiID id, const ImVec2& size = ImVec2( 0, 0 ),
		const ImGuiWindowFlags extra_flags = 0 )
	{
		return ImGui::BeginChildFrame( id, size, extra_flags );
	}
	GENERATE void EndChildFrame()
	{
		ImGui::EndChildFrame();
	}
	GENERATE ImVec4 ColorConvertU32ToFloat4( const ImU32 in )
	{
		return ImGui::ColorConvertU32ToFloat4( in );
	}
	GENERATE ImU32 ColorConvertFloat4ToU32( const ImVec4& in )
	{
		return ImGui::ColorConvertFloat4ToU32( in );
	}
	GENERATE std::tuple<float, float, float> ColorConvertHSVtoRGB( const float h, const float s,
		const float v )
	{
		float out_r, out_g, out_b;
		ImGui::ColorConvertHSVtoRGB( h, s, v, out_r, out_g, out_b );
		return std::tie( out_r, out_g, out_b );
	}
	GENERATE std::tuple<float, float, float> ColorConvertRGBtoHSV( const float r, const float g,
		const float b )
	{
		float out_h, out_s, out_v;
		ImGui::ColorConvertRGBtoHSV( r, g, b, out_h, out_s, out_v );
		return std::tie( out_h, out_s, out_v );
	}
	GENERATE int GetKeyIndex( const ImGuiKey imgui_key )
	{
		return ImGui::GetKeyIndex( imgui_key );
	}
	GENERATE bool IsKeyDown( const int user_key_index )
	{
		return ImGui::IsKeyDown( user_key_index );
	}
	GENERATE bool IsKeyPressed( const int user_key_index, const bool repeat = true )
	{
		return ImGui::IsKeyPressed( user_key_index, repeat );
	}
	GENERATE bool IsKeyReleased( const int user_key_index )
	{
		return ImGui::IsKeyReleased( user_key_index );
	}
	GENERATE int GetKeyPressedAmount( const int key_index, const float repeat_delay,
		const float repeat_rate )
	{
		return ImGui::GetKeyPressedAmount( key_index, repeat_delay, repeat_rate );
	}
	GENERATE bool IsMouseDown( const int button )
	{
		return ImGui::IsMouseDown( button );
	}
	GENERATE bool IsAnyMouseDown()
	{
		return ImGui::IsAnyMouseDown();
	}
	GENERATE bool IsMouseClicked( const int button, const bool repeat = false )
	{
		return ImGui::IsMouseClicked( button, repeat );
	}
	GENERATE bool IsMouseDoubleClicked( const int button )
	{
		return ImGui::IsMouseDoubleClicked( button );
	}
	GENERATE bool IsMouseReleased( const int button )
	{
		return ImGui::IsMouseReleased( button );
	}
	GENERATE bool IsMouseDragging( const int button, const float lock_threshold = -1.0f )
	{
		return ImGui::IsMouseDragging( button, lock_threshold );
	}
	GENERATE bool IsMouseHoveringRect( const ImVec2& r_min, const ImVec2& r_max, const bool clip = true )
	{
		return ImGui::IsMouseHoveringRect( r_min, r_max, clip );
	}
	GENERATE bool IsMousePosValid( const ImVec2* mouse_pos = nullptr )
	{
		return ImGui::IsMousePosValid( mouse_pos );
	}
	GENERATE ImVec2 GetMousePos()
	{
		return ImGui::GetMousePos();
	}
	GENERATE ImVec2 GetMousePosOnOpeningCurrentPopup()
	{
		return ImGui::GetMousePosOnOpeningCurrentPopup();
	}
	GENERATE ImVec2 GetMouseDragDelta( const int button = 0, const float lock_threshold = -1.0f )
	{
		return ImGui::GetMouseDragDelta( button, lock_threshold );
	}
	GENERATE void ResetMouseDragDelta( const int button = 0 )
	{
		ImGui::ResetMouseDragDelta( button );
	}
	GENERATE ImGuiMouseCursor GetMouseCursor()
	{
		return ImGui::GetMouseCursor();
	}
	GENERATE void SetMouseCursor( const ImGuiMouseCursor cursor_type )
	{
		ImGui::SetMouseCursor( cursor_type );
	}
	GENERATE void CaptureKeyboardFromApp( const bool capture = true )
	{
		ImGui::CaptureKeyboardFromApp( capture );
	}
	GENERATE void CaptureMouseFromApp( const bool capture = true )
	{
		ImGui::CaptureMouseFromApp( capture );
	}
	GENERATE const char* GetClipboardText()
	{
		return ImGui::GetClipboardText();
	}
	GENERATE void SetClipboardText( const string& text )
	{
		ImGui::SetClipboardText( text.c_str() );
	}
	GENERATE py::object LoadFont( const std::string& font_file, const float size = 12.0f,
		const int oversample_h = 3, const int oversample_v = 1,
		const bool pixel_snap = false )
	{
		auto& io = ImGui::GetIO();

		ImFontConfig config;
		config.OversampleH = oversample_h;
		config.OversampleV = oversample_v;
		config.PixelSnapH = pixel_snap;

		ImFont* font = io.Fonts->AddFontFromFileTTF( font_file.c_str(), size, &config );
		assert( font );
		return py::cast( font );
	}

	GENERATE void ShowViewportThumbnails()
	{
		ImGui::ShowViewportThumbnails();
	}

	template<typename T>
	void _DeclareVector( py::module& m, const char* typestr )
	{
		{
			std::string pyclass_name = std::string( "ImVector<" ) + typestr + ">";
			py::class_<ImVector<T>>( m, pyclass_name.c_str(), py::buffer_protocol(), py::dynamic_attr() )
				.py_read( ImVector<T>, Capacity )
				.py_read( ImVector<T>, Size )
				.def( "__getitem__",
				[]( const ImVector<T>& v, int idx ) -> const T & { return v[idx]; },
				py::is_operator() );
		}

		{
			std::string pyclass_name = std::string( "ImVector<" ) + typestr + "*>";
			py::class_<ImVector<T*>>( m, pyclass_name.c_str(), py::buffer_protocol(), py::dynamic_attr() )
				.py_read( ImVector<T*>, Capacity )
				.py_read( ImVector<T*>, Size )
				.def( "__getitem__",
				[]( const ImVector<T*>& v, int idx ) -> const T & { return *v[idx]; },
				py::is_operator() );
		}
	}

#define DECLARE_VECTOR( type ) _DeclareVector<type>( mod, #type );

	void ImGuiModule::Initialise( py::module& mod )
	{
		DECLARE_VECTOR( ImFont );
		DECLARE_VECTOR( ImGuiWindow );
		DECLARE_VECTOR( ImGuiViewportP );
		DECLARE_VECTOR( ImGuiViewport );
		DECLARE_VECTOR( ImGuiPlatformMonitor );
		DECLARE_VECTOR( ImDrawList );

		py_class1( ImFont )
			.py_read( ImFont, FallbackAdvanceX )
			.py_read( ImFont, FontSize )
			.py_read( ImFont, IndexLookup )
			.py_read( ImFont, Glyphs )
			.py_read( ImFont, FallbackGlyph )
			.py_read( ImFont, DisplayOffset )
			.py_read( ImFont, ContainerAtlas )
			.py_read( ImFont, ConfigData )
			.py_read( ImFont, ConfigDataCount )
			.py_read( ImFont, FallbackChar )
			.py_read( ImFont, Scale )
			.py_read( ImFont, Descent )
			.py_read( ImFont, MetricsTotalSurface )
			.py_read( ImFont, DirtyLookupTables )
			.py_method( ImFont, FindGlyph )
			.py_method( ImFont, FindGlyphNoFallback, py_arg( c ) )
			.py_method( ImFont, GetCharAdvance, py_arg( c ) )
			.py_method( ImFont, IsLoaded )
			.py_method( ImFont, GetDebugName );

		py_class1( ImColor, float, float, float, float )
			.def( "__eq__", []( const ImColor& a, const ImColor& b ) { return a == b; }, py::is_operator() )
			.def( "__mul__", []( float a, const ImColor& b ) { return b * a; }, py::is_operator() )
			.def( "__repr__", []( const ImColor& v ) { std::stringstream str; str << v.Value.x << ", " << v.Value.y << ", " << v.Value.z << ", " << v.Value.w; return str.str(); } )
			.py_write( ImColor, Value );

		py_class1( ImVec2, float, float )
			.def( py::self + py::self )
			.def( py::self - py::self )
			.def( py::self * py::self )
			.def( py::self / py::self )
			.def( py::self += py::self )
			.def( py::self -= py::self )
			.def( py::self * float() )
			.def( py::self *= float() )
			.def( py::self / float() )
			.def( py::self /= float() )
			.def( "__mul__", []( float a, const ImVec2& b ) { return b * a; }, py::is_operator() )
			.def( "__eq__", []( const ImVec2& a, const ImVec2& b ) { return a.x == b.x && a.y == b.y; }, py::is_operator() )
			.def( "__repr__", []( const ImVec2& v ) { std::stringstream str; str << v.x << ", " << v.y; return str.str(); } )
			.py_write( ImVec2, x )
			.py_write( ImVec2, y );

		py_class1( ImVec4, float, float, float, float )
			.def( py::self + py::self )
			.def( py::self - py::self )
			.def( py::self * py::self )
			.def( "__div__", []( const ImVec4& a, const ImVec4& b ) { return ImVec4( a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w ); }, py::is_operator() )
			.def( "__isub__", []( const ImVec4& a, const ImVec4& b ) { return ImVec4( a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w ); }, py::is_operator() )
			.def( "__iadd__", []( const ImVec4& a, const ImVec4& b ) { return ImVec4( a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w ); }, py::is_operator() )
			.def( "__mul__", []( const ImVec4& a, float b ) { return ImVec4( a.x * b, a.y * b, a.z * b, a.w * b ); }, py::is_operator() )
			.def( "__imul__", []( const ImVec4& a, float b ) { return ImVec4( a.x * b, a.y * b, a.z * b, a.w * b ); }, py::is_operator() )
			.def( "__div__", []( const ImVec4& a, float b ) { return ImVec4( a.x / b, a.y / b, a.z / b, a.w / b ); }, py::is_operator() )
			.def( "__idiv__", []( const ImVec4& a, float b ) { return ImVec4( a.x / b, a.y / b, a.z / b, a.w / b ); }, py::is_operator() )
			.def( "__mul__", []( float a, const ImVec4& b ) { return ImVec4( a * b.x, a * b.y, a * b.z, a * b.w ); }, py::is_operator() )
			.def( "__eq__", []( const ImVec4& a, const ImVec4& b ) { return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w; }, py::is_operator() )
			.def( "__repr__", []( const ImVec4& v ) { std::stringstream str; str << v.x << ", " << v.y << ", " << v.z << ", " << v.w; return str.str(); } )
			.py_write( ImVec4, x )
			.py_write( ImVec4, y )
			.py_write( ImVec4, z )
			.py_write( ImVec4, w );

		py_class1( ImDrawListSharedData );

		py_class1( ImDrawList, const ImDrawListSharedData* )
			.py_read( ImDrawList, CmdBuffer )
			.py_read( ImDrawList, IdxBuffer )
			.py_read( ImDrawList, VtxBuffer )
			.py_read( ImDrawList, Flags )
			.py_read( ImDrawList, Flags )
			.py_method( ImDrawList,
			PushClipRect,
			py_arg( clip_rect_min ),
			py_arg( clip_rect_max ),
			py_arg( intersect_with_current_clip_rect ) = false )
			.py_method( ImDrawList, PushClipRectFullScreen )
			.py_method( ImDrawList, PopClipRect )
			.py_method( ImDrawList, PushTextureID, py_arg( texture_id ) )
			.py_method( ImDrawList, PopTextureID )
			.py_method( ImDrawList, GetClipRectMin )
			.py_method( ImDrawList, GetClipRectMax )
			.py_method( ImDrawList, AddLine, py_arg( a ), py_arg( b ), py_arg( col ), py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, AddRect, py_arg( a ), py_arg( b ), py_arg( col ), py_arg( rounding ) = 0.0f, py_arg( rounding_corners_flags ) = (int)ImDrawCornerFlags_All, py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, AddRectFilled, py_arg( a ), py_arg( b ), py_arg( col ), py_arg( rounding ) = 0.0f, py_arg( rounding_corners_flags ) = (int)ImDrawCornerFlags_All )
			.py_method( ImDrawList, AddRectFilledMultiColor, py_arg( a ), py_arg( b ), py_arg( col_upr_left ), py_arg( col_upr_right ), py_arg( col_bot_right ), py_arg( col_bot_left ) )
			.py_method( ImDrawList, AddQuad, py_arg( a ), py_arg( b ), py_arg( c ), py_arg( d ), py_arg( col ), py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, AddQuadFilled, py_arg( a ), py_arg( b ), py_arg( c ), py_arg( d ), py_arg( col ) )
			.py_method( ImDrawList, AddTriangle, py_arg( a ), py_arg( b ), py_arg( c ), py_arg( col ), py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, AddTriangleFilled, py_arg( a ), py_arg( b ), py_arg( c ), py_arg( col ) )
			.py_method( ImDrawList, AddCircle, py_arg( centre ), py_arg( radius ), py_arg( col ), py_arg( num_segments ) = 12, py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, AddCircleFilled, py_arg( centre ), py_arg( radius ), py_arg( col ), py_arg( num_segments ) = 12 )
			.def( "AddText", ( void ( ImDrawList::* )( const ImFont*, float, const ImVec2&, ImU32, const char*, const char*, float, const ImVec4* ) ) & ImDrawList::AddText, py_arg( font ), py_arg( font_size ), py_arg( pos ), py_arg( col ), py_arg( text_begin ), py_arg( text_end ) = nullptr, py_arg( wrap_width ) = 0.0f, py_arg( cpu_fine_clip_rect ) = nullptr )
			.def( "AddText", ( void ( ImDrawList::* )( const ImVec2 & pos, ImU32 col, const char* text_begin, const char* text_end ) ) & ImDrawList::AddText, py_arg( pos ), py_arg( col ), py_arg( text_begin ), py_arg( text_end ) = nullptr )
			.py_method( ImDrawList, AddImage, py_arg( user_texture_id ), py_arg( a ), py_arg( b ), py_arg( uv_a ) = ImVec2( 0, 0 ), py_arg( uv_b ) = ImVec2( 1, 1 ), py_arg( col ) = IM_COL32_WHITE )
			.py_method( ImDrawList, AddImageQuad, py_arg( user_texture_id ), py_arg( a ), py_arg( b ), py_arg( c ), py_arg( d ), py_arg( uv_a ) = ImVec2( 0, 0 ), py_arg( uv_b ) = ImVec2( 1, 1 ), py_arg( uv_c ) = ImVec2( 0, 0 ), py_arg( uv_d ) = ImVec2( 1, 1 ), py_arg( col ) = IM_COL32_WHITE )
			.py_method( ImDrawList, AddImageRounded, py_arg( user_texture_id ), py_arg( a ), py_arg( b ), py_arg( uv_a ), py_arg( uv_b ), py_arg( col ), py_arg( rounding ), py_arg( rounding_corners ) = (int)ImDrawCornerFlags_All )
			.py_method( ImDrawList,
			AddPolyline, py_arg( points ), py_arg( num_points ), py_arg( col ), py_arg( closed ), py_arg( thickness ) )
			.py_method( ImDrawList, AddConvexPolyFilled, py_arg( points ), py_arg( num_points ), py_arg( col ) )
			.py_method( ImDrawList, AddBezierCurve, py_arg( pos0 ), py_arg( cp0 ), py_arg( cp1 ), py_arg( pos1 ), py_arg( col ), py_arg( thickness ), py_arg( num_segments ) )
			.py_method( ImDrawList, PathClear )
			.py_method( ImDrawList, PathLineTo, py_arg( pos ) )
			.py_method( ImDrawList, PathLineToMergeDuplicate, py_arg( pos ) )
			.py_method( ImDrawList, PathFillConvex, py_arg( coll ) )
			.py_method( ImDrawList, PathStroke, py_arg( col ), py_arg( closed ), py_arg( thickness ) = 1.0f )
			.py_method( ImDrawList, PathArcTo, py_arg( centre ), py_arg( radius ), py_arg( a_min ), py_arg( a_max ), py_arg( num_segments ) = 10 )
			.py_method( ImDrawList, PathArcToFast, py_arg( centre ), py_arg( radius ), py_arg( a_min_of_12 ), py_arg( a_max_of_12 ) )
			.py_method( ImDrawList, PathBezierCurveTo, py_arg( p1 ), py_arg( p2 ), py_arg( p3 ), py_arg( num_segments ) = 0 )
			.py_method( ImDrawList, PathRect, py_arg( rect_min ), py_arg( rect_max ), py_arg( rounding ), py_arg( rounding_corners_flags ) = (int)ImDrawCornerFlags_All )
			.py_method( ImDrawList, ChannelsSplit, py_arg( channels_count ) )
			.py_method( ImDrawList, ChannelsMerge )
			.py_method( ImDrawList, ChannelsSetCurrent, py_arg( channel_index ) );

		py_class1( ImGuiViewport )
			.py_read( ImGuiViewport, ID )
			.py_read( ImGuiViewport, Flags )
			.py_read( ImGuiViewport, Pos )
			.py_read( ImGuiViewport, Size )
			.py_read( ImGuiViewport, DpiScale )
			.py_read( ImGuiViewport, DrawData )
			.py_read( ImGuiViewport, ParentViewportId );

		py_class1( ImGuiViewportP )
			.py_read( ImGuiViewportP, ID )
			.py_read( ImGuiViewportP, Flags )
			.py_read( ImGuiViewportP, Pos )
			.py_read( ImGuiViewportP, Size )
			.py_read( ImGuiViewportP, DpiScale )
			.py_read( ImGuiViewportP, DrawData )
			.py_read( ImGuiViewportP, ParentViewportId )
			.py_read( ImGuiViewportP, Idx )
			.py_read( ImGuiViewportP, LastFrameActive )
			.py_read( ImGuiViewportP, LastFrameDrawLists )
			.py_read( ImGuiViewportP, LastFrontMostStampCount )
			.py_read( ImGuiViewportP, LastNameHash )
			.py_read( ImGuiViewportP, LastPos )
			.py_read( ImGuiViewportP, Alpha )
			.py_read( ImGuiViewportP, LastAlpha )
			.py_read( ImGuiViewportP, PlatformMonitor )
			.py_read( ImGuiViewportP, PlatformWindowCreated )
			.py_read( ImGuiViewportP, Window )
			//.py_read( ImGuiViewportP, DrawLists )
			.py_read( ImGuiViewportP, DrawDataP )
			.py_read( ImGuiViewportP, DrawDataBuilder )
			.py_read( ImGuiViewportP, LastPlatformPos )
			.py_read( ImGuiViewportP, LastPlatformSize )
			.py_read( ImGuiViewportP, LastRendererSize );

		py_class1( ImDrawDataBuilder )
			.py_read( ImDrawDataBuilder, Layers )
			.py_method( ImDrawDataBuilder, Clear )
			.py_method( ImDrawDataBuilder, ClearFreeMemory )
			.py_method( ImDrawDataBuilder, FlattenIntoSingleLayer );

		py_class1( ImGuiPlatformMonitor )
			.py_read( ImGuiPlatformMonitor, MainPos )
			.py_read( ImGuiPlatformMonitor, MainSize )
			.py_read( ImGuiPlatformMonitor, WorkPos )
			.py_read( ImGuiPlatformMonitor, WorkSize )
			.py_read( ImGuiPlatformMonitor, DpiScale );

		py_class1( ImGuiPlatformIO )
			.py_read( ImGuiPlatformIO, Monitors )
			.py_read( ImGuiPlatformIO, MainViewport )
			.py_read( ImGuiPlatformIO, Viewports );

		py_class1( ImGuiWindow, ImGuiContext*, const char* )
			.py_read( ImGuiWindow, Name )
			.py_read( ImGuiWindow, ID )
			.py_read( ImGuiWindow, Flags )
			.py_read( ImGuiWindow, FlagsPreviousFrame )
			.py_read( ImGuiWindow, WindowClass )
			.py_read( ImGuiWindow, Viewport )
			.py_read( ImGuiWindow, ViewportId )
			.py_read( ImGuiWindow, ViewportPos )
			.py_read( ImGuiWindow, ViewportAllowPlatformMonitorExtend )
			.py_read( ImGuiWindow, Pos )
			.py_read( ImGuiWindow, Size )
			.py_read( ImGuiWindow, SizeFull )
			.py_read( ImGuiWindow, WindowPadding )
			.py_read( ImGuiWindow, WindowRounding )
			.py_read( ImGuiWindow, WindowBorderSize )
			.py_read( ImGuiWindow, NameBufLen )
			.py_read( ImGuiWindow, MoveId )
			.py_read( ImGuiWindow, ChildId )
			.py_read( ImGuiWindow, Scroll )
			.py_read( ImGuiWindow, ScrollTarget )
			.py_read( ImGuiWindow, ScrollTargetCenterRatio )
			.py_read( ImGuiWindow, ScrollbarSizes )
			.py_read( ImGuiWindow, ScrollbarX )
			.py_read( ImGuiWindow, ScrollbarY )
			.py_read( ImGuiWindow, ViewportOwned )
			.py_read( ImGuiWindow, Active )
			.py_read( ImGuiWindow, WasActive )
			.py_read( ImGuiWindow, WriteAccessed )
			.py_read( ImGuiWindow, Collapsed )
			.py_read( ImGuiWindow, WantCollapseToggle )
			.py_read( ImGuiWindow, SkipItems )
			.py_read( ImGuiWindow, Appearing )
			.py_read( ImGuiWindow, Hidden )
			.py_read( ImGuiWindow, HasCloseButton )
			.py_read( ImGuiWindow, ResizeBorderHeld )
			.py_read( ImGuiWindow, BeginCount )
			.py_read( ImGuiWindow, BeginOrderWithinParent )
			.py_read( ImGuiWindow, BeginOrderWithinContext )
			.py_read( ImGuiWindow, PopupId )
			.py_read( ImGuiWindow, AutoFitFramesX )
			.py_read( ImGuiWindow, AutoFitFramesY )
			.py_read( ImGuiWindow, AutoFitOnlyGrows )
			.py_read( ImGuiWindow, AutoFitChildAxises )
			.py_read( ImGuiWindow, AutoPosLastDirection )
			//.py_read( ImGuiWindow, HiddenFramesCanSkipItems )
			//.py_read( ImGuiWindow, HiddenFramesCannotSkipItems )
			.py_read( ImGuiWindow, SetWindowPosAllowFlags )
			.py_read( ImGuiWindow, SetWindowSizeAllowFlags )
			.py_read( ImGuiWindow, SetWindowCollapsedAllowFlags )
			.py_read( ImGuiWindow, SetWindowDockAllowFlags )
			.py_read( ImGuiWindow, SetWindowPosVal )
			.py_read( ImGuiWindow, SetWindowPosPivot )
			.py_read( ImGuiWindow, DC )
			.py_read( ImGuiWindow, IDStack )
			.py_read( ImGuiWindow, ClipRect )
			.py_read( ImGuiWindow, OuterRectClipped )
			.py_read( ImGuiWindow, InnerClipRect )
			.py_read( ImGuiWindow, HitTestHoleSize )
			.py_read( ImGuiWindow, HitTestHoleOffset )
			.py_read( ImGuiWindow, ContentsRegionRect )
			.py_read( ImGuiWindow, LastFrameActive )
			//.py_read( ImGuiWindow, LastFrameJustFocused )
			.py_read( ImGuiWindow, ItemWidthDefault )
			.py_read( ImGuiWindow, MenuColumns )
			.py_read( ImGuiWindow, StateStorage )
			.py_read( ImGuiWindow, ColumnsStorage )
			.py_read( ImGuiWindow, FontWindowScale )
			.py_read( ImGuiWindow, FontDpiScale )
			.py_read( ImGuiWindow, SettingsIdx )
			.py_read( ImGuiWindow, DrawList )
			.py_read( ImGuiWindow, DrawListInst )
			.py_read( ImGuiWindow, ParentWindow )
			.py_read( ImGuiWindow, RootWindow )
			.py_read( ImGuiWindow, RootWindowDockStop )
			.py_read( ImGuiWindow, RootWindowForTitleBarHighlight )
			.py_read( ImGuiWindow, RootWindowForNav )
			.py_read( ImGuiWindow, NavLastChildNavWindow )
			.py_read( ImGuiWindow, DockNode )
			.py_read( ImGuiWindow, DockNodeAsHost )
			.py_read( ImGuiWindow, DockId )
			.py_read( ImGuiWindow, DockTabItemStatusFlags )
			.py_read( ImGuiWindow, DockTabItemRect )
			.py_read( ImGuiWindow, DockOrder )
			//.py_read( ImGuiWindow, DockIsActive )
			//.py_read( ImGuiWindow, DockTabIsVisible )
			//.py_read( ImGuiWindow, DockTabWantClose )
			.py_read( ImGuiWindow, NavLastIds )
			.py_read( ImGuiWindow, NavRectRel )
			.py_method( ImGuiWindow, Rect )
			.py_method( ImGuiWindow, CalcFontSize )
			.py_method( ImGuiWindow, TitleBarHeight )
			.py_method( ImGuiWindow, TitleBarRect )
			.py_method( ImGuiWindow, MenuBarHeight )
			.py_method( ImGuiWindow, MenuBarRect );

		py_class1( ImGuiContext, ImFontAtlas* )
			.py_read( ImGuiContext, Initialized )
			.py_read( ImGuiContext, FrameScopeActive )
			.py_read( ImGuiContext, FrameScopePushedImplicitWindow )
			.py_read( ImGuiContext, FontAtlasOwnedByContext )
			.py_read( ImGuiContext, IO )
			.py_read( ImGuiContext, PlatformIO )
			.py_read( ImGuiContext, Style )
			.py_read( ImGuiContext, ConfigFlagsForFrame )
			.py_read( ImGuiContext, Font )
			.py_read( ImGuiContext, FontSize )
			.py_read( ImGuiContext, FontBaseSize )
			.py_read( ImGuiContext, DrawListSharedData )
			.py_read( ImGuiContext, Time )
			.py_read( ImGuiContext, FrameCount )
			.py_read( ImGuiContext, FrameCountEnded )
			.py_read( ImGuiContext, FrameCountPlatformEnded )
			.py_read( ImGuiContext, FrameCountRendered )
			.py_read( ImGuiContext, Windows )
			.py_read( ImGuiContext, WindowsFocusOrder )
			.py_read( ImGuiContext, WindowsSortBuffer )
			.py_read( ImGuiContext, CurrentWindowStack )
			.py_read( ImGuiContext, WindowsById )
			.py_read( ImGuiContext, WindowsActiveCount )
			.py_read( ImGuiContext, CurrentWindow )
			.py_read( ImGuiContext, HoveredWindow )
			.py_read( ImGuiContext, HoveredRootWindow )
			.py_read( ImGuiContext, HoveredWindowUnderMovingWindow )
			.py_read( ImGuiContext, HoveredId )
			.py_read( ImGuiContext, HoveredIdAllowOverlap )
			.py_read( ImGuiContext, HoveredIdPreviousFrame )
			.py_read( ImGuiContext, HoveredIdTimer )
			.py_read( ImGuiContext, HoveredIdNotActiveTimer )
			.py_read( ImGuiContext, ActiveId )
			.py_read( ImGuiContext, ActiveIdPreviousFrame )
			.py_read( ImGuiContext, ActiveIdIsAlive )
			.py_read( ImGuiContext, ActiveIdTimer )
			.py_read( ImGuiContext, ActiveIdIsJustActivated )
			.py_read( ImGuiContext, ActiveIdAllowOverlap )
			.py_read( ImGuiContext, ActiveIdPreviousFrameIsAlive )
			.py_read( ImGuiContext, ActiveIdAllowNavDirFlags )
			.py_read( ImGuiContext, ActiveIdBlockNavInputFlags )
			.py_read( ImGuiContext, ActiveIdClickOffset )
			.py_read( ImGuiContext, ActiveIdWindow )
			.py_read( ImGuiContext, ActiveIdPreviousFrameWindow )
			.py_read( ImGuiContext, ActiveIdSource )
			.py_read( ImGuiContext, LastActiveId )
			.py_read( ImGuiContext, LastActiveIdTimer )
			.py_read( ImGuiContext, LastValidMousePos )
			.py_read( ImGuiContext, MovingWindow )
			.py_read( ImGuiContext, ColorModifiers )
			.py_read( ImGuiContext, StyleModifiers )
			.py_read( ImGuiContext, FontStack )
			.py_read( ImGuiContext, OpenPopupStack )
			.py_read( ImGuiContext, BeginPopupStack )
			.py_read( ImGuiContext, NextWindowData )
			.py_read( ImGuiContext, Viewports )
			.py_read( ImGuiContext, CurrentViewport )
			.py_read( ImGuiContext, MouseViewport )
			.py_read( ImGuiContext, MouseLastHoveredViewport )
			.py_read( ImGuiContext, PlatformLastFocusedViewport )
			//.py_read( ImGuiContext, ViewportFrontMostStampCount )
			//.py_read( ImGuiContext, ViewportFrontMostStampCount )
			.py_read( ImGuiContext, NavWindow )
			.py_read( ImGuiContext, NavId )
			.py_read( ImGuiContext, NavActivateId )
			.py_read( ImGuiContext, NavActivateDownId )
			.py_read( ImGuiContext, NavActivatePressedId )
			.py_read( ImGuiContext, NavInputId )
			.py_read( ImGuiContext, NavJustTabbedId )
			.py_read( ImGuiContext, NavJustMovedToId )
			.py_read( ImGuiContext, NavNextActivateId )
			.py_read( ImGuiContext, NavInputSource )
			.py_read( ImGuiContext, NavScoringRectScreen )
			.py_read( ImGuiContext, NavScoringCount )
			.py_read( ImGuiContext, NavWindowingTarget )
			.py_read( ImGuiContext, NavWindowingTargetAnim )
			.py_read( ImGuiContext, NavWindowingList )
			.py_read( ImGuiContext, NavWindowingTimer )
			.py_read( ImGuiContext, NavWindowingHighlightAlpha )
			.py_read( ImGuiContext, NavWindowingToggleLayer )
			.py_read( ImGuiContext, NavLayer )
			.py_read( ImGuiContext, NavIdTabCounter )
			.py_read( ImGuiContext, NavIdIsAlive )
			.py_read( ImGuiContext, NavMousePosDirty )
			.py_read( ImGuiContext, NavDisableHighlight )
			.py_read( ImGuiContext, NavDisableMouseHover )
			.py_read( ImGuiContext, NavAnyRequest )
			.py_read( ImGuiContext, NavInitRequest )
			.py_read( ImGuiContext, NavInitRequestFromMove )
			.py_read( ImGuiContext, NavInitResultId )
			.py_read( ImGuiContext, NavInitResultRectRel )
			.py_read( ImGuiContext, NavMoveFromClampedRefRect )
			.py_read( ImGuiContext, NavMoveRequest )
			.py_read( ImGuiContext, NavMoveRequestFlags )
			.py_read( ImGuiContext, NavMoveRequestForward )
			.py_read( ImGuiContext, NavMoveDir )
			.py_read( ImGuiContext, NavMoveDirLast )
			.py_read( ImGuiContext, NavMoveClipDir )
			.py_read( ImGuiContext, NavMoveResultLocal )
			.py_read( ImGuiContext, NavMoveResultLocalVisibleSet )
			.py_read( ImGuiContext, NavMoveResultOther )
			.py_read( ImGuiContext, FocusRequestCurrWindow )
			.py_read( ImGuiContext, FocusRequestNextWindow )
			.py_read( ImGuiContext, FocusRequestCurrCounterAll )
			.py_read( ImGuiContext, FocusRequestCurrCounterTab )
			.py_read( ImGuiContext, FocusRequestNextCounterAll )
			.py_read( ImGuiContext, FocusRequestNextCounterTab )
			.py_read( ImGuiContext, FocusTabPressed )
			.py_read( ImGuiContext, DimBgRatio )
			.py_read( ImGuiContext, MouseCursor )
			.py_read( ImGuiContext, DragDropActive )
			.py_read( ImGuiContext, DragDropWithinSourceOrTarget )
			.py_read( ImGuiContext, DragDropSourceFlags )
			.py_read( ImGuiContext, DragDropSourceFrameCount )
			.py_read( ImGuiContext, DragDropMouseButton )
			.py_read( ImGuiContext, DragDropPayload )
			.py_read( ImGuiContext, DragDropTargetRect )
			.py_read( ImGuiContext, DragDropTargetId )
			.py_read( ImGuiContext, DragDropAcceptFlags )
			.py_read( ImGuiContext, DragDropAcceptIdCurrRectSurface )
			.py_read( ImGuiContext, DragDropAcceptIdCurr )
			.py_read( ImGuiContext, DragDropAcceptIdPrev )
			.py_read( ImGuiContext, DragDropAcceptFrameCount )
			.py_read( ImGuiContext, DragDropPayloadBufHeap )
			.py_read( ImGuiContext, DragDropPayloadBufLocal )
			.py_read( ImGuiContext, TabBars )
			.py_read( ImGuiContext, CurrentTabBar )
			.py_read( ImGuiContext, CurrentTabBarStack )
			.py_read( ImGuiContext, InputTextState )
			.py_read( ImGuiContext, InputTextPasswordFont )
			.py_read( ImGuiContext, ColorEditOptions )
			.py_read( ImGuiContext, ColorPickerRef )
			.py_read( ImGuiContext, DragCurrentAccumDirty )
			.py_read( ImGuiContext, DragCurrentAccum )
			.py_read( ImGuiContext, DragSpeedDefaultRatio )
			.py_read( ImGuiContext, ScrollbarClickDeltaToGrabCenter )
			.py_read( ImGuiContext, TooltipOverrideCount )
			.py_read( ImGuiContext, PrivateClipboard )
			.py_read( ImGuiContext, MultiSelectScopeId )
			.py_read( ImGuiContext, PlatformImePos )
			.py_read( ImGuiContext, PlatformImeLastPos )
			.py_read( ImGuiContext, PlatformImePosViewport )
			//.py_read( ImGuiContext, DockContext )
			.py_read( ImGuiContext, SettingsLoaded )
			.py_read( ImGuiContext, SettingsDirtyTimer )
			.py_read( ImGuiContext, SettingsIniData )
			.py_read( ImGuiContext, SettingsHandlers )
			.py_read( ImGuiContext, SettingsWindows )
			.py_read( ImGuiContext, LogEnabled )
			.py_read( ImGuiContext, LogType )
			.py_read( ImGuiContext, LogFile )
			.py_read( ImGuiContext, LogBuffer )
			.py_read( ImGuiContext, LogLinePosY )
			.py_read( ImGuiContext, LogLineFirstItem )
			.py_read( ImGuiContext, LogDepthRef )
			.py_read( ImGuiContext, LogDepthToExpand )
			.py_read( ImGuiContext, LogDepthToExpandDefault )
			.py_read( ImGuiContext, FramerateSecPerFrame )
			.py_read( ImGuiContext, FramerateSecPerFrameIdx )
			.py_read( ImGuiContext, FramerateSecPerFrameAccum )
			.py_read( ImGuiContext, WantCaptureMouseNextFrame )
			.py_read( ImGuiContext, WantCaptureKeyboardNextFrame )
			.py_read( ImGuiContext, WantTextInputNextFrame )
			.py_read( ImGuiContext, TempBuffer );

		py_class1( ImGuiIO )
			.py_read( ImGuiIO, ConfigFlags )
			.py_read( ImGuiIO, BackendFlags )
			.py_read( ImGuiIO, DisplaySize )
			.py_read( ImGuiIO, DeltaTime )
			.py_read( ImGuiIO, IniSavingRate )
			.py_write( ImGuiIO, IniFilename )
			.py_write( ImGuiIO, LogFilename )
			.py_write( ImGuiIO, MouseDoubleClickTime )
			.py_write( ImGuiIO, MouseDoubleClickMaxDist )
			.py_write( ImGuiIO, MouseDragThreshold )
			.py_read( ImGuiIO, KeyMap )
			.py_write( ImGuiIO, KeyRepeatDelay )
			.py_write( ImGuiIO, KeyRepeatRate )
			.py_read( ImGuiIO, UserData )
			.py_read( ImGuiIO, Fonts )
			.py_write( ImGuiIO, FontGlobalScale )
			.py_write( ImGuiIO, FontAllowUserScaling )
			.py_write( ImGuiIO, FontDefault )
			.py_read( ImGuiIO, DisplayFramebufferScale )
			.py_write( ImGuiIO, ConfigDockingNoSplit )
			.py_write( ImGuiIO, ConfigDockingWithShift )
			.py_write( ImGuiIO, ConfigDockingTabBarOnSingleWindows )
			.py_write( ImGuiIO, ConfigDockingTransparentPayload )
			.py_write( ImGuiIO, ConfigViewportsNoAutoMerge )
			.py_write( ImGuiIO, ConfigViewportsNoTaskBarIcon )
			.py_write( ImGuiIO, ConfigViewportsNoDecoration )
			.py_write( ImGuiIO, ConfigViewportsNoDefaultParent )
			.py_write( ImGuiIO, MouseDrawCursor )
			.py_read( ImGuiIO, ConfigMacOSXBehaviors )
			.py_read( ImGuiIO, ConfigInputTextCursorBlink )
			.py_read( ImGuiIO, ConfigWindowsResizeFromEdges )
			.py_read( ImGuiIO, ConfigWindowsMoveFromTitleBarOnly )
			.py_write( ImGuiIO, BackendPlatformName )
			.py_write( ImGuiIO, BackendRendererName )
			.py_write( ImGuiIO, BackendPlatformUserData )
			.py_write( ImGuiIO, BackendLanguageUserData )
			.py_read( ImGuiIO, ClipboardUserData )
			.py_read( ImGuiIO, MousePos )
			.py_read( ImGuiIO, MouseDown )
			.py_read( ImGuiIO, MouseWheel )
			.py_read( ImGuiIO, MouseWheelH )
			.py_read( ImGuiIO, MouseHoveredViewport )
			.py_read( ImGuiIO, KeyCtrl )
			.py_read( ImGuiIO, KeyShift )
			.py_read( ImGuiIO, KeyAlt )
			.py_read( ImGuiIO, KeySuper )
			.py_read( ImGuiIO, KeysDown )
			.py_read( ImGuiIO, NavInputs )
			.py_read( ImGuiIO, WantCaptureMouse )
			.py_read( ImGuiIO, WantCaptureKeyboard )
			.py_read( ImGuiIO, WantTextInput )
			.py_read( ImGuiIO, WantSetMousePos )
			.py_read( ImGuiIO, WantSaveIniSettings )
			.py_read( ImGuiIO, NavActive )
			.py_read( ImGuiIO, NavVisible )
			.py_read( ImGuiIO, Framerate )
			.py_read( ImGuiIO, MetricsRenderVertices )
			.py_read( ImGuiIO, MetricsRenderIndices )
			.py_read( ImGuiIO, MetricsRenderWindows )
			.py_read( ImGuiIO, MetricsActiveWindows )
			.py_read( ImGuiIO, MetricsActiveAllocations )
			.py_read( ImGuiIO, MouseDelta )
			.py_read( ImGuiIO, MousePosPrev )
			.py_read( ImGuiIO, MouseClickedPos )
			.py_read( ImGuiIO, MouseClickedTime )
			.py_read( ImGuiIO, MouseClicked )
			.py_read( ImGuiIO, MouseDoubleClicked )
			.py_read( ImGuiIO, MouseReleased )
			.py_read( ImGuiIO, MouseDownOwned )
			.py_read( ImGuiIO, MouseDownDuration )
			.py_read( ImGuiIO, MouseDownDurationPrev )
			.py_read( ImGuiIO, MouseDragMaxDistanceAbs )
			.py_read( ImGuiIO, MouseDragMaxDistanceSqr )
			.py_read( ImGuiIO, KeysDownDuration )
			.py_read( ImGuiIO, KeysDownDurationPrev )
			.py_read( ImGuiIO, NavInputsDownDuration )
			.py_read( ImGuiIO, NavInputsDownDurationPrev )
			.py_read( ImGuiIO, InputQueueCharacters );

		py_class1( ImFontAtlas )
			.py_method( ImFontAtlas, AddFont )
			.py_method( ImFontAtlas, AddFontDefault )
			.py_method( ImFontAtlas, AddFontFromFileTTF )
			.py_method( ImFontAtlas, AddFontFromMemoryTTF )
			.py_method( ImFontAtlas, AddFontFromMemoryCompressedTTF )
			.py_method( ImFontAtlas, AddFontFromMemoryCompressedBase85TTF )
			.py_method( ImFontAtlas, ClearInputData )
			.py_method( ImFontAtlas, ClearTexData )
			.py_method( ImFontAtlas, ClearFonts )
			.py_method( ImFontAtlas, Clear )
			.py_method( ImFontAtlas, Build )
			//.py_method( ImFontAtlas, GetTexDataAsAlpha8 )
			//.py_method( ImFontAtlas, GetTexDataAsRGBA32 )
			.py_method( ImFontAtlas, IsBuilt )
			.py_method( ImFontAtlas, SetTexID )
			.py_method( ImFontAtlas, GetGlyphRangesDefault )
			.py_method( ImFontAtlas, GetGlyphRangesKorean )
			.py_method( ImFontAtlas, GetGlyphRangesJapanese )
			.py_method( ImFontAtlas, GetGlyphRangesChineseFull )
			.py_method( ImFontAtlas, GetGlyphRangesChineseSimplifiedCommon )
			.py_method( ImFontAtlas, GetGlyphRangesCyrillic )
			.py_method( ImFontAtlas, GetGlyphRangesThai )
			.py_method( ImFontAtlas, GetGlyphRangesVietnamese )
			.py_read( ImFontAtlas, Locked )
			.py_read( ImFontAtlas, Flags )
			.py_read( ImFontAtlas, TexID )
			.py_read( ImFontAtlas, TexDesiredWidth )
			.py_read( ImFontAtlas, TexGlyphPadding )
			.py_read( ImFontAtlas, TexPixelsAlpha8 )
			.py_read( ImFontAtlas, TexPixelsRGBA32 )
			.py_read( ImFontAtlas, TexWidth )
			.py_read( ImFontAtlas, TexHeight )
			.py_read( ImFontAtlas, TexUvScale )
			.py_read( ImFontAtlas, TexUvWhitePixel )
			.py_read( ImFontAtlas, Fonts )
			.py_read( ImFontAtlas, CustomRects )
			.py_read( ImFontAtlas, ConfigData )
			.py_read( ImFontAtlas, CustomRectIds );

		py_class1( ImGuiStyle )
			.py_write( ImGuiStyle,
			Alpha )					  // Global alpha applies to everything in ImGui.
			.py_write( ImGuiStyle, WindowPadding ) // Padding within a window.
			.py_write( ImGuiStyle,
			WindowRounding ) // Radius of window corners rounding. Set
							 // to 0.0f to have rectangular windows.
			.py_write( ImGuiStyle,
			WindowBorderSize ) // Thickness of border around windows.
							   // Generally set to 0.0f or 1.0f.
							   // (Other values are not well tested
							   // and more CPU/GPU costly).
			.py_write( ImGuiStyle,
			WindowMinSize ) // Minimum window size. This is a global
							// setting. If you want to constraint
							// individual windows, use
							// SetNextWindowSizeConstraints().
			.py_write( ImGuiStyle,
			WindowTitleAlign ) // Alignment for title bar text.
							   // Defaults to (0.0f,0.5f) for
							   // left-aligned,vertically centered.
			.py_write( ImGuiStyle,
			ChildRounding ) // Radius of child window corners
							// rounding. Set to 0.0f to have
							// rectangular windows.
			.py_write( ImGuiStyle,
			ChildBorderSize ) // Thickness of border around child
							  // windows. Generally set to 0.0f
							  // or 1.0f. (Other values are not well
							  // tested and more CPU/GPU costly).
			.py_write( ImGuiStyle,
			PopupRounding ) // Radius of popup window corners
							// rounding. (Note that tooltip windows
							// use WindowRounding)
			.py_write( ImGuiStyle,
			PopupBorderSize ) // Thickness of border around
							  // popup/tooltip windows. Generally set
							  // to 0.0f or 1.0f. (Other values are
							  // not well tested and more CPU/GPU
							  // costly).
			.py_write( ImGuiStyle,
			FramePadding ) // Padding within a framed rectangle
						   // (used by most widgets).
			.py_write( ImGuiStyle,
			FrameRounding ) // Radius of frame corners rounding. Set
							// to 0.0f to have rectangular frame
							// (used by most widgets).
			.py_write( ImGuiStyle,
			FrameBorderSize ) // Thickness of border around frames.
							  // Generally set to 0.0f or 1.0f.
							  // (Other values are not well tested
							  // and more CPU/GPU costly).
			.py_write( ImGuiStyle,
			ItemSpacing ) // Horizontal and vertical spacing
	// between widgets/lines.
			.py_write( ImGuiStyle,
			ItemInnerSpacing ) // Horizontal and vertical spacing
							   // between within elements of a
							   // composed widget (e.g. a slider and
							   // its label).
			.py_write( ImGuiStyle,
			TouchExtraPadding ) // Expand reactive bounding box for
								// touch-based system where touch
								// position is not accurate enough.
								// Unfortunately we don't sort widgets
								// so priority on overlap will always
								// be given to the first widget. So
								// don't grow this too much!
			.py_write( ImGuiStyle,
			IndentSpacing ) // Horizontal indentation when e.g.
							// entering a tree node. Generally ==
							// (FontSize + FramePadding.x*2).
			.py_write( ImGuiStyle,
			ColumnsMinSpacing ) // Minimum horizontal spacing
								// between two columns.
			.py_write( ImGuiStyle,
			ScrollbarSize ) // Width of the vertical scrollbar,
							// Height of the horizontal scrollbar.
			.py_write( ImGuiStyle,
			ScrollbarRounding ) // Radius of grab corners for
								// scrollbar.
			.py_write( ImGuiStyle,
			GrabMinSize ) // Minimum width/height of a grab
	// box for slider/scrollbar.
			.py_write( ImGuiStyle,
			GrabRounding ) // Radius of grabs corners rounding. Set to
						   // 0.0f to have rectangular slider grabs.
			.py_write( ImGuiStyle,
			TabRounding ) // Radius of upper corners of a tab. Set
	// to 0.0f to have rectangular tabs.
			.py_write( ImGuiStyle,
			TabBorderSize ) // Thickness of border around tabs.
			.py_write( ImGuiStyle,
			ButtonTextAlign ) // Alignment of button text when button
							  // is larger than text. Defaults to
							  // (0.5f, 0.5f) (centered).
			.py_write( ImGuiStyle,
			SelectableTextAlign ) // Alignment of selectable text
								  // when selectable is larger than
								  // text. Defaults to (0.0f, 0.0f)
								  // (top-left aligned).
			.py_write( ImGuiStyle,
			DisplayWindowPadding ) // Window position are clamped to
								   // be visible within the display
								   // area or monitors by at least
								   // this amount. Only applies to
								   // regular windows.
			.py_write( ImGuiStyle,
			DisplaySafeAreaPadding ) // If you cannot see the edges of
									 // your screen (e.g. on a TV)
									 // increase the safe area
									 // padding. Apply to
									 // popups/tooltips as well
									 // regular windows. NB: Prefer
									 // configuring your TV sets
									 // correctly!
			.py_write( ImGuiStyle,
			MouseCursorScale ) // Scale software rendered mouse
							   // cursor (when io.MouseDrawCursor is
							   // enabled). May be removed later.
			.py_write( ImGuiStyle,
			AntiAliasedLines ) // Enable anti-aliasing on
							   // lines/borders. Disable if you are
							   // really tight on CPU/GPU.
			.py_write( ImGuiStyle,
			AntiAliasedFill ) // Enable anti-aliasing on filled shapes
							  // (rounded rectangles, circles, etc.)
			.py_write( ImGuiStyle,
			CurveTessellationTol ) // Tessellation tolerance when
								   // using PathBezierCurveTo()
								   // without a specific number of
								   // segments. Decrease for highly
								   // tessellated curves (higher
								   // quality, more polygons),
								   // increase to reduce quality.
			.py_read( ImGuiStyle, Colors );

		/*
			BEGIN IMGUI

				Reasons why a function might be commented out /
		   unimplemented:

					DC: Doesn't compile, will need custom implementation
					NN: Not needed, handled by backed
					NR: Not ready, waiting on more backend to be implemented
					OF: Obsolete function, no need to support it
		*/
		mod.doc() = "ImGui Python Binding using pybind11";
	#include "ImGuiModule.inc"
	}
} // namespace Python