#pragma once

#include <functional>

#include <Common/ImGui/ImGuiEx.h>

namespace Paint
{
	enum class Buttons
	{
		LMB,
		RMB,
		MMB,
	};

	class DragOperation
	{
	public:
		using Callback_t = std::function<void()>;

		void SetButton( Buttons button );
		void SetStartedCallback( Callback_t callback );
		void SetChangedCallback( Callback_t callback );
		void SetFinishedCallback( Callback_t callback );

		void Tick();

		bool Dragging() const { return dragging; }

		ImVec2 begin;
		ImVec2 last;
		ImVec2 end;

	private:
		Callback_t started = nullptr;
		Callback_t changed = nullptr;
		Callback_t finished = nullptr;
		Buttons button;
		bool dragging = false;
	};
}