#pragma once

#include <initializer_list>

#include <Common/Include/ImGui/imgui.h>
#include <Common/Include/ImGui/imgui_internal.h>
#include <Common/Utility/Finally.h>

static inline bool operator==( const ImVec2& lhs, const ImVec2& rhs ) { return lhs.x == rhs.x && lhs.y == rhs.y; }
static inline bool operator!=( const ImVec2& lhs, const ImVec2& rhs ) { return !( lhs == rhs ); }

namespace ImGuiEx
{
	bool Checkbox( const char* label, bool b );
	bool Button( const char* label, bool enabled, const ImVec2& size = ImVec2( 0, 0 ) );

	Finally Begin( const char* name, bool* open = nullptr, ImGuiWindowFlags flags = 0 );
	Finally BeginGroup();
	Finally BeginChild( const char* str_id, const ImVec2& size = ImVec2( 0, 0 ), bool border = false, ImGuiWindowFlags flags = 0 );
	Finally BeginChildFrame( const char* str_id, const ImVec2& size, ImGuiWindowFlags flags = 0 );
	Finally BeginMainMenuBar();
	Finally BeginMenuBar();
	Finally BeginMenu( const char* label, const bool enabled = true );
	Finally BeginCombo( const char* label, const char* preview_value, ImGuiComboFlags flags = 0 );
	Finally BeginDragDropSource( ImGuiDragDropFlags flags = 0 );
	Finally BeginDragDropTarget();
	Finally BeginPopupModal( const char* name, bool* p_open = NULL, ImGuiWindowFlags flags = 0 );
	Finally BeginPopup( const char* name, ImGuiWindowFlags flags = 0 );

	std::unique_ptr<Finally> Indent( const float width = 0.0f );
	std::unique_ptr<Finally> PushID( const char* str_id );
	std::unique_ptr<Finally> PushID( const int id );
	std::unique_ptr<Finally> PushStyleColour( const ImGuiCol type, const ImU32 colour );
	std::unique_ptr<Finally> PushStyleColours( std::initializer_list<std::pair<ImGuiCol, ImU32>> list );
	std::unique_ptr<Finally> PushItemWidth( const float width );

    std::tuple<float,float,float> ColorConvertRGBtoHSV(float r, float g, float b, float& out_h, float& out_s, float& out_v);

	void StyleColorsGemini();
}

#define ImGuiExScope auto scope = ImGuiEx