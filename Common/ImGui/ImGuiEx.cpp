#include "Modules/ImGuiModule.h"

#include "ImGuiEx.h"

namespace ImGuiEx
{
	bool Checkbox( const char* label, bool b )
	{
		return ImGui::Checkbox( label, &b );
	}

	bool Button( const char* label, bool enabled, const ImVec2& size /*= ImVec2( 0, 0 ) */ )
	{
		auto scoped_colour = enabled ? nullptr : PushStyleColours(
			{
				{ ImGuiCol_Button, 0xFF555555 },
				{ ImGuiCol_ButtonHovered, 0xFF555555 },
				{ ImGuiCol_ButtonActive, 0xFF555555 },
			} );

		return ImGui::Button( label, size ) && enabled;
	}

	Finally Begin( const char* name, bool* open, ImGuiWindowFlags flags )
	{
		if( !ImGui::Begin( name, open, flags ) )
		{
			ImGui::End();
			return {};
		}

		return Finally( ImGui::End );
	}

	Finally BeginGroup()
	{
		ImGui::BeginGroup();
		return Finally( ImGui::EndGroup );
	}

	Finally BeginChild( const char* str_id, const ImVec2& size /*= ImVec2( 0, 0 )*/, bool border /*= false*/, ImGuiWindowFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginChild( str_id, size, border, flags ) )
		{
			ImGui::EndChild();
			return {};
		}

		return Finally( ImGui::EndChild );
	}

	Finally BeginChildFrame( const char* str_id, const ImVec2& size, ImGuiWindowFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginChildFrame( ImGui::GetID( str_id ), size, flags ) )
		{
			ImGui::EndChildFrame();
			return {};
		}

		return Finally( ImGui::EndChildFrame );
	}

	Finally BeginMainMenuBar()
	{
		if( !ImGui::BeginMainMenuBar() )
			return {};

		return Finally( ImGui::EndMainMenuBar );
	}

	Finally BeginMenuBar()
	{
		if( !ImGui::BeginMenuBar() )
			return {};

		return Finally( ImGui::EndMenuBar );
	}

	Finally BeginMenu( const char* label, const bool enabled )
	{
		if( !ImGui::BeginMenu( label, enabled ) )
			return {};

		return Finally( ImGui::EndMenu );
	}

	Finally BeginCombo( const char* label, const char* preview_value, ImGuiComboFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginCombo( label, preview_value, flags ) )
			return {};

		return Finally( ImGui::EndCombo );
	}

	Finally BeginDragDropSource( ImGuiDragDropFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginDragDropSource( flags ) )
			return {};

		return Finally( ImGui::EndDragDropSource );
	}

	Finally BeginDragDropTarget()
	{
		if( !ImGui::BeginDragDropTarget() )
			return {};

		return Finally( ImGui::EndDragDropTarget );
	}

	Finally BeginPopupModal( const char* name, bool* p_open /*= NULL*/, ImGuiWindowFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginPopupModal( name, p_open, flags ) )
			return {};

		return Finally( ImGui::EndPopup );
	}

	Finally BeginPopup( const char* name, ImGuiWindowFlags flags /*= 0 */ )
	{
		if( !ImGui::BeginPopup( name, flags ) )
			return {};

		return Finally( ImGui::EndPopup );
	}

	std::unique_ptr<Finally> Indent( const float width )
	{
		ImGui::Indent( width );
		return make_finally( [=]() { ImGui::Unindent( width ); } );
	}

	std::unique_ptr<Finally> PushID( const char* str_id )
	{
		ImGui::PushID( str_id );
		return make_finally( ImGui::PopID );
	}

	std::unique_ptr<Finally> PushID( const int id )
	{
		ImGui::PushID( id );
		return make_finally( ImGui::PopID );
	}

	std::unique_ptr<Finally> PushStyleColour( const ImGuiCol type, const ImU32 colour )
	{
		ImGui::PushStyleColor( type, colour );
		return make_finally( []() { ImGui::PopStyleColor( 1 ); } );
	}

	std::unique_ptr<Finally> PushStyleColours( std::initializer_list<std::pair<ImGuiCol, ImU32>> list )
	{
		for( auto& pair : list )
			ImGui::PushStyleColor( pair.first, pair.second );

		const auto size = list.size();
		return make_finally( [=]() { ImGui::PopStyleColor( size ); } );
	}

	std::unique_ptr<Finally> PushItemWidth( const float width )
	{
		ImGui::PushItemWidth( width );
		return make_finally( [=]() { ImGui::PopItemWidth(); } );
	}

	void StyleColorsGemini()
	{
		ImVec4* colors = ImGui::GetStyle().Colors;
		colors[ImGuiCol_Text]                   = ImVec4(0.90f, 0.90f, 0.90f, 1.00f);
		colors[ImGuiCol_TextDisabled]           = ImVec4(0.57f, 0.57f, 0.57f, 1.00f);
		colors[ImGuiCol_WindowBg]               = ImVec4(0.25f, 0.25f, 0.25f, 1.00f);
		colors[ImGuiCol_ChildBg]                = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_PopupBg]                = ImVec4(0.11f, 0.11f, 0.14f, 0.92f);
		colors[ImGuiCol_Border]                 = ImVec4(0.50f, 0.50f, 0.50f, 0.50f);
		colors[ImGuiCol_BorderShadow]           = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg]                = ImVec4(0.00f, 0.00f, 0.00f, 0.39f);
		colors[ImGuiCol_FrameBgHovered]         = ImVec4(0.47f, 0.68f, 0.69f, 0.40f);
		colors[ImGuiCol_FrameBgActive]          = ImVec4(1.00f, 0.50f, 0.00f, 0.69f);
		colors[ImGuiCol_TitleBg]                = ImVec4(0.71f, 0.31f, 0.00f, 0.83f);
		colors[ImGuiCol_TitleBgActive]          = ImVec4(0.84f, 0.28f, 0.12f, 0.87f);
		colors[ImGuiCol_TitleBgCollapsed]       = ImVec4(1.00f, 0.38f, 0.00f, 0.20f);
		colors[ImGuiCol_MenuBarBg]              = ImVec4(0.55f, 0.43f, 0.40f, 0.80f);
		colors[ImGuiCol_ScrollbarBg]            = ImVec4(0.00f, 0.00f, 0.00f, 0.60f);
		colors[ImGuiCol_ScrollbarGrab]          = ImVec4(0.00f, 0.90f, 0.83f, 0.30f);
		colors[ImGuiCol_ScrollbarGrabHovered]   = ImVec4(0.00f, 1.00f, 0.83f, 0.40f);
		colors[ImGuiCol_ScrollbarGrabActive]    = ImVec4(0.00f, 0.36f, 0.32f, 0.60f);
		colors[ImGuiCol_CheckMark]              = ImVec4(0.90f, 0.90f, 0.90f, 0.50f);
		colors[ImGuiCol_SliderGrab]             = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
		colors[ImGuiCol_SliderGrabActive]       = ImVec4(0.80f, 0.61f, 0.39f, 0.60f);
		colors[ImGuiCol_Button]                 = ImVec4(0.00f, 0.00f, 0.00f, 0.62f);
		colors[ImGuiCol_ButtonHovered]          = ImVec4(0.00f, 0.55f, 0.47f, 0.79f);
		colors[ImGuiCol_ButtonActive]           = ImVec4(0.05f, 0.87f, 0.75f, 1.00f);
		colors[ImGuiCol_Header]                 = ImVec4(0.00f, 0.70f, 0.64f, 0.45f);
		colors[ImGuiCol_HeaderHovered]          = ImVec4(1.00f, 0.44f, 0.00f, 0.80f);
		colors[ImGuiCol_HeaderActive]           = ImVec4(0.82f, 0.33f, 0.10f, 0.80f);
		colors[ImGuiCol_Separator]              = ImVec4(0.68f, 0.31f, 0.02f, 1.00f);
		colors[ImGuiCol_SeparatorHovered]       = ImVec4(0.98f, 0.40f, 0.00f, 1.00f);
		colors[ImGuiCol_SeparatorActive]        = ImVec4(1.00f, 0.35f, 0.00f, 1.00f);
		colors[ImGuiCol_ResizeGrip]             = ImVec4(1.00f, 1.00f, 1.00f, 0.16f);
		colors[ImGuiCol_ResizeGripHovered]      = ImVec4(0.85f, 0.27f, 0.00f, 0.60f);
		colors[ImGuiCol_ResizeGripActive]       = ImVec4(1.00f, 0.41f, 0.00f, 0.90f);
		colors[ImGuiCol_Tab]                    = ImVec4(0.74f, 0.26f, 0.00f, 0.79f);
		colors[ImGuiCol_TabHovered]             = ImVec4(1.00f, 0.50f, 0.00f, 0.80f);
		colors[ImGuiCol_TabActive]              = ImVec4(1.00f, 0.38f, 0.00f, 0.84f);
		colors[ImGuiCol_TabUnfocused]           = ImVec4(0.57f, 0.38f, 0.28f, 0.82f);
		colors[ImGuiCol_TabUnfocusedActive]     = ImVec4(0.47f, 0.24f, 0.14f, 0.84f);
		colors[ImGuiCol_DockingPreview]         = ImVec4(0.83f, 0.34f, 0.00f, 0.31f);
		colors[ImGuiCol_DockingEmptyBg]         = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
		colors[ImGuiCol_PlotLines]              = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered]       = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogram]          = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered]   = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		colors[ImGuiCol_TextSelectedBg]         = ImVec4(0.00f, 0.71f, 0.70f, 0.35f);
		colors[ImGuiCol_DragDropTarget]         = ImVec4(1.00f, 1.00f, 1.00f, 0.90f);
		colors[ImGuiCol_NavHighlight]           = ImVec4(0.94f, 0.42f, 0.01f, 0.80f);
		colors[ImGuiCol_NavWindowingHighlight]  = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
		colors[ImGuiCol_NavWindowingDimBg]      = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
		colors[ImGuiCol_ModalWindowDimBg]       = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);
	}
}