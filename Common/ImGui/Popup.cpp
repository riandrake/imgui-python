#include "Popup.h"

#include <algorithm>

namespace ImGuiEx
{
	Popup::Popup( const char* name )
		: name( name )
	{

	}

	void PopupHandler::AddPopup( std::unique_ptr<Popup> popup )
	{
		if( popup )
			popups.emplace_back( std::move( popup ), false );
	}

	void PopupHandler::Render()
	{
		auto iter = std::remove_if( popups.begin(), popups.end(), []( auto& pair )
		{
			auto& [popup, opened] = pair;

			if( !opened )
			{
				ImGui::OpenPopup( popup->GetName() );
				opened = true;
			}

			return !popup || popup->Render();
		} );
		popups.erase( iter, popups.end() );
	}
};