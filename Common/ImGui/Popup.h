#pragma once

#include <memory>
#include <vector>

#include <Common/ImGui/ImGuiEx.h>

namespace ImGuiEx
{
	class Popup
	{
	public:
		Popup( const char* name );

		const char* GetName() const { return name; }

		virtual bool Render() = 0;

	private:
		const char* name;
	};

	class PopupHandler
	{
	public:
		void AddPopup( std::unique_ptr<Popup> popup );

		void Render();

	private:
		std::vector<std::pair<std::unique_ptr<Popup>, bool>> popups;
	};
};