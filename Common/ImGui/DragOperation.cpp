#include "DragOperation.h"

namespace Paint
{
	void DragOperation::SetButton( Buttons button )
	{
		this->button = button;
	}

	void DragOperation::SetStartedCallback( Callback_t callback )
	{
		started = callback;
	}

	void DragOperation::SetChangedCallback( Callback_t callback )
	{
		changed = callback;
	}

	void DragOperation::SetFinishedCallback( Callback_t callback )
	{
		finished = callback;
	}

	void DragOperation::Tick()
	{
		if( ImGui::IsMouseDragging( static_cast<int>( button ), 0.0f ) )
		{
			if( !dragging )
			{
				begin = ImGui::GetMousePos();
				last = begin;
				dragging = true;

				if( started )
					started();
			}

			end = ImGui::GetMousePos();

			if( changed && last != end )
				changed();

			last = end;
		}
		else if( dragging )
		{
			end = ImGui::GetMousePos();
			dragging = false;

			if( finished )
				finished();
		}
	}
}