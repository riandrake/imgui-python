#pragma once

#define GENERATE
#define REFERENCE

#include <pybind11/pybind11.h>
#include <pybind11/complex.h>
#include <pybind11/functional.h>
#include <pybind11/operators.h>
#include <pybind11/embed.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#define py_write( cls, x ) def_readwrite( #x, &cls::x )
#define py_read( cls, x ) def_readonly( #x, &cls::x )
#define py_func( x, ... ) def( #x, &x, __VA_ARGS__ )
#define py_method( cls, x, ... ) def( #x, &cls::x, __VA_ARGS__ )
#define py_method2( name, cls, x, ... ) def( name, &cls::x, __VA_ARGS__ )
#define py_arg( a ) py::arg( #a )
#define py_class1( cls, ... ) py::class_<cls>( mod, #cls ).def( py::init<__VA_ARGS__>() )
#define py_class2( cls1, cls2, ... ) \
	py::class_<cls1, cls2>( mod, #cls1, py::dynamic_attr() ).def( py::init<__VA_ARGS__>() )
#define py_implements( ret, cls, func, ... )                   \
	override                                                   \
	{                                                          \
		PYBIND11_OVERLOAD( ret, cls, func, __VA_ARGS__ );      \
	}
#define py_implements_pure( ret, cls, func, ... )              \
	override                                                   \
	{                                                          \
		PYBIND11_OVERLOAD_PURE( ret, cls, func, __VA_ARGS__ ); \
	}