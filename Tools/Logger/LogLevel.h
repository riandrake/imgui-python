#pragma once

#include <string>

namespace Logger
{
	struct LogLevel
	{
		enum Levels
		{
			COMMAND,
			DEBUG,
			INFO,
			WARN,
			CRIT,
			Count
		};

		static const char* LevelToString( const Levels level );
	};
}