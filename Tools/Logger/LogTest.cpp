#include "LogTest.h"

#include <Windows.h>
#include <fstream>
#include <algorithm>
#include <stdlib.h>
#include <random>

#include <Common/ImGui/ImGuiEx.h>

namespace Logger
{
	LogTest::LogTest()
		: sock( AF_INET, SOCK_DGRAM, IPPROTO_UDP )
	{
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htonl( INADDR_LOOPBACK );
		addr.sin_port = htons( 50007 );
	}

	void LogTest::OnInitialise( const std::string_view& args )
	{
		auto file = std::ifstream( "log.txt" );

		std::string line;
		while( std::getline( file, line ) )
			log_lines.push_back( line );

		std::random_device rng;
		std::mt19937 urng( rng() );
		std::shuffle( log_lines.begin(), log_lines.end(), urng );
	}

	void LogTest::OnRender()
	{
		if( ImGui::Begin( "Log Tester", &open, ImGuiWindowFlags_AlwaysAutoResize ) )
		{
			if( ImGui::SliderFloat( "MinRate", &min_rate, 0.016f, 1.0f ) )
			{
				if( min_rate > max_rate )
					max_rate = min_rate;
			}

			if( ImGui::SliderFloat( "MaxRate", &max_rate, 0.016f, 1.0f ) )
			{
				if( max_rate < min_rate )
					min_rate = max_rate;
			}

			ImGui::SliderInt( "PerTick", &per_tick, 1, 100 );
			ImGui::Checkbox( "Pause", &pause );

			if( !pause )
			{
				log_timer -= ImGui::GetIO().DeltaTime;
				if( log_timer < 0.0f )
				{
					log_timer += min_rate + ( max_rate - min_rate ) * static_cast<float>( rand() ) / RAND_MAX;

					for( int i = 0; i < per_tick; ++i )
					{
						Log( log_lines[log_iter++] );

						if( log_iter == log_lines.size() )
						{
							log_iter = 0;
							std::random_device rng;
							std::mt19937 urng( rng() );
							std::shuffle( log_lines.begin(), log_lines.end(), urng );
						}
					}
				}
			}
		}
		ImGui::End();
	}

	void LogTest::Log( const std::string_view& message )
	{
		auto to_send = message;
		if( to_send.size() > 1499 )
			to_send = to_send.substr( 0, 1499 );

		const auto sent = sendto( sock.sock, message.data(), static_cast<int>( message.size() ), 0, reinterpret_cast<const sockaddr*>( &addr ), sizeof( addr ) );
		if( sent == SOCKET_ERROR )
			std::cout << "Send error: " << WSAGetLastError() << std::endl;
	}
}