/*
	Used to sporadically print logs to the console, for testing purposes
*/

#pragma once

#include <vector>
#include <string>

#include <Common/Program/BaseApp.h>

#include "Network/Socket.h"

namespace Logger
{
	class LogTest : public App::Base
	{
	public:
		LogTest();

		virtual void OnInitialise( const std::string_view& args ) override;
		virtual void OnRender() override;

		void Log( const std::string_view& message );

		virtual const char* ProgramName() const { return "UDPLoggerTest"; }
		virtual const char* WindowName() const { return "UDPLoggerTest"; }

	protected:
		Network::WinSock winsock;
		Network::Socket sock;
		sockaddr_in addr;

		std::vector<std::string> log_lines;
		unsigned log_iter = 0;
		int per_tick = 1;
		float log_timer = 0.0f;
		float min_rate = 0.016f;
		float max_rate = 0.016f;
		bool pause = false;
	};
}