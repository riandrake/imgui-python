#pragma once

#include <memory>
#include <map>
#include <sstream>
#include <string_view>

#include <fmt/format.h>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>

#include <Common/ImGui/ImGuiEx.h>
#include <Common/Program/BaseApp.h>
#include <Common/Utility/Logging.h>
#include <Common/Utility/WrapVector.h>

#include "Network/UDPListener.h"

#include "LogWindow.h"
#include "LogTest.h"
#include "LogLine.h"

namespace py = pybind11;

namespace Logger
{
	class LogApp : public App::Base
	{
	public:
		enum { MaxLines = 100'000 };

		LogApp();

		static void LogUDP( const std::string_view& message );
		static void Log( const LogLevel::Levels level, const std::string_view& message );

		static LogApp* Get();

		virtual const char* ProgramName() const { return "LoggerApp"; }
		virtual const char* WindowName() const { return "Logger"; }

		void AddWindow( const std::string_view& name = "NewWindow" );
		void AddMessage( const std::string_view& message );
		void Clear();
		void Handle( const std::string_view& message );

		using LogWindow_t = std::unique_ptr<LogWindow>;
		const std::vector<LogWindow_t>& GetWindows() const { return log_windows; }
		const WrapVector<LogLine>& GetLines() const { return lines; }

		py::module& GetColourModule() { return colour_module; }

	protected:
		virtual void ProcessCommandLine( const std::string& command );
		virtual void OnInitialise( const std::string_view& args ) override;
		virtual void OnPostInitialise() override;
		virtual void OnDestroy() override;
		virtual void OnRender() override;

		py::scoped_interpreter interpreter;
		py::module colour_module;

		std::vector<LogWindow_t> log_windows;
		WrapVector<LogLine> lines;

		std::unique_ptr<Network::UDPListener> udp_listener;

		unsigned line_count = 0;
	};
}