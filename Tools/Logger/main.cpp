#include <Common/Program/ProgramMain.h>

#include "LogTest.h"
#include "LogApp.h"

int __stdcall WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd )
{
	Logger::LogApp app;
	Main::DoMain( app, hInstance, hPrevInstance, lpCmdLine, nShowCmd, false );
}