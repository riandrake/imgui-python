#pragma once

#include <Common/Python/Module.h>

namespace Python::Modules::Logger
{
	void Initialise( pybind11::module& mod );
}