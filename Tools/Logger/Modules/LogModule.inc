;
py::enum_<LogColumns>(mod, "LogColumns", py::arithmetic(), "")
	.value("LineNumber", LineNumber, "")
	.value("Time", Time, "")
	.value("Level", Level, "")
	.value("Subsystem", Subsystem, "")
	.value("PID", PID, "")
	.value("Message", Message, "")
	.value("NumColumns", NumColumns, "")
	.export_values()
;
