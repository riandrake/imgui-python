#include "LogModule.h"
#include "Tools/Logger/LogLine.h"

namespace py = pybind11;

namespace Python::Modules::Logger
{
	void Initialise( pybind11::module& mod )
	{
	#include "LogModule.inc"
	}
}