#pragma once

#include <iostream>
#include <string_view>

#include <WinSock.h>
#pragma comment( lib, "Ws2_32.lib" )

namespace Network
{
	struct WinSock
	{
		WinSock();
		~WinSock();
	};

	struct Socket
	{
		Socket( int af, int type, int protocol );
		~Socket();

		SOCKET sock;
	};
}