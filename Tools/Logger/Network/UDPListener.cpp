#include "UDPListener.h"

#include <assert.h>

#include <Common/Utility/Logging.h>

namespace Network
{
	UDPListener::UDPListener( std::function<void( std::string_view )> callback ) : sock( AF_INET, SOCK_DGRAM, IPPROTO_UDP )
		, callback( callback )
	{
		assert( callback );

		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons( 50007 );

		if( bind( sock.sock, reinterpret_cast<const sockaddr*>( &addr ), sizeof( addr ) ) == SOCKET_ERROR )
		{
			Log::Crit( "Bind error: {}", WSAGetLastError() );
			return;
		}

		Log::Info( "UDPListener initialised" );
	}

	UDPListener::~UDPListener()
	{
		Log::Info( "UDPListener destroyed" );
	}

	int UDPListener::Update()
	{
		fd_set set;
		struct timeval timeout;
		FD_ZERO( &set ); /* clear the set */
		FD_SET( sock.sock, &set ); /* add our file descriptor to the set */
		timeout.tv_usec = 1;

		int rv = select( sock.sock + 1, &set, NULL, NULL, &timeout );

		if( rv == SOCKET_ERROR )
		{
			Log::Crit( "Select error: {}", WSAGetLastError() );
			return -1;
		}
		else if( rv == 0 )
		{
			return 0;
		}

		const int received = recv( sock.sock, buffer, sizeof( buffer ) - 1, 0 );
		if( received == SOCKET_ERROR )
		{
			Log::Crit( "Receive error: {}", WSAGetLastError() );
			return 1;
		}

		if( callback )
		{
			const auto view = std::string_view( buffer, received );
			callback( view );
		}

		return 0;
	}
}