#include "Socket.h"

#include <Common/Utility/Logging.h>

namespace Network
{
	WinSock::WinSock()
	{
		WSADATA wsadata;
		if( WSAStartup( MAKEWORD( 2, 2 ), &wsadata ) )
		{
			Log::Crit( "Failed to start winsock" );
			return;
		}

		Log::Info( "Winsock created" );
	}

	WinSock::~WinSock()
	{
		WSACleanup();
		Log::Info( "Winsock destroyed" );
	}

	Socket::Socket( int af, int type, int protocol ) : sock( socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP ) )
	{
		if( sock == INVALID_SOCKET )
		{
			Log::Crit( "Failed to create socket" );
			return;
		}

		Log::Info( "Socket created" );
	}

	Socket::~Socket()
	{
		closesocket( sock );
		Log::Crit( "Socket destroyed" );
	}
}