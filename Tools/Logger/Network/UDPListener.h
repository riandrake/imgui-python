#pragma once

#include <functional>

#include "Socket.h"

namespace Network
{
	class UDPListener
	{
		std::function<void( const std::string_view& )> callback;
		WinSock winsock;
		Socket sock;
		char buffer[1500];

	public:
		UDPListener( std::function<void( std::string_view )> callback );
		~UDPListener();

		int Update();
	};
}