#include "LogLine.h"

#include <stdexcept>

#include <pybind11/pybind11.h>

#include "LogApp.h"

namespace Logger
{
	float LogLine::ColumnWidths[LogColumns::NumColumns] = { };

	LogLine::LogLine( const std::string& raw, const unsigned line_number )
		: line_number( line_number )
	{
		using namespace std::string_literals;

		try
		{
			const auto time_iter = raw.find( ' ' );
			if( time_iter == -1 )
				throw std::runtime_error( "Invalid log line: time" );

			const auto time_iter_end = raw.find( ' ', time_iter + 1 );
			if( time_iter_end == -1 )
				throw std::runtime_error( "Invalid log line: time" );

			const auto level_iter = raw.find( '[', time_iter_end );
			if( level_iter == -1 )
				throw std::runtime_error( "Invalid log line: level" );

			const auto subsystem_iter = raw.find( ' ', level_iter );
			if( subsystem_iter == -1 )
				throw std::runtime_error( "Invalid log line: subsystem" );

			const auto pid_iter = raw.find( ' ', subsystem_iter + 1 );
			if( pid_iter == -1 )
				throw std::runtime_error( "Invalid log line: pid" );

			const auto message_iter = raw.find( ']', pid_iter );
			if( message_iter == -1 )
				throw std::runtime_error( "Invalid log line: message" );

			source.reserve( raw.size() );

			offsets[LineNumber] = source.size();
			source += std::to_string( line_number ) + "\0"s;

			auto add = [&]( const size_t begin, const size_t end = std::string::npos )
			{
				source += raw.substr( begin, end >= 0 ? end - begin : -1 ) + "\0"s;
			};

			// Add time
			offsets[Time] = source.size();
			add( time_iter + 1, time_iter_end );

			// Add level
			offsets[Level] = source.size();
			add( level_iter + 1, subsystem_iter );

			// Add subsystem
			offsets[Subsystem] = source.size();
			add( subsystem_iter + 1, pid_iter );

			// Add pid
			offsets[PID] = source.size();
			add( pid_iter + 1, message_iter );

			// Add message
			offsets[Message] = source.size();
			add( message_iter + 2 );

			auto& colour_mod = Logger::LogApp::Get()->GetColourModule();
			auto func = colour_mod.attr( "get_colour" );

			for( int i = 0; i < NumColumns; ++i )
			{
				const std::string_view& msg = Get( static_cast<LogColumns>( i ) );
				const float width = ImGui::CalcTextSize( msg.data(), msg.data() + msg.size() ).x;

				ColumnWidths[i] = std::max( width, ColumnWidths[i] );

				colours[i] = py::cast<ImVec4>( func( msg, i ) );
			}
		}
		catch( const std::runtime_error& e )
		{
			for( auto& offset : offsets )
				offset = -1;

			offsets[Message] = 0;
			source = raw;

			for( int i = 0; i < NumColumns; ++i )
				colours[i] = ImVec4( 1, 1, 1, 1 );
		}
	}

	float LogLine::GetColumnWidth( const LogColumns column )
	{
		return ColumnWidths[column] + 25.0f;
	}

	const char* LogLine::Get( LogColumns column ) const
	{
		const auto offset = offsets[column];
		if( offset == -1 )
			return "";

		return &source[offset];
	}

	const ImVec4& LogLine::GetColour( const LogColumns column ) const
	{
		return colours[column];
	}
}