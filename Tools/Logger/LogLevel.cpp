#include "LogLevel.h"

#include <stdexcept>

namespace Logger
{
	const char* LogLevel::LevelToString( const LogLevel::Levels level )
	{
		switch( level )
		{
		case COMMAND: return "COMMAND";
		case DEBUG: return "DEBUG";
		case INFO:  return "INFO";
		case WARN:  return "WARN";
		case CRIT:  return "CRIT";
		}

		throw std::runtime_error( "Invalid log level" );
	}
}