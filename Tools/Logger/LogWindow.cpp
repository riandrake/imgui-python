#include "LogWindow.h"
#include <ImGui/imgui.h>
#include <ImGui/imgui_internal.h>
#include <fmt/format.h>
#include "LogApp.h"

#include <Windows.h>

namespace Logger
{
	unsigned LogWindow::idx = 0;

	LogWindow::LogWindow( const std::string_view& name )
		: id( idx++ )
		, hash( "###LogWindow" + std::to_string( id ) )
		, name( std::string( name ) )
		, name_buffer( this->name )
	{
		this->name.reserve( MaxCommandSize );
		ClearCommandText();
	}

	void LogWindow::OnRender( const float elapsed_time, const WrapVector<LogLine>& lines )
	{
		if( clear )
		{
			start_from = lines.size();
			clear = false;
		}

		ImGui::SetNextWindowSize( ImVec2( 1000, 400 ), ImGuiCond_Once );
		ImGui::SetNextWindowPos( ImVec2( float( 100 + 10 * ( id % 5 ) + 10 * id / 5 ), float( 100 + 10 * ( id % 5 ) ) ), ImGuiCond_Once );

		const auto program_flags = ImGuiWindowFlags_NoCollapse;
		if( ImGui::Begin( ( name + hash ).c_str(), &open, program_flags ) )
		{
			RenderLog( lines );
			RenderCommandline();
			RenderPopups();
		}
		ImGui::End();
	}

	void LogWindow::ClearCommandText()
	{
		for( unsigned i = 0; i < MaxCommandSize; ++i )
			command_text[i] = '\0';
	}

	void LogWindow::Handle( const std::string& msg )
	{
		if( msg.starts_with( "name " ) )
			name = msg.substr( 5 );
		else if( msg == "lock" )
			lock_to_bottom = true;
		else if( msg == "unlock" )
			lock_to_bottom = false;
		else if( msg == "bottom" )
			scroll_bottom = true;
		else if( msg == "top" )
			scroll_top = true;
		else if( msg == "clear" )
			clear = true;

		LogApp::Get()->Handle( msg );
	}

	void LogWindow::RenderLog( const WrapVector<LogLine>& lines )
	{
		auto size = ImGui::GetContentRegionAvail();
		size.y -= ImGui::GetTextLineHeightWithSpacing() + ImGui::GetStyle().FramePadding.y * 2;

		if( !ImGui::ListBoxHeader( "##LogFrame", size ) )
			return;

		const float scroll = ImGui::GetScrollMaxY() ? ImGui::GetScrollY() / ImGui::GetScrollMaxY() : 0;

		ImGuiContext& g = *ImGui::GetCurrentContext();
		bool value_changed = false;
		ImGuiListClipper clipper( lines.size() - start_from, ImGui::GetTextLineHeightWithSpacing() ); // We know exactly our line height here so we pass it as a minor optimization, but generally you don't need to.

		ImGui::BeginColumns( "LogFrameColumns", NumColumns, ImGuiColumnsFlags_NoResize );
		for( unsigned i = 0; i < NumColumns; ++i )
			ImGui::SetColumnWidth( i, static_cast<float>( LogLine::GetColumnWidth( static_cast<LogColumns>( i ) ) ) );

		while( clipper.Step() )
		{
			for( int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++ )
			{
				const auto idx = i + start_from;

				ImGui::PushID( i );

				for( unsigned col = 0; col < NumColumns; ++col )
				{
					const auto* text = lines[idx].Get( static_cast<LogColumns>( col ) );
					const ImVec4& colour = lines[idx].GetColour( static_cast<LogColumns>( col ) );

					ImGui::TextColored( colour, text );
					ImGui::NextColumn();
				}
				ImGui::PopID();
			}
		}

		if( ImGui::IsKeyPressed( 'G' ) && ImGui::IsKeyDown( VK_CONTROL ) )
		{
			OpenPopup( "GotoLine" );
		}

		if( lock_to_bottom || scroll_bottom )
		{
			ImGui::SetScrollHereY( 1.0f );
			scroll_bottom = false;
		}
		else if( scroll_top )
		{
			ImGui::SetScrollHereY( 0.0f );
			scroll_top = false;
		}
		else if( scroll_to )
		{
			const float max = ImGui::GetScrollMaxY() / lines.size();
			ImGui::SetScrollY( scroll_to_line * max );
			scroll_to = false;
			scroll_to_line = 0;
		}

		ImGui::EndColumns();
		ImGui::ListBoxFooter();
	}

	void LogWindow::RenderCommandline()
	{
		const auto input_text_flags = ImGuiInputTextFlags_EnterReturnsTrue;

		ImGui::TextUnformatted( ">>" );
		ImGui::SameLine();
		if( ImGui::InputText( "##CommandLine", command_text, MaxCommandSize, input_text_flags ) )
		{
			// Fix the broken string now (size is 0 even though there is text in it)
			auto msg = command_text;

			// Log to screen
			LogApp::Get()->Log( LogLevel::COMMAND, fmt::format( "> {}", msg ) );

			// Handle the command
			Handle( msg );

			// Reset the string
			ClearCommandText();

			// Put focus back
			ImGui::SetKeyboardFocusHere();
		}

		if( ImGui::IsWindowFocused() && !ImGui::IsItemFocused() && ImGui::IsKeyPressed( VK_RETURN ) )
			ImGui::SetKeyboardFocusHere();
	}

	void LogWindow::RenderPopups()
	{
		for( auto& popup : pending_popups )
			ImGui::OpenPopup( popup.c_str() );
		pending_popups.clear();

		bool open = true;

		if( ImGui::BeginPopupModal( "GotoLine", &open, ImGuiWindowFlags_AlwaysAutoResize ) )
		{
			if( ImGui::InputInt( "Line", &scroll_to_line, 1, 100, ImGuiInputTextFlags_EnterReturnsTrue ) )
			{
				scroll_to = true;
				ImGui::CloseCurrentPopup();
			}

			ImGui::SetKeyboardFocusHere();
			ImGui::EndPopup();
		}

		if( !open )
			ImGui::CloseCurrentPopup();
	}

	void LogWindow::OpenPopup( std::string&& name )
	{
		pending_popups.push_back( std::move( name ) );
	}

	void LogWindow::OnMessageAdded()
	{
	}

	void LogWindow::OnClearAll()
	{
		start_from = 0;
	}
}