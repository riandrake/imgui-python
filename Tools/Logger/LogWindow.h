#pragma once

#include <vector>

#include "LogLine.h"
#include "Common/Utility/WrapVector.h"

namespace Logger
{
	class LogWindow
	{
	public:
		// Static
		static unsigned idx;

		// Constant
		const unsigned id;
		const std::string hash;

		// Runtime
		std::string name;
		std::string name_buffer;
		bool open = true;

		LogWindow( const std::string_view& name );

		void OnRender( const float elapsed_time, const WrapVector<LogLine>& lines );

		void OnMessageAdded();
		void OnClearAll();

	protected:
		void RenderLog( const WrapVector<LogLine>& lines );
		void RenderCommandline();
		void RenderPopups();

		void Handle( const std::string& msg );
		void OpenPopup( std::string&& name );

		enum { MaxCommandSize = 256 };
		char command_text[MaxCommandSize];
		void ClearCommandText();

		std::vector<std::string> pending_popups;
		unsigned start_from = 0;
		bool lock_to_bottom = false;
		bool scroll_bottom = false;
		bool scroll_top = false;
		bool scroll_to = false;
		bool clear = false;
		int scroll_to_line = 0;
	};
}