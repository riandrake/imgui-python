#pragma once

#include <string>

#include <Common/ImGui/ImGuiEx.h>

#include "LogLevel.h"

enum LogColumns
{
	LineNumber,
	Time,
	Level,
	Subsystem,
	PID,
	Message,
	NumColumns
};

namespace Logger
{
	class LogLine
	{
	public:
		LogLine( const std::string& raw, const unsigned line_number );

		static float GetColumnWidth( const LogColumns column );

		const char* Get( const LogColumns column ) const;
		const ImVec4& GetColour( const LogColumns column ) const;


	private:
		static float ColumnWidths[LogColumns::NumColumns];

		unsigned line_number;
		std::string source;
		unsigned offsets[NumColumns];
		ImVec4 colours[NumColumns];
	};
}