#include "LogApp.h"

#include <algorithm>

#include <Common/Program/BaseApp.h>

#include "Modules/LogModule.h"

PYBIND11_EMBEDDED_MODULE( Log, mod )
{
	Python::Modules::Logger::Initialise( mod );
}

namespace Logger
{
	LogApp* app = nullptr;

	LogApp::LogApp()
		: lines( MaxLines )
	{
		app = this;
	}

	void LogApp::ProcessCommandLine( const std::string& command )
	{
		if( command == "-new" )
			AddWindow();
	}

	void LogApp::OnInitialise( const std::string_view& args )
	{
		Log::Info( "[STARTUP] OnInitialise" );
		udp_listener = std::make_unique<Network::UDPListener>( LogApp::LogUDP );

		AddWindow();
		colour_module = py::module::import( "colours" );
		assert( colour_module );
	}

	void LogApp::OnPostInitialise()
	{
		Log::Info( "[STARTUP] Post Initialisation Begin" );

		auto& io = ImGui::GetIO();
		io.IniFilename = nullptr;
		io.ConfigViewportsNoTaskBarIcon = false;
		io.ConfigViewportsNoAutoMerge = true;
		io.ConfigViewportsNoDecoration = true;

		Log::Info( "[STARTUP] Post Initialisation Complete" );
	}

	void LogApp::OnDestroy()
	{
		Log::Info( "[DESTROY] Begin" );
		Log::Info( "[DESTROY] End" );
	}

	void LogApp::OnRender()
	{
		if( udp_listener )
			udp_listener->Update();

		const float elapsed = ImGui::GetIO().DeltaTime;

		for( unsigned i = 0; i < log_windows.size(); ++i )
			log_windows[i]->OnRender( elapsed, lines );

		log_windows.erase( std::remove_if( log_windows.begin(), log_windows.end(), []( const LogWindow_t& window )
		{
			return !window->open;
		} ), log_windows.end() );

		open = !log_windows.empty();
	}

	void LogApp::AddWindow( const std::string_view& name )
	{
		log_windows.emplace_back( std::make_unique<LogWindow>( name ) );
	}

	void LogApp::Log( const LogLevel::Levels level, const std::string_view& message )
	{
		std::string header = fmt::format( "DATE TIME [{} Log 0] ", LogLevel::LevelToString( level ) );
		app->AddMessage( header + std::string( message ) );
	}

	void LogApp::LogUDP( const std::string_view& message )
	{
		app->AddMessage( message );
	}

	void LogApp::AddMessage( const std::string_view& message )
	{
		lines.emplace_back( std::string( message ), ++line_count );

		for( auto& window : log_windows )
			window->OnMessageAdded();
	}

	LogApp* LogApp::Get()
	{
		return app;
	}

	void LogApp::Clear()
	{
		line_count = 0;
		lines.clear();

		for( auto& window : log_windows )
			window->OnClearAll();
	}

	void LogApp::Handle( const std::string_view& msg )
	{
		if( msg == "clear_all" )
		{
			Clear();
		}
		else if( msg == "new" )
		{
			AddWindow( "NewWindow" );
		}
		else if( msg.starts_with( "new " ) )
		{
			AddWindow( msg.substr( 4 ) );
		}
		else if( msg.find( "log " ) != -1 )
		{
			Log( LogLevel::INFO, msg.substr( 4 ) );
		}
	}
}