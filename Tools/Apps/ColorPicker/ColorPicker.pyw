""" Implements the Color Picker program """
import ImGui
from ctypes import windll
import math
import json

class ColorPicker(ImGui.Base):
#
    """ ColorPicker Program Class """

    def __init__(self):
    #
        self.sampling = False
        self.color = ImGui.ImVec4(0,0,0,0)
        self.sampled = []
        self.msg = None
        super().__init__()
    #

    def program_name(self):
        return "ColorPicker"
    def window_name(self):
        return "Color Picker"

    def initialise(self):
    #
        """ API: Called once before entering the main loop """
        settings = None

        try:
            with open('saved.json', 'r') as f:
                settings = json.load(f)
        except FileNotFoundError:
            return

        if not settings:
            return

        arr = settings['color']
        self.color = ImGui.ImVec4(*arr)
        self.sampled = [ImGui.ImVec4(*arr) for arr in settings['sampled']]
    #

    def destroy(self):
    #
        """ API: Called once after exiting the main loop """
        settings = {
            'sampled': [[col.x, col.y, col.z, col.w] for col in self.sampled],
            'color': [self.color.x, self.color.y, self.color.z, self.color.w],
        }

        with open('saved.json', 'w') as f:
            json.dump(settings, f)
    #

    def msg_proc(self, hwnd, msg, wparam, lparam):
    #
        """ API: Message handling """
        return False
    #

    def frame_move(self, elapsed_time):
    #
        """ API: Called every frame """
        pass
    #

    def sample_pixel(self, x, y):
    #
        """ Sample a pixel color at a location """
        hdc = windll.user32.GetDC(0)
        return windll.gdi32.GetPixel(hdc, int(x), int(y))
    #

    def save_to_palette(self, color):
    #
        """ Save a colour to the palette """
        if color not in self.sampled:
            self.sampled.append(color)
            self.msg = (ImGui.ImVec4(0.529, 0.608, 0.373, 1), 'Success!')
        else:
            self.msg = (ImGui.ImVec4(0.353, 0.341, 0.612, 1), f'Duplicate of: #{self.sampled.index(self.color)+1}')
    #

    def render(self, elapsed_time):
    #
        """ API: Called during rendering """
        if self.sampling:
        #
            if ImGui.IsMousePosValid():
            #
                mouse_pos = ImGui.GetMousePos()
                col = self.sample_pixel(math.floor(mouse_pos.x), math.floor(mouse_pos.y))
                self.color = ImGui.ColorConvertU32ToFloat4(col)
            #
            else:
            #
                self.save_to_palette(self.color)
                self.sampling = False
            #
        #

        # Get the size and position of the primary monitor
        primary_monitor = ImGui.GetMonitor(0)
        pos = primary_monitor.MainPos
        size = primary_monitor.MainSize

        # Force the Python window size and position when the program first
        # opens
        ImGui.SetNextWindowPos(pos + size / 2, ImGui.ImGuiCond_Once, ImGui.ImVec2(0.5,0.5))
        ImGui.SetNextWindowSize(ImGui.ImVec2(300, 0), ImGui.ImGuiCond_Always)

        def on_changed(col):
            self.color = col

        # Render the Python window if not minimized
        # Quit the application if the "X" button is clicked
        if ImGui.Begin("Color Picker", self.exit, ImGui.ImGuiWindowFlags_MenuBar):
        #
            if ImGui.BeginMenuBar():
            #
                if ImGui.BeginMenu("File"):
                #
                    if ImGui.Selectable("New"):
                        pass
                    if ImGui.Selectable("Load"):
                        pass
                    if ImGui.Selectable("Save"):
                        pass
                    if ImGui.Selectable("Save As"):
                        pass
                    ImGui.EndMenu()
                #
                ImGui.EndMenuBar()
            #

            ImGui.PushItemWidth(-1)
            picker_flags = ImGui.ImGuiColorEditFlags_NoSidePreview
            ImGui.ColorPicker4('##ColorPicker', self.color, picker_flags, on_changed)

            def on_checked(b):
                self.sampling = b
                self.msg = None

            ImGui.Separator()
            ImGui.Text('Palette:')

            ImGui.SameLine(ImGui.GetContentRegionAvailWidth() - 190)
            if ImGui.Button('Add', ImGui.ImVec2(60,0)):
                self.save_to_palette(self.color)

            if not self.sampling:
                ImGui.SameLine(ImGui.GetContentRegionAvailWidth() - 120)
                if ImGui.Button('Sample', ImGui.ImVec2(60,0)):
                    self.sampling = True
                    self.msg = (ImGui.ImVec4(0.953, 0.529, 0.373, 1), 'Sampling...')

            ImGui.SameLine(ImGui.GetContentRegionAvailWidth() - 50)
            if ImGui.Button('Clear', ImGui.ImVec2(60,0)):
                ImGui.OpenPopup('Clear')

            if self.msg:
                ImGui.TextColored(*self.msg)
            else:
                ImGui.NewLine()

            for idx, col in enumerate(self.sampled):
            #
                if idx % 10:
                    ImGui.SameLine()
                if ImGui.ColorButton(str(idx), col):
                    self.color = col
            #

            if ImGui.BeginPopup('Clear'):
            #
                ImGui.Text('Are you sure you want to clear all sampled colors?')
                if ImGui.Button('Yes'):
                    self.sampled = []
                    ImGui.CloseCurrentPopup()
                    self.msg = (ImGui.ImVec4(0.353, 0.341, 0.612, 1), 'Palette cleared.')
                ImGui.SameLine()
                if ImGui.Button('No'):
                    ImGui.CloseCurrentPopup()
                ImGui.EndPopup()
            #
        #
        ImGui.End()
    #
#
if __name__ == '__main__':
    app = ColorPicker()
    ImGui.main(app)
