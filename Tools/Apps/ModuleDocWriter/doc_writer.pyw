""" Tool to write documentation for imgui-python modules """
import tkinter
from tkinter import filedialog
from dataclasses import dataclass

import ImGui
from popup import Popup
from function import Function
from string_ref import StringRef
from enum import Enum
import toml

class DocWriter(ImGui.Program):
    """ Documentation Writer Program """

    def __init__(self):
        """ Initialise the program state """
        super().__init__()
        self.root = tkinter.Tk()
        self.root.withdraw()
        self.filename = None
        self.functions = {}
        self.dirty = False
        self.popup = None

    def get_unsaved_color(self):
        return ImGui.ImVec4(0.772,0.270,0.024,1)

    def initialise(self):
        """ API: Called once before entering the main loop """
        #self.font = ImGui.LoadFont('../SourceCodePro-Regular.ttf', 16, 1, 1, True)
        ImGui.SetStyleColorsClassic()

    def msg_proc(self, hwnd, msg, wparam, lparam):
        """ API: Message Handling """
        return False

    def frame_move(self, elapsed_time):
        """ API: Called every frame """
        pass

    def on_render(self, elapsed_time):
        """ API: Called during rendering """
        if self.dirty:
            col = self.get_unsaved_color()
            ImGui.PushStyleColor(ImGui.ImGuiCol_TitleBg, col)
            ImGui.PushStyleColor(ImGui.ImGuiCol_TitleBgActive, col)
            ImGui.PushStyleColor(ImGui.ImGuiCol_TitleBgCollapsed, col)
            window_open = ImGui.Begin("Document Writer", self.quit_writer, ImGui.ImGuiWindowFlags_MenuBar)
            ImGui.PopStyleColor(3)
        else:
            window_open = ImGui.Begin("Document Writer", self.quit_writer, ImGui.ImGuiWindowFlags_MenuBar)

        if window_open:
            # Render the documentation writer
            self.render_writer()

            # Render the modal popup window if we have one
            if self.popup:
                self.popup.on_render()
        ImGui.End()

    def content_changed(self, func):
        """ Called when changes are made to the file, for safety """
        self.dirty = True
        func.content_changed()

    def quit_writer(self, ignore_changes = False):
        """ Begin exit process """
        if self.dirty and not ignore_changes:
            # Confirm that the user intends to quit without saving
            self.popup = Popup("Quit Program"
                    , "Quit program without saving?"
                    , lambda: self.quit_writer(True)
                    , self.close_popup)
        else:
            # Exit the program
            self.Exit()

    def render_menu_bar(self):
        """ Render the menu toolbar for the document writer """
        if ImGui.BeginMenu('File'):
            if ImGui.Selectable('Open'):
                # Allow the user to browse for a source file
                self.browse_file()

            if self.filename:
                if ImGui.Selectable('Save'):
                    # Save the current documentation
                    self.save_file()
                if ImGui.Selectable('Reload'):
                    # Reload the editor
                    self.reload_file()
                if ImGui.Selectable('Close'):
                    # Close the current source file
                    self.close_file()

            ImGui.EndMenu()

    def render_writer(self):
        """ Render the document writer tool """
        if ImGui.BeginMenuBar():
            self.render_menu_bar()
            ImGui.EndMenuBar()
        
        if ImGui.BeginChildFrame('DocWriterFrame'):
            # Render the documentation information
            self.render_doc_writer()
        ImGui.EndChildFrame()

    def save_file(self, function = None):
        """ Save changes to file """
        if not self.filename:
            return

        def save_overloads(text, overloads):
            for func in overloads:
                search_term = f'GENERATE {func.return_value.name} {func.name}'
                found = text.find(search_term)

                if found == -1:
                    print(f'WARNING: Could not save {func.name}. Reason: Definition not found.')
                    continue

                comment_begin = text.rfind('/*', 0, found)

                if comment_begin != -1:
                    comment_begin = text.rfind('\n', 0, comment_begin)
                    comment_end = text.rfind('*/', 0, found)
                    if comment_end == -1:
                        print(f'WARNING: Could not save {func.name}. Reason: Comment /* */ mis-match')
                        continue

                    between = text[comment_end+2:found]
                    if not between.strip():
                        # Wipe the existing comment
                        text = text[:comment_begin] + text[comment_end+2:]
                        found -= comment_end + 2 - comment_begin

                comment_dict = {}
                func.write_comment(comment_dict)
                if comment_dict:
                    text = text[:found] + '/*\n\t\t\t' + toml.dumps(comment_dict) + '\n\t\t*/\n\t\t' + text[found:]

                with open(self.filename, 'w') as f:
                    f.write(text)

        text = open(self.filename, 'r').read()

        if function:
            if function in self.functions.keys():
                save_overloads(text, self.functions[function])

            self.dirty = False
            for _, overloads in self.functions.items():
                if any(func.dirty for func in overloads):
                    self.dirty = True
                    break
        else:
            for _, overloads in sorted(self.functions.items()):
                save_overloads(text, overloads)
            self.dirty = False

    def close_popup(self):
        """ Discard the current popup """
        self.popup = None

    def close_file(self, ignore_changes = False):
        """ Close the file """
        if self.dirty and not ignore_changes:
            # Confirm the user intends to discard any changes
            self.popup = Popup("Close File"
                    , "Close file without saving?"
                    , lambda: self.close_file(True)
                    , self.close_popup)
        else:
            # Close the file
            self.filename = None
            self.functions = {}
            self.dirty = False

    def browse_file(self, ignore_changes = False):
        """ Load a new CPP module from file, browsed from the user """
        if self.dirty and not ignore_changes:
            # Confirm that the user intends to discard any changes
            self.popup = Popup("Discard changes"
                    , "Discard changes to current file?"
                    , lambda: self.close_file(True)
                    , self.close_popup)
        else:
            # Browse for a new source file
            filetypes = ("CPP files", "*.cpp"), ("All files", "*")
            self.filename = filedialog.askopenfilename(filetypes=filetypes)
            if self.filename:
                text = open(self.filename, 'r').read()
                self.load_file(text)
            ImGui.SetWindowFocus()

    def reload_file(self, ignore_changes = False):
        """ Reload a new CPP module from file """
        if self.dirty and not ignore_changes:
            # Confirm that the user intends to discard any changes
            self.popup = Popup("Discard changes"
                    , "Discard changes to current file?"
                    , lambda: self.reload_file(True)
                    , self.close_popup)
        else:
            # Browse for a new source file
            text = open(self.filename, 'r').read()
            self.load_file(text)
            ImGui.SetWindowFocus()
    
    def load_file(self, text):
        """ Load information from file """
        # Clear any information we may have dangling from a previous session
        self.functions = {}
        self.dirty = False

        # Search for 'GENERATE ' function tags in the file
        last = 0
        while True:
            found = text.find('GENERATE ', last)
            if found == -1:
                break

            # Find the definition of the function
            definition_begin = found + len('GENERATE ')
            definition_end = text.find('{', definition_begin)

            if definition_end == -1:
                raise ValueError('Could not find function definition')

            definition = text[definition_begin:definition_end]

            # Find an existing comment for the function
            comment_begin = text.rfind('/*', last, found)
            if comment_begin != -1:
                comment_end = text.find('*/', comment_begin, found)
                if comment_end == -1:
                    raise ValueError('Mismatch of /* and */ comments in file')
                comment = text[comment_begin+2:comment_end].strip()

                # Check this comment is actually related to the function before
                # we continue
                between = text[comment_end+2:found]
                if between.strip():
                    comment = None
            else:
                comment = None

            # Add the function to the list
            func = Function(definition, comment)

            if func.python_name not in self.functions:
                self.functions[func.python_name] = []

            self.functions[func.python_name].append(func)
            last = found + 1

    def render_cpp(self, cpp_text):
        pink = ImGui.ImVec4(198/255,149/255,198/255,1)
        blue = ImGui.ImVec4(153/255,180/255,209/255,1)

        pink_words = (
            'const',
            'void',
            'nullptr',
            'true',
            'false',
            'bool',
            'int',
            'float',
        )

        ImGui.Text('>')
        cursor = 0
        new_line_pos = 0
        for word in cpp_text:
            if not word:
                continue

            ImGui.SameLine()

            if word[-1] == '(':
                # Function
                if not new_line_pos:
                    new_line_pos = cursor + len(word) + 2

                ImGui.TextColored(blue, word[:-1])
                ImGui.SameLine(0, 0)
                ImGui.Text('(')
            elif word in pink_words:
                ImGui.TextColored(pink, word)
            else:
                ImGui.Text(word)

            cursor += len(word) + 1

    def input_text(self, func: Function, name: str, multi: bool, string_ref: StringRef, col=None):
        """ ImGui.InputText wrapper """
        def on_changed(text):
            string_ref.value = text

        requires_comment = not string_ref.value
        if requires_comment:
            ImGui.PushStyleColor(ImGui.ImGuiCol_FrameBg, ImGui.ImVec4(0.3,0.2,0.2,1))

        ImGui.PushItemWidth(-1)

        if multi:
            if ImGui.InputTextMultiline(name, string_ref.value, ImGui.ImVec2(0,60), 0, on_changed):
                self.content_changed(func)

        else:
            if ImGui.InputText(name, string_ref.value, 0, on_changed):
                self.content_changed(func)

        ImGui.PopItemWidth()

        if requires_comment:
            ImGui.PopStyleColor()

    def render_doc_writer(self):
        """ Render the documentation editor """

        red = ImGui.ImVec4(1,0.3,0.3,1)
        green = ImGui.ImVec4(0,1,0,1)
        unsaved_col = self.get_unsaved_color()

        for python_name, overloads in sorted(self.functions.items()):
        #{
            ImGui.PushID(python_name)
            complete = all(func.is_complete() for func in overloads)
            changes = any(func.dirty for func in overloads)

            if changes:
                ImGui.PushStyleColor(ImGui.ImGuiCol_Header, unsaved_col)
                tree_open = ImGui.CollapsingHeader(python_name)
                ImGui.PopStyleColor(1)
            else:
                tree_open = ImGui.CollapsingHeader(python_name)

            progress = sum(func.get_progress() for func in overloads)
            progress /= len(overloads)

            ImGui.SameLine(ImGui.GetContentRegionAvailWidth() - 200)
            if progress == 1.0:
                ImGui.PushStyleColor(ImGui.ImGuiCol_PlotHistogram, ImGui.ImVec4(0.2,0.7,0.2,1))
                ImGui.ProgressBar(progress)
                ImGui.PopStyleColor()
            else:
                ImGui.ProgressBar(progress)

            if tree_open:
            #{
                if changes:
                    if ImGui.Button('Save'):
                        self.save_file(python_name)
                else:
                    ImGui.NewLine()

                for idx, function in enumerate(overloads):
                #{
                    ImGui.PushID(idx)
                    self.render_cpp(function.definition)

                    ImGui.Indent()
                    self.input_text(function, '##Brief', True, function.comment)
                    ImGui.Unindent()

                    if function.return_value.name != 'void':
                    #{
                        ImGui.NewLine()
                        ImGui.Text('Return Value:')
                        ImGui.Indent()
                        self.input_text(function, f'##ReturnValue', True, function.return_value.comment)
                        ImGui.Unindent()
                    #}

                    if len(function.arguments):
                    #{
                        ImGui.NewLine()
                        ImGui.Text('Arguments:')
                        ImGui.Indent()

                        captions = [f'{idx+1}. {arg.name}:' for idx, arg in enumerate(function.arguments)]
                        width = max(ImGui.CalcTextSize(caption).x for caption in captions) + 50

                        for arg, caption in zip(function.arguments, captions):
                        #{
                            ImGui.Text(caption)
                            ImGui.SameLine(width)
                            self.input_text(function, f'##{arg.name}', False, arg.comment)
                        #}
                        ImGui.Unindent()
                    #}

                    ImGui.PopID()
                #}
            #}

            ImGui.PopID()
        #}

if __name__ == '__main__':
    """ Start the program """
    ImGui.main(DocWriter())

