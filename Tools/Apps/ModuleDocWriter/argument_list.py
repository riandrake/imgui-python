from argument import Argument

class ArgumentList(list):
    """ Contains information about an argument list """
    def __init__(self, argument_str, comment):
        # Remove any excess whitespace
        argument_str = argument_str.strip()

        # We need to scan the string manually, as there are comma characters
        # that do not necessitate the beginning of a new argument (i.e.
        # bracketed text)
        brackets = '<[({', '])}>'
        last = 0
        block = 0

        # Enumerate through the string and search for comma-delimited arguments
        for idx, char in enumerate(argument_str):
            if char in brackets[0]:
                # We are entering bracketed scope
                block += 1
            elif char in brackets[1]:
                # We are exiting bracketed scope
                block -= 1
            elif char == ',' and not block:
                # We have found a comma that precedes a new argument
                self.append(Argument(argument_str[last:idx], comment))
                last = idx + 1

        # Append the final argument (or perhaps the first)
        #                           (or perhaps there are no arguments at all)
        remaining = argument_str[last:]
        if remaining:
            self.append(Argument(remaining, comment))

    def write_comment(self, comment_lines):
        """ Write comments to file """
        if not self:
            return

        arg_comments = []
        for arg in self:
            arg.write_comment(arg_comments)

        if arg_comments:
            comment_lines.append('')
            comment_lines.append('[parameters]')
            comment_lines += arg_comments

    def get_progress(self):
        """ Returns the % progress towards comment completion """
        if not len(self):
            return 1.0

        progress = sum(1.0 if arg.comment.value else 0.0 for arg in self)
        return progress / len(self)
