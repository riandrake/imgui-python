""" Basic wrapper for strings to allow them to act as references """

class StringRef:
    def __init__(self, value=''):
        self.value = value
