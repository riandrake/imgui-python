import sys
sys.path.append('../')

from dataclasses import dataclass

import ImGui

@dataclass
class Popup:
    """ Wrapper for rendering a simple yes/no popup window """
    name: str    # Window title
    text: str    # Prompt question
    on_yes: None # 'Yes' callback
    on_no: None  #  'No' callback

    def __post_init__(self):
        self.first = True

    def on_render(self):
        """ Render the popup """
        if self.first:
            ImGui.OpenPopup(self.name)
            self.first = False

        if not self.name:
            raise ValueError('Tried to render a popup with no name')

        # Start the popup window
        if ImGui.BeginPopupModal(self.name):
            # Render the prompt question
            ImGui.Text(self.text)

            # Ask the user for input

            if ImGui.Button('Yes'):
                # The user has selected 'Yes'
                if self.on_yes:
                    self.on_yes() 
                ImGui.CloseCurrentPopup()
            ImGui.SameLine()
            if ImGui.Button('No'):
                # The user has selected 'No'
                if self.on_no:
                    self.on_no() 
                ImGui.CloseCurrentPopup()
            ImGui.EndPopup()
