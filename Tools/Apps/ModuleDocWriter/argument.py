from string_ref import StringRef

class Argument:
    """ Contains information about an argument """
    def __init__(self, argument_str, comment_dict):
        # Remove any excess whitespace
        argument_str = argument_str.strip()
        
        # Scan for default value
        self.default = None

        default_iter = argument_str.find('=')
        if default_iter != -1:
            # Store the default value
            self.default = argument_str[default_iter+1:].strip()
            # Strip the default value from the string
            argument_str = argument_str[:default_iter].strip()
        
        # Scan for the argument name
        name_iter = argument_str.rfind(' ')
        if name_iter == -1:
            raise ValueError('Could not differentiate argument name from type')

        # Strip the argument name, then infer the argument type
        self.name = argument_str[name_iter+1:].strip()
        self.type = argument_str[:name_iter].strip()

        array_iter = self.name.find('[')
        if array_iter != -1:
            self.type += self.name[array_iter:]
            self.name = self.name[:array_iter]

        self.comment = StringRef()
        if comment_dict and 'parameters' in comment_dict:
            parameters = comment_dict['parameters']
            if self.name in parameters:
                self.comment.value = parameters[self.name]

    def write_comment(self, comment_lines):
        """ Write comments to file """
        if self.comment.value:
            comment_lines.append(f'\t{self.name} = "{self.comment.value}"')
