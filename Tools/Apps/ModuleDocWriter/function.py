from dataclasses import dataclass
from argument_list import ArgumentList
from string_ref import StringRef
import toml

@dataclass
class CommentableText:
    name: str
    comment: StringRef

class Function:
    """ Contains information about a function """
    def __init__(self, definition, comment):
        # Locate the start of the argument list
        bracket_open = definition.find('(')
        if bracket_open == -1:
            raise ValueError('Could not find function arguments')

        # Infer the name of the function from the location of the open bracket
        name_iter = definition.rfind(' ', 0, bracket_open)
        if name_iter == -1:
            raise ValueError('Could not find function name')

        # Locate the end of the argument list
        bracket_close = definition.rfind(')')
        if bracket_close == -1:
            raise ValueError('Could not find function arguments end')

        # Store the function name and return value
        self.name = definition[name_iter+1:bracket_open].strip()

        # Remove overload from the name
        overload_split = self.name.find('_')
        if overload_split != -1:
            self.python_name = self.name[:self.name.find('_')]
            self.definition = definition.replace(self.name, self.python_name)
        else:
            self.python_name = self.name
            self.definition = definition

        self.definition = self.definition
        self.comment = StringRef()
        self.return_value = CommentableText(definition[:name_iter].strip(), StringRef())

        self.definition = self.definition.replace('\t', ' ')
        self.definition = self.definition.replace('\n', ' ')
        self.definition = [word.strip() for word in self.definition.split(' ')]

        if comment:
            # Load comment from file
            comment = toml.loads(comment)

            if 'brief' in comment:
                self.comment.value = comment['brief']

            if 'return' in comment:
                self.return_value.comment.value = comment['return']

        # Generate a parsed argument list from the raw string
        self.arguments = ArgumentList(definition[bracket_open+1:bracket_close], comment)

        self.dirty = False

    def write_comment(self, comment_lines):
        """ Write comment to file """
        if self.comment.value:
            comment_lines.append(f'brief = "{self.comment.value}"')

        if self.return_value.name != 'void':
            if self.return_value.comment.value:
                comment_lines.append(f'return = "{self.return_value.comment.value}"')

        self.arguments.write_comment(comment_lines)
        self.dirty = False

    def content_changed(self):
        self.dirty = True

    def get_progress(self):
        """ Get the % progress towards completed documentation """
        checklist = [
            1.0 if self.comment.value else 0.0,
        ]

        if self.return_value.name != 'void':
            checklist.append(1.0 if self.return_value.comment.value else 0.0)

        if self.arguments:
            checklist.append(self.arguments.get_progress())

        return sum(checklist) / len(checklist)
        
    def is_complete(self):
        """ Returns true if the comments for this method are filled out """
        return self.get_progress() >= 1.0

