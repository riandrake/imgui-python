""" Implements the test ImGui Python program """
import sys
sys.path.append('../')

import ImGui

class ExampleProgram(ImGui.Program):
    """ ImGui Program Class """

    def __init__(self):
        """ Initialise the program state """
        self.show_demo = False
        self.show_about = False
        self.show_metrics = False
        super().__init__()

    def check_demo(self, checked):
        """ Disable/Enable demo window """
        self.show_demo = checked
    def check_about(self, checked):
        """ Disable/Enable about window """
        self.show_about = checked
    def check_metrics(self, checked):
        """ Disable/Enable metrics window """
        self.show_metrics = checked

    def close_demo(self):
        """ Stop showing demo window """
        self.show_demo = False
    def close_about(self):
        """ Stop showing about window """
        self.show_about = False
    def close_metrics(self):
        """ Stop showing metrics window """
        self.show_metrics = False

    def Initialise(self):
        """ API: Called once before entering the main loop """
        pass

    def MsgProc(self, hwnd, msg, wparam, lparam):
        """ API: Message handling """
        # if msg == WM_KEYDOWN:
        #   if wparam == VK_ESCAPE:
        #       self.quit()
        return False

    def FrameMove(self, elapsed_time):
        """ API: Called every frame """
        pass

    def OnRender(self, elapsed_time):
        """ API: Called during rendering """

        # Get the size and position of the primary monitor
        primary_monitor = ImGui.GetMonitor(0)
        pos = primary_monitor.MainPos
        size = primary_monitor.MainSize

        # Force the Python window size and position when the program first opens 
        ImGui.SetNextWindowPos(pos + size / 2, ImGui.ImGuiCond_Once, ImGui.ImVec2(0.5,0.5))
        ImGui.SetNextWindowSize(ImGui.ImVec2(350,150), ImGui.ImGuiCond_Once)

        # Render the Python window if not minimized
        # Quit the application if the "X" button is clicked
        if ImGui.Begin("Python", self.Exit):
            self.render_menu()
        ImGui.End()

        # Additional windows, implemented in C++ by ImGui
        if self.show_demo:
            ImGui.ShowDemoWindow(self.close_demo)
        if self.show_about:
            ImGui.ShowAboutWindow(self.close_about)
        if self.show_metrics:
            ImGui.ShowMetricsWindow(self.close_metrics)

    def render_menu(self):
        """ Main menu panel """

        # Get the ImGui IO state
        io = ImGui.GetIO()

        # Calculate the frame time and fps values, rounded to 1dp
        ms = round(1000.0 / io.Framerate, 1)
        fps = round(io.Framerate, 1)

        # Add some text 
        ImGui.Text("ImGui Version: " + ImGui.GetVersion())
        ImGui.Text(f"FrameTime: {ms}ms")
        ImGui.Text(f"FrameRate: {fps}")

        # 3 Checkboxes on the same line to open additional windows
        ImGui.Checkbox("Show Demo", self.show_demo, self.check_demo)
        ImGui.SameLine()
        ImGui.Checkbox("Show About", self.show_about, self.check_about)
        ImGui.SameLine()
        ImGui.Checkbox("Show Metrics", self.show_metrics, self.check_metrics)

if __name__ == '__main__':
    """ Start the program """
    ImGui.main(ExampleProgram())
