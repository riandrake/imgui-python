""" Replica of the 'About' window found in the ImGui demo code, written in Python """
import ImGui
from ImGui import ImVec2
import sys

show_config_info = False

def ShowAboutWindow(on_close):
#
    if not ImGui.Begin("About Dear ImGui", on_close, ImGui.ImGuiWindowFlags_AlwaysAutoResize):
    #
        ImGui.End()
        return
    #

    ImGui.Text("Dear ImGui " + ImGui.GetVersion())
    ImGui.Separator()
    ImGui.Text("By Omar Cornut and all dear imgui contributors.")
    ImGui.Text("Dear ImGui is licensed under the MIT License, see LICENSE for more information.")

    def on_checked(b):
    #
        global show_config_info
        show_config_info = b
    #

    ImGui.Checkbox("Config/Build Information", show_config_info, on_checked)

    if show_config_info:
    #
        io = ImGui.GetIO()
        style = ImGui.GetStyle()

        copy_to_clipboard = ImGui.Button("Copy to clipboard")

        ImGui.BeginChildFrame(ImGui.GetID("cfginfos"), ImVec2(0, ImGui.GetTextLineHeightWithSpacing() * 18), ImGui.ImGuiWindowFlags_NoMove)

        if copy_to_clipboard:
            ImGui.LogToClipboard()

        ImGui.Text(f"Dear ImGui {ImGui.GetVersion()} ({ImGui.GetVersionNumber()})")
        ImGui.Separator()
        ImGui.Text("...")
        ImGui.Separator()
        ImGui.Text(f"io.BackendPlatformName: {io.BackendPlatformName}") 
        ImGui.Text(f"io.BackendRendererName: {io.BackendRendererName}") 
        ImGui.Text(f"io.ConfigFlags: {io.ConfigFlags}") 

        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NavEnableKeyboard:
            ImGui.Text(" NavEnableKeyboard")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NavEnableGamepad:
            ImGui.Text(" NavEnableGamepad")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NavEnableSetMousePos:
            ImGui.Text(" NavEnableSetMousePos")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NavNoCaptureKeyboard:
            ImGui.Text(" NavNoCaptureKeyboard")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NoMouse:
            ImGui.Text(" NoMouse")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_NoMouseCursorChange:
            ImGui.Text(" NoMouseCursorChange")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_DockingEnable:
            ImGui.Text(" DockingEnable")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_ViewportsEnable:
            ImGui.Text(" ViewportsEnable")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_DpiEnableScaleViewports:
            ImGui.Text(" DpiEnableScaleViewports")
        if io.ConfigFlags & ImGui.ImGuiConfigFlags_DpiEnableScaleFonts:
            ImGui.Text(" DpiEnableScaleFonts")
        if io.MouseDrawCursor:
            ImGui.Text("io.MouseDrawCursor")
        if io.ConfigViewportsNoAutoMerge:
            ImGui.Text("io.ConfigViewportsNoAutoMerge")
        if io.ConfigViewportsNoTaskBarIcon:
            ImGui.Text("io.ConfigViewportsNoTaskBarIcon")
        if io.ConfigViewportsNoDecoration:
            ImGui.Text("io.ConfigViewportsNoDecoration")
        if io.ConfigViewportsNoDefaultParent:
            ImGui.Text("io.ConfigViewportsNoDefaultParent")
        if io.ConfigDockingNoSplit:
            ImGui.Text("io.ConfigDockingNoSplit")
        if io.ConfigDockingWithShift:
            ImGui.Text("io.ConfigDockingWithShift")
        if io.ConfigDockingTabBarOnSingleWindows:
            ImGui.Text("io.ConfigDockingTabBarOnSingleWindows")
        if io.ConfigDockingTransparentPayload:
            ImGui.Text("io.ConfigDockingTransparentPayload")
        if io.ConfigMacOSXBehaviors:
            ImGui.Text("io.ConfigMacOSXBehaviors")
        if io.ConfigInputTextCursorBlink:
            ImGui.Text("io.ConfigInputTextCursorBlink")
        if io.ConfigWindowsResizeFromEdges:
            ImGui.Text("io.ConfigWindowsResizeFromEdges")
        if io.ConfigWindowsMoveFromTitleBarOnly:
            ImGui.Text("io.ConfigWindowsMoveFromTitleBarOnly")

        ImGui.Text(f"io.BackendFlags: {io.BackendFlags}")

        if io.BackendFlags & ImGui.ImGuiBackendFlags_HasGamepad:
            ImGui.Text(" HasGamepad")
        if io.BackendFlags & ImGui.ImGuiBackendFlags_HasMouseCursors:
            ImGui.Text(" HasMouseCursors")
        if io.BackendFlags & ImGui.ImGuiBackendFlags_HasSetMousePos:
            ImGui.Text(" HasSetMousePos")
        if io.BackendFlags & ImGui.ImGuiBackendFlags_PlatformHasViewports:
            ImGui.Text(" PlatformHasViewports")
        if io.BackendFlags & ImGui.ImGuiBackendFlags_HasMouseHoveredViewport:
            ImGui.Text(" HasMouseHoveredViewport")
        if io.BackendFlags & ImGui.ImGuiBackendFlags_RendererHasViewports:
            ImGui.Text(" RendererHasViewports")

        ImGui.Separator()

        ImGui.Text("io.Fonts: {} fonts, Flags: {}, TexSize: {},{}".format(
                io.Fonts.Fonts.Size, io.Fonts.Flags, io.Fonts.TexWidth, io.Fonts.TexHeight))

        ImGui.Text(f"io.DisplaySize: {io.DisplaySize.x},{io.DisplaySize.y}")
        ImGui.Text(f"io.DisplayFramebufferScale: {io.DisplayFramebufferScale.x},{io.DisplayFramebufferScale.y}")

        ImGui.Separator()

        ImGui.Text(f"style.WindowPadding: {style.WindowPadding.x},{style.WindowPadding.y}")
        ImGui.Text(f"style.WindowBorderSize: {style.WindowBorderSize}")
        ImGui.Text(f"style.FramePadding: {style.FramePadding.x},{style.FramePadding.y}")
        ImGui.Text(f"style.FrameRounding: {style.FrameRounding}")
        ImGui.Text(f"style.FrameBorderSize: {style.FrameBorderSize}")
        ImGui.Text(f"style.ItemSpacing: {style.ItemSpacing.x},{style.ItemSpacing.y}")
        ImGui.Text(f"style.ItemInnerSpacing: {style.ItemInnerSpacing.x},{style.ItemInnerSpacing.y}")

        if copy_to_clipboard:
            ImGui.LogFinish()

        ImGui.EndChildFrame()
    #
    ImGui.End()
#
