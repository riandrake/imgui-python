import ImGui
from ImGui import ImVec2, ImVec4

show_draw_cmd_clip_rects = True
show_window_begin_order = True

def NodeDrawList(window, viewport, draw_list, label):
#
    node_open = ImGui.TreeNode(draw_list, f"{label}: '{draw_list._OwnerName}' {draw_list.VtxBuffer.Size} vtx, {draw_list.IdxBuffer.Size} indices, {draw_list.CmdBuffer.Size} cmds")
    if draw_list == ImGui.GetWindowDrawList():
    #
        ImGui.SameLine()
        ImGui.TextColored(ImVec4(1.0,0.4,0.4,1.0), "CURRENTLY APPENDING")
        if node_open:
            ImGui.TreePop()
        return
    #

    fg_draw_list = ImGui.GetForegroundDrawList(viewport) if viewport else None
    if window and fg_draw_list and ImGui.IsItemHovered():
        fg_draw_list.AddRect(window.Pos, window.Pos + window.Size, ImGui.GetColorU32(255,255,0,255))
    if not node_open:
        return

    elem_offset = 0
    for pcmd in draw_list.CmdBuffer:
    #
        if not pcmd.UserCallback and pcmd.ElementCount == 0:
            continue
            
        if pcmd.UserCallback:
        #
            ImGui.BulletText(f"Callback {pcmd.UserCallback}, user_data {pcmd.UserCallbackData}")
            continue
        #

        idx_buffer = draw_list.IdxBuffer.Data if draw_list.IdxBuffer.Size else None

        pcmd_node_open = ImGui.TreeNode(pcmd - draw_list.CmdBuffer.begin(), "Draw {} {} vtx, tex {}, clip_rect ({},{})-({},{})", pcmd.ElemCount, "indexed" if draw_list.IdxBuffer.Size else "non-indexed", pcmd.TextureId, pcmd.ClipRect.x, pcmd.ClipRect.y, pcmd.ClipRect.z, pcmd.ClipRect.w)

        if show_draw_cmd_clip_rects and fg_draw_list and ImGui.IsItemHovered():
        #
            clip_rect = pcmd.ClipRect
            for i in range(elem_offset, elem_offest + pcmd.ElemCount):
                vtxs_rect.Add(draw_list.VtxBuffer[idx_buffer[i] if idx_buffer else i].pos)

            clip_rect.Floor()
            fg_draw_list.AddRect(clip_rect.Min, clip_rect.Max, ImGui.GetColorU32(255,255,0,255))
            vtxs_rect.Floor()
            fg_draw_list.AddRect(vtxs_rect.Min, vtxs_rect.Max, ImGui.GetColorU32(255,0,255,255))
        #

        if not pcmd_node_open:
            continue

        clipper = ImGui.ImGuiListClipper(pcmd.ElemCount/3)
        while clipper.Step():
        #
            for prim in range(clipper.DisplayStart, prim < clipper.DisplayEnd):
            #
                idx_i = elem_offset + clipper.DisplayStart * 3
                triangles_pos = [None, None, None]
                buff = ""

                for n in range(3):
                #
                    vtx_i = idx_buffer[idx_i] if idx_buffer else idx_i
                    v = draw_list.VtxBuffer[vtx_i]
                    triangles_pos[n] = v.pos
                    buff += f"{'idx' if not n else '   '} {idx_i}: pos ({v.pos.x},{v.pos.y}), uv ({v.uv.x},{v.uv.y}), col {v.col}\n"
                    idx_i += 1
                #

                ImGui.Selectable(buff, False)

                if fg_draw_list and ImGui.IsItemHovered():
                #
                    backup_flags = fg_draw_list.Flags
                    fg_draw_list.Flags &= ~ImGui.ImDrawListFlags_AntiAliasedLines
                    fg_draw_list.AddPolyline(triangles_pos, 3, ImGui.GetColorU32(255,255,0,255), True, 1.0)
                    fg_draw_list.Flags = backup_flags
                #
            #
        #
        ImGui.TreePop()
    #
    ImGui.TreePop()
#

def NodeWindow(window, label):
#
    if not ImGui.TreeNode(window, f"{label} '{window.Name}', {window.Active or window.WasActive} @ {window}"):
        return

    flags = window.Flags

    NodeDrawList(window, window.Viewport, window.DrawList, "DrawList")

    ImGui.BulletText(f"Pos: ({window.Pos.x},{window.Pos.y}), Size: ({window.Size.x},{window.Size.y}), SizeContents ({window.SizeContents.x},{window.SizeContents.y})")

    ImGui.BulletText("Flags: {} ({}{}{}{}{}{}{}{}{}..)".format(
        flags,
        "Child " if flags & ImGui.ImGuiWindowFlags_ChildWindow else "",
        "Tooltip " if flags & ImGui.ImGuiWindowFlags_Tooltip else "",
        "Popup " if flags & ImGui.ImGuiWindowFlags_Popup else "",
        "Modal " if flags & ImGui.ImGuiWindowFlags_Modal else "",
        "ChildMenu " if flags & ImGui.ImGuiWindowFlags_ChildMenu else "",
        "NoSavedSettings " if flags & ImGui.ImGuiWindowFlags_NoSavedSettings else "",
        "NoMouseInputs " if flags & ImGui.ImGuiWindowFlags_NoMouseInputs else "",
        "NoNavInputs " if flags & ImGui.ImGuiWindowFlags_NoNavInputs else "",
        "AlwaysAutoResize " if flags & ImGui.ImGuiWindowFlags_AlwaysAutoResize else "",
        ))

    ImGui.BulletText("Scroll: ({}/{},{}/{})".format(window.Scroll.x, ImGui.GetWindowScrollMaxX(window), window.Scroll.y, ImGui.GetWindowScrollMaxY(window)))
    ImGui.BulletText("Active: {}/{}, WriteAccessed: {}, BeginOrderWithinContext: {}".format(window.Active, window.WasActive, window.WriteAccessed, window.BeginOrderWithinContext if window.Active or window.WasActive else -1))
    ImGui.BulletText("Appearing: {}, Hidden: {} (CanSkip {} Cannot {}), SkipItems: {}".format(window.Appearing, window.Hidden, window.HiddenFramesCanSkipItems, window.HiddenFramesCannotSkipItems, window.SkipItems))
    ImGui.BulletText("NavLastIds: {},{}, NavLayerActiveMask: {}".format(window.NavLastIds[0], window.NavLastIds[1], window.DC.NavLayerActiveMask))
    ImGui.BulletText("NavLastChildNavWindows: {}".format(window.NavLastChildNavWindow.Name if window.NavLastChildNavWindow else None))

    if not window.NavRectRel[0].IsInverted():
    #
        ImGui.BulletText("NavRectRel[0]: ({},{})({},{})".format(
            window.NavRectRel[0].Min.x, window.NavRectRel[0].Min.y,
            window.NavRectRel[0].Max.x, window.NavRectRel[0].Max.y))
    #
    else:
    #
        ImGui.BulletText("NavRectRel[0]: <None>")
    #

    ImGui.BulletText("Viewport: {}{}, ViewportId: {}, ViewportPos: ({},{})".format(
        window.Viewport.Idx if window.Viewport else -1,
        " (Owned)" if window.ViewportOwned else "",
        window.ViewportId,
        window.ViewportPos.x, window.ViewportPos.y))

    ImGui.BulletText(f"ViewportMonitor: {window.Viewport.PlatformMonitor if window.Viewport else -1}")
    ImGui.BulletText("DockId: {}, DockOrder: {}, {}: {}, Act: {}, Vis: {}".format(
        window.DockId, window.DockOrder,
        'DockNodeAsHost' if window.DockNodeAsHost else 'DockNode',
        window.DockNodeAsHost if window.DockNodeAsHost else window.DockNode,
        window.DockIsActive, window.DockTabIsVisible))

    if window.RootWindow != window:
        NodeWindow(window.RootWindow, "RootWindow")

    if window.RootWindowDockStop != window.RootWindow:
        NodeWindow(window.RootWindowDockStop, "RootWindowDockStop")

    if window.ParentWindow:
        NodeWindow(window.ParentWindow, "ParentWindow")

    if window.DC.ChildWindows.Size:
        NodeWindows(window.DC.ChildWindows, "ChildWindows")

    if window.ColumnsStorage.Size and ImGui.TreeNode("Columns", f"Columns sets ({window.ColumnsStorage.Size})"):
    #
        for n in range(window.ColumnsStorage.Size):
        #
            columns = window.ColumnsStorage[n]
            if ImGui.TreeNode(columns.ID, "Columns Id: {}, Count: {}, Flags: {}".format(columns.ID, columns.Count, columns.Flags)):
            #
                ImGui.BulletText("Width: {} (MinX: {}, MaxX: {})".format(columns.MaxX - columnx.MinX, columns.MinX, columns.MaxX))

                for column_n in range(columns.Columns.Size):
                #
                    ImGui.BulletText("Column {}: OffsetNorm {} (= {} px)".format(
                        column_n, columns.Columns[column_n].OffsetNorm, ImGui.OffsetNormToPixels(columns, columns.Columns[column_n].OffsetNorm)))
                #
                ImGui.TreePop()
            #
        #
        ImGui.TreePop()
    #

    ImGui.BulletText("Storage: {} bytes".format(window.StateStorage.Data.Size * 20))
    ImGUI.TreePop()
#

def NodeWindows(windows, label):
#
    if not ImGui.TreeNode(label, f"{label} ({windows.Size})"):
        return

    for i in range(windows.Size):
        NodeWindow(windows[i], "Window")

    ImGui.TreePop()
#

def NodeViewport(viewport):
#
    ImGui.SetNextTreeNodeOpen(True, ImGui.ImGuiCond_Once)

    label = "Viewport {}, ID: {}, Parent: {}, Window: '{}'".format(
            viewport.Idx, viewport.ID, viewport.ParentViewportId, viewport.Window.Name if viewport.Window else None)

    if ImGui.TreeNode(viewport.ID, label):
    #
        flags = viewport.Flags

        ImGui.BulletText("Pos: ({},{}), Size: ({},{}), Monitor: {}, DpiScale: {}".format(
            viewport.Pos.x, viewport.Pos.y, viewport.Size.x, viewport.Size.y, viewport.PlatformMonitor, viewport.DpiScale * 100.0
            ))

        if viewport.Idx:
        #
            ImGui.SameLine()
            if ImGui.SmallButton("Reset Pos"):
            #
                viewport.Pos = ImVec2(200,200)
                if viewport.Window:
                    viewport.Window.Pos = ImVec2(200,200)
            #
        #

        ImGui.BulletText("Flags: {} = {}{}{}{}{}".format(viewport.Flags
          # , " CanHostOtherWindows" if flags & ImGui.ImGuiViewportFlags_CanHostOtherWindows else ""
            , " NoDecoration" if flags & ImGui.ImGuiViewportFlags_NoDecoration else ""
            , " NoFocusOnAppearing" if flags & ImGui.ImGuiViewportFlags_NoFocusOnAppearing else ""
            , " NoInputs" if flags & ImGui.ImGuiViewportFlags_NoInputs else ""
            , " NoRendererClear" if flags & ImGui.ImGuiViewportFlags_NoRendererClear else ""
            , " PlatformWindowMinimized" if viewport.PlatformWindowMinimized else ""
            ))

        for layer_i in viewport.DrawDataBuilder.Layers:
        #
            for draw_list_i in viewport.DrawDataBuilder.Layers[layer_i].Size:
                NodeDrawList(None, viewport, viewport.DrawDataBuilder.Layers[layer_i][draw_list_i], "DrawList")
        #

        ImGui.TreePop()
    #
#

def ShowMetricsWindow(on_close):
#
    if not ImGui.Begin("ImGui Metrics", on_close):
    #
        ImGui.End()
        return
    #

    io = ImGui.GetIO()

    ImGui.Text("Dear ImGui " + ImGui.GetVersion())
    ImGui.Text("Application average {1000.0 / io.Framerate} ms/frame ({io.Framerate} FPS)")
    ImGui.Text("{io.MetricsRenderVertices} vertices, {io.MetricsRenderIndices} indices ({io.MetricsRenderIndices / 3} visible)")
    ImGui.Text("{io.MetricsActiveWindows} active windows ({io.MetricsRenderWindows} visible)")
    ImGui.Text("{io.MetricsActiveAllocations}, allocations")

    def on_check(b):
    #
        global show_draw_cmd_clip_rects
        show_draw_cmd_clip_rects = b
    #

    ImGui.Checkbox("Show clipping rectangles when hovering draw commands", show_draw_cmd_clip_rects, on_check)

    def on_check(b):
    #
        global show_window_begin_order
        show_window_begin_order = b
    #

    ImGui.Checkbox("Ctrl shows window begin order", show_window_begin_order, on_check)

    ImGui.Separator()

    g = ImGui.GetCurrentContext()
    NodeWindows(g.Windows, "Windows")

    if ImGui.TreeNode("Viewport", f"Viewports ({g.Viewports.Size})"):
    #
        ImGui.Indent(ImGui.GetTreeNodeToLabelSpacing())
        ImGui.ShowViewportThumbnails()
        ImGui.Unindent(ImGui.GetTreeNodeToLabelSpacing())

        if g.PlatformIO.Monitors.Size > 0 and ImGui.TreeNode("Monitors", f"Monitors ({g.PlatformIO.Monitors.Size})"):
        #
            for i in range(g.PlatformIO.Monitors.Size):
            #
                mon = g.PlatformIO.Monitors[i]
                ImGui.BulletText("Monitor {}: DPI {}\n MainMin ({},{}), MainMax ({},{}), MainSize ({},{})\n WorkMin ({},{}), WorkMax ({}, {}), WorkSize ({}, {})"
                    , i, mon.DpiScale * 100.0
                    , mon.MainPos.x, mon.MainPos.y, mon.MainPos.x + mon.MainSize.x, mon.MainPos.y + mon.MainSize.y, mon.MainSize.x, mon.MainSize.y
                    , mon.WorkPos.x, mon.WorkPos.y, mon.WorkPos.x + mon.WorkSize.x, mon.WorkPos.y + mon.WorkSize.y, mon.WorkSize.x, mon.WorkSize.y)
            #
            ImGui.TreePop()
        #

        for i in range(g.Viewports.Size):
            NodeViewport(g.Viewports[i])

        ImGui.TreePop()
    #

    if ImGui.TreeNode("Popups", f"Popups ({g.OpenPopupStack.Size})"):
    #
        for i in range(g.OpenPopupStack.Size):
        #
            window = g.OpenPopupStack[i].Window
            ImGui.BulletText("PopupID: {}, Window '{}'{}{}".format(
                g.OpenPopupStack[i].PopupId, window.Name,
                " ChildWindow" if window and (window.Flags & ImGui.ImGuiWindowFlags_ChildWindow) else "",
                " ChildMenu" if window and (window.Flags & ImGui.ImGuiWindowFlags_ChildMenu) else "",
                ))
            ImGui.TreePop()
        #
    #

    if ImGui.TreeNode("Docking & Tab Bars"):
    #
        ImGui.ShowDockingDebug()
        ImGui.TreePop()
    #

    if ImGui.TreeNode("Internal state"):
    #
        input_source_names = ["None", "Mouse", "Nav", "NavKeyboard", "NavGamepad"]
        assert(len(input_source_names) == ImGui.ImGuiInputSource_COUNT)

        ImGui.Text(f"HoveredWindow: '{g.HoveredWindow.Name if g.HoveredWindow else None}'")
        ImGui.Text(f"HoveredWindowUnderMovingWindow: '{g.HoveredWindowUnderMovingWindow.Name if g.HoveredWindowUnderMovingWindow else None}'")
        ImGui.Text(f"HoveredId: {g.HoveredId}/{g.HoveredIdPreviousFrame} ({g.HoveredIdTimer} sec), AllowOverlap: {g.HoveredIdAllowOverlap}")
        ImGui.Text(f"ActiveId: {g.ActiveId}/{g.ActiveIdPreviousFrame} ({g.ActiveIdTimer} sec), AllowOverlap: {g.ActiveIdAllowOverlap}, Source: {input_source_names[g.ActiveIdSource]}")
        ImGui.Text(f"ActiveIdWindow: '{g.ActiveIdWindow.Name if g.ActiveIdWindow else None}'")
        ImGui.Text(f"MovingWindow: '{g.MovingWindow.Name if g.MovingWindow else None}'")
        ImGui.Text(f"NavWindow: '{g.NavWindow.Name if g.NavWindow else None}'")
        ImGui.Text(f"NavId: {g.NavId}, NavLayer: {g.NavLayer}")
        ImGui.Text(f"NavInputSource: {input_source_names[g.NavInputSource]}")
        ImGui.Text(f"NavActive: {g.IO.NavActive}, NavVisible: {g.IO.NavVisible}")
        ImGui.Text(f"NavActivateId: {g.NavActivateId}, NavInputId: {g.NavInputId}")
        ImGui.Text(f"NavDisableHighlight: {g.NavDisableHighlight}, NavDisableMouseHover: {g.NavDisableMouseHover}")
        ImGui.Text(f"NavWindowingTarget: '{g.NavWindowingTarget.Name if g.NavWindowTarget else None}'")
        ImGui.Text(f"DragDrop: {g.DragDropActive}, SourceId = {g.DragDropPayload.SourceId}, Payload '{g.DragDropPayload.DataType}' ({g.DragDropPayload.DataSize} bytes)")
        ImGui.Text(f"MouseViewport: {g.MouseViewport.ID} (UserHovered {g.IO.MouseHoveredViewport}, LastHovered {g.MouseLastHoveredViewport.ID if g.MouseLastHoveredViewport else None})")

        ImGui.TreePop()
    #

    if g.IO.KeyCtrl and show_window_begin_order:
    #
        for n in range(g.Windows.Size):
        #
            window = g.Windows[n]
            if not window.WasActive or ((window.Flags & ImGui.ImGuiWindowFlags_ChildWindow) and not window.DockNode):
                continue
            
            buff = f"Order: {window.BeginOrderWithinContext}\n"
            overlay_draw_list = ImGui.GetForegroundDrawList(window.Viewport)
            overlay_draw_list.AddRectFilled(window.Pos - ImVec2(1, 1), window.Pos + ImGui.CalcTextSize(buff) + ImVec2(1,1), ImGui.GetColorU32(200,100,100,255))
            overlay_draw_list.AddText(None, 0.0, window.Pos, ImGui.GetColorU32(255,255,255,255), buff)
        #
    #

    ImGui.End()
#
