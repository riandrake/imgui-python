""" Slow zone calculator """
import ImGui
import json
import os
import dataclasses

import ctypes
 
EnumWindows = ctypes.windll.user32.EnumWindows
EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
GetWindowText = ctypes.windll.user32.GetWindowTextW
GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
IsWindowVisible = ctypes.windll.user32.IsWindowVisible
 
def get_windows():
    titles = []
    def foreach_window(hwnd, lParam):
        if IsWindowVisible(hwnd):
            length = GetWindowTextLength(hwnd)
            buff = ctypes.create_unicode_buffer(length + 1)
            GetWindowText(hwnd, buff, length + 1)
            titles.append(buff.value)
        return True
    EnumWindows(EnumWindowsProc(foreach_window), 0)
    return [x for x in titles if x]

@dataclasses.dataclass
class WindowData:
    handle: str
    level: int
    pid: int
    status: str
    image: str

class WindowHandler(ImGui.Program):
    def __init__(self):
        self.output = None
        self.cmd = ''
        self.tick = 0.0
        self.windows = []
        self.rules = []
        super().__init__()

    def initialise(self):
        if os.path.exists('rules.txt'):
            with open('rules.txt', 'r') as f:
                self.rules = json.load(f)

    def destroy(self):
        with open('rules.txt', 'w') as f:
            json.dump(self.rules, f)

    def read(self, cmd):
        return os.popen(f'cmdow {cmd}').read()

    def call(self, cmd):
        os.system(f'cmdow {cmd}')
        self.refresh()

    def print(self, cmd):
        self.output = self.read(cmd)
        self.refresh()

    def cmd_changed(self, text):
        self.cmd = text
    
    def exec_rule(self, idx):
        self.call(f'"{self.rules[idx][0]}" {self.rules[idx][1]}')

    def refresh(self):
        old = self.windows
        self.windows = get_windows()

        for new in (x for x in self.windows if x not in old):
            if not new:
                continue
            for idx, rule in enumerate(self.rules):
                if not rule:
                    continue
                if new == rule[0]:
                    self.exec_rule(idx)

    def on_render(self, elapsed_time):
        self.tick -= elapsed_time
        if self.tick < 0.0:
            self.tick += 1.0
            self.refresh()

        if ImGui.Begin('Window Handler', self.exit, 0):
            size = ImGui.ImVec2(125,0)
            if ImGui.Button('Cascade', size):
                self.call('/CW')
            ImGui.SameLine()
            if ImGui.Button('Tile Horizontally', size):
                self.call('/TH')
            ImGui.SameLine()
            if ImGui.Button('Tile Vertically', size):
                self.call('/TV')
            ImGui.SameLine()
            if ImGui.Button('Full Screen', size):
                self.call('/TV')
            if ImGui.Button('Log', size):
                self.print('/T')
            ImGui.SameLine()
            if ImGui.Button('Help', size):
                self.print('/?')

            ImGui.Separator()

            def rule_callback(idx):
                def callback(text):
                    self.rules[idx][0] = text
                return callback

            def cmd_callback(idx):
                def callback(text):
                    self.rules[idx][1] = text
                return callback

            if ImGui.Button('Add Rule', size):
                self.rules.append(['', ''])

            ImGui.PushItemWidth(ImGui.GetContentRegionAvailWidth()/3)
            deleted = []
            for idx, rule in enumerate(self.rules):
                ImGui.PushID(idx)
                if ImGui.Button('X'):
                    deleted.append(idx)
                ImGui.SameLine()
                ImGui.InputText('##RuleText', rule[0], 0, rule_callback(idx))
                ImGui.SameLine()
                ImGui.InputText('##CmdText', rule[1], 0, cmd_callback(idx))
                ImGui.SameLine()
                if ImGui.Button('Execute'):
                    self.exec_rule(idx)
                ImGui.PopID()
            ImGui.PopItemWidth()

            for x in reversed(deleted):
                del self.rules[x]

            ImGui.Separator()

            ImGui.Text("Windows")
            if ImGui.BeginChildFrame("Windows", ImGui.ImVec2(0, ImGui.GetContentRegionAvail().y / 3), ImGui.ImGuiWindowFlags_HorizontalScrollbar):
                for window in self.windows:
                    if ImGui.Selectable(window):
                        ImGui.SetClipboardText(window)
                        self.output = f"{window} saved to clipboard."
            ImGui.EndChildFrame()

            ImGui.Text("Log")
            
            if ImGui.BeginChildFrame("Log", ImGui.ImVec2(0, ImGui.GetContentRegionAvail().y - 30), ImGui.ImGuiWindowFlags_HorizontalScrollbar):
                if self.output:
                    ImGui.Text(self.output)
            ImGui.EndChildFrame()

            ImGui.InputText('##Custom', self.cmd, 0, self.cmd_changed)
            ImGui.SameLine()
            if ImGui.Button('Execute'):
                print(self.cmd)
                self.call(self.cmd)
                self.cmd = ''

        ImGui.End()

if __name__ == '__main__':
    ImGui.main(WindowHandler())
