import pydoc
import ImGui
import webbrowser
import os

print('Writing documentation to "ImGui.txt..."')

with open('ImGui.txt', 'w') as f:
    f.write(pydoc.plain(pydoc.render_doc(ImGui)))

print('Documentation generated. Opening file...')
webbrowser.open('ImGui.txt')
