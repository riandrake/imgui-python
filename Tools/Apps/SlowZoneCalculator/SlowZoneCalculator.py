""" Slow zone calculator """
import ImGui
import json
import copy

def slider(n, v, v0, v1, c, f, p):
    def changed(v):
        if v < v0:
            c(v0)
        elif v > v1:
            c(v1)
        else:
            c(v)

    ImGui.InputFloat(n, v, 0.05, 0.1, f, 0, changed)

class SlowZoneCalculator(ImGui.Program):
    def __init__(self):
        super().__init__()
        self.cast_time = 3.0
        self.animation_length = 3.0
        self.slow_length = 0.7
        self.slow_cap = 0.125
        self.animation_length_override = 0.0
        self.slow_animations_go_to_idle = False

    def initialise(self):
        try:
            with open('SlowZoneCalculator.json', 'r') as f:
                settings = json.load(f)

            self.cast_time = settings['cast_time']
            self.animation_length = settings['animation_length']
            self.slow_length = settings['slow_length']
            self.slow_cap = settings['slow_cap']
            self.animation_length_override = settings['animation_length_override']
            self.cast_time = settings['cast_time']
            self.slow_animations_go_to_idle = settings['slow_animations_go_to_idle']
        except BaseException as e:
            print(str(e))

    def destroy(self):
        try:
            with open('SlowZoneCalculator.json', 'w') as f:
                settings = dict(
                    cast_time=self.cast_time,
                    animation_length=self.animation_length,
                    slow_length=self.slow_length,
                    slow_cap=self.slow_cap,
                    animation_length_override=self.animation_length_override,
                    slow_animations_go_to_idle=self.slow_animations_go_to_idle,
                )
                json.dump(settings, f)
        except BaseException as e:
            print(str(e))

    def cast_time_changed(self, v):
        self.cast_time = v

    def animation_length_changed(self, v):
        self.animation_length = v

    def slow_length_changed(self, v):
        self.slow_length = v

    def slow_cap_changed(self, v):
        self.slow_cap = v

    def animation_length_override_changed(self, v):
        self.animation_length_override = v

    def slow_animations_go_to_idle_changed(self, v):
        self.slow_animations_go_to_idle = v

    def on_render(self, elapsed_time):
        flags = ( ImGui.ImGuiWindowFlags_AlwaysAutoResize
                | ImGui.ImGuiWindowFlags_NoCollapse
        )

        if ImGui.Begin('Slow Zone Calculator', self.exit, flags):
            slider('Attack/Cast Time', self.cast_time, 0.0, 20.0, self.cast_time_changed, "%.3f seconds", 1.0)
            slider('Animation Length', self.animation_length, 0.0, 20.0, self.animation_length_changed, "%.3f seconds", 1.0)
            slider('Slow Length', self.slow_length, 0.0, 20.0, self.slow_length_changed, "%.3f seconds", 1.0)
            slider('Animation Length Override', self.animation_length_override, 0.0, 20.0, self.animation_length_override_changed, "%.3f seconds", 1.0)
            slider('Slow Cap', self.slow_cap, 0.0, 1.0, self.slow_cap_changed, "%.3fx", 1.0)
            ImGui.Checkbox('Slow Animations Go To Idle', self.slow_animations_go_to_idle, self.slow_animations_go_to_idle_changed)

            ImGui.BeginChildFrame('Results', ImGui.ImVec2(0, 600))
            try:
                SlowZoneCalculator.calculate(
                        self.cast_time,
                        self.animation_length,
                        self.slow_length,
                        self.slow_cap,
                        self.slow_animations_go_to_idle,
                        self.animation_length_override
                )
            except ValueError as e:
                ImGui.TextColored(ImGui.ImVec4(1,0,0,1), str(e))
            ImGui.EndChildFrame()

        ImGui.End()

    def calculate(CAST_TIME, ANIMATION_LENGTH, SLOW_LENGTH, SLOW_CAP, SLOW_IDLE, OVERRIDDEN_ANIMATION_LENGTH = 0.0):
        if not CAST_TIME:
            raise ValueError('Cannot have zero cast time')
        if not ANIMATION_LENGTH:
            raise ValueError('Cannot have zero animation length')
        if not SLOW_LENGTH:
            raise ValueError('Cannot have zero slow length')

        # Get the actual animation length we're going to work with, possibly overridden by the user
        DESIRED_LENGTH = ANIMATION_LENGTH
        if OVERRIDDEN_ANIMATION_LENGTH != 0.0:
            DESIRED_LENGTH = OVERRIDDEN_ANIMATION_LENGTH

        class Calculation:
            def print(self):
                if ImGui.CollapsingHeader(self.name):
                    if self.do_nothing:
                        ImGui.Text('SLOW ZONE DISABLED')
                        return

                    def text(text, good):
                        ImGui.TextColored(ImGui.ImVec4(0,1,0,1) if good else ImGui.ImVec4(1,0,0,1), text)

                    ImGui.Columns(2)
                    ImGui.Text(f'Modified Animation Length')
                    ImGui.NextColumn()
                    text(f'{self.modified_animation_length:.3f} seconds', self.modified_animation_length - self.animation_length < 0.001)
                    ImGui.NextColumn()

                    ImGui.Text(f'Modified Animation Time')
                    ImGui.NextColumn()
                    text(f'{self.modified_animation_time:.3f} seconds', self.modified_animation_time - CAST_TIME < 0.001)
                    ImGui.NextColumn()

                    ImGui.Text(f'Additional Length')
                    ImGui.NextColumn()
                    text(f'{self.additional_length:.3f} seconds', self.additional_length > 0)
                    ImGui.NextColumn()

                    normal_time = self.scaled_length - self.new_slow_length
                    slow_time = self.new_slow_length
                    good = (normal_time + slow_time) - CAST_TIME < 0.001

                    ImGui.Text('Normal Scale')
                    ImGui.NextColumn()
                    text(f'{self.normal_scale:.3f}x', good)
                    ImGui.NextColumn()
                    ImGui.Text('Normal Zone Time')
                    ImGui.NextColumn()
                    text(f'{normal_time:.3f} seconds', good)
                    ImGui.NextColumn()

                    ImGui.Text('Slow Scale')
                    ImGui.NextColumn()
                    text(f'{self.slow_scale:.3f}x', good)
                    ImGui.NextColumn()
                    ImGui.Text('Slow Zone Time')
                    ImGui.NextColumn()
                    text(f'{slow_time:.3f} seconds', good)
                    ImGui.NextColumn()
                    ImGui.Columns(1)

            def clone(self):
                return copy.copy(self)

            def calculate(self, name, animation_length):
                scale = animation_length / ANIMATION_LENGTH
                self.name = name
                self.animation_length = animation_length
                self.scaled_slow_length = SLOW_LENGTH / scale
                self.do_nothing = False

                # Calculate the speed multiplier required to make the normal animation time finish in cast_time
                self.animation_speed = self.animation_length / CAST_TIME
                
                # Calculate the animation time including cast speed
                self.scaled_length = self.animation_length / self.animation_speed

                # Calculate the extra time added by animation speed multipliers
                self.additional_length = self.scaled_length - self.animation_length

                if self.additional_length < 0:
                    # Nothing to do
                    self.normal_scale = 1.0
                    self.slow_scale = 1.0
                    self.new_slow_length = 0.0
                    self.do_nothing = True
                else:
                    # Calculate the new slow zone length after animation speed multipliers
                    self.new_slow_length = self.scaled_slow_length + self.additional_length

                    # Calculate the animation speed in the normal zone
                    self.normal_scale = 1.0

                    # Calculate the animation speed in the slow zone
                    self.slow_scale = self.scaled_slow_length / self.new_slow_length

                self.recalculate()
                return self

                
            def recalculate(self):
                # Calculate the new animation length, post modification with animation speed multipliers
                prev_normal_length = self.scaled_length - self.new_slow_length
                self.new_slow_length = SLOW_LENGTH / self.slow_scale
                normal_time = self.scaled_length - self.new_slow_length
                self.normal_scale = normal_time / prev_normal_length

                self.modified_animation_time = self.scaled_slow_length + (self.animation_length - SLOW_LENGTH) / self.normal_scale
                self.modified_animation_length = self.modified_animation_time * self.animation_speed

        normal = Calculation().calculate('Default', ANIMATION_LENGTH)
        desired = Calculation().calculate('Overridden', DESIRED_LENGTH)

        normal.print()

        normal_time = normal.scaled_length - normal.new_slow_length
        slow_time = normal.new_slow_length

        desired.print()

        merged = normal.clone()
        merged.slow_scale = desired.slow_scale
        merged.name = 'Merged'
        merged.recalculate()
        merged.print()

        correction_scale = merged.modified_animation_length / normal.modified_animation_length
        merged.normal_scale *= correction_scale
        merged.slow_scale *= correction_scale
        merged.name = 'Corrected'
        merged.recalculate()
        merged.print()

        if merged.slow_scale < SLOW_CAP and not SLOW_IDLE:
            merged.slow_scale = SLOW_CAP
            merged.recalculate()

        merged.name = 'Capped'
        merged.print()

if __name__ == '__main__':
    ImGui.main(SlowZoneCalculator())
