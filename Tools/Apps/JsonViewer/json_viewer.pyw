""" Implements the test ImGui Python program """
import json
import ImGui
import tkinter
from tkinter import filedialog

class JsonViewer(ImGui.Program):
    """ Json Viewer Program """

    def __init__(self):
        """ Initialise the program state """
        super().__init__()
        self.root = tkinter.Tk()
        self.root.withdraw()
        self.json_filename = None
        self.json_doc = None

    def initialise(self):
        """ API: Called once before entering the main loop """
        pass

    def msg_proc(self, hwnd, msg, wparam, lparam):
        """ API: Message Handling """

        # if msg == WM_KEYDOWN:
        #   if wparam == VK_ESCAPE:
        #       self.quit()
        return False

    def frame_move(self, elapsed_time):
        """ API: Called every frame """
        pass

    def on_render(self, elapsed_time):
        """ API: Called during rendering """

        # Get the size and position of the primary monitor
        primary_monitor = ImGui.GetMonitor(0)
        pos = primary_monitor.MainPos
        size = primary_monitor.MainSize

        # Force the Python window size and position when the program first opens 
        ImGui.SetNextWindowPos(pos + size / 2, ImGui.ImGuiCond_Once, ImGui.ImVec2(0.5,0.5))

        # Render the Python window if not minimized
        # Quit the application if the "X" button is clicked
        if ImGui.Begin("Json Viewer", self.exit):
            self.render_menu()
        ImGui.End()

    def browse_file(self):
        """ Load a new JSON file from file, browsed from the user """
        try:
            self.json_filename = filedialog.askopenfilename(filetypes = (("JSON files", "*.json"), ("All files", "*")))
            if self.json_filename:
                self.json_doc = json.load(open(self.json_filename, 'rb'))
        except json.decoder.JSONDecodeError as e:
            self.json_doc = str(e)

    def render_something(self, key, something):
        if isinstance(something, dict):
            if ImGui.TreeNode(key if key else '{dict}'):
                ImGui.Indent(0)
                self.render_object(key, something)
                ImGui.Unindent(0)
                ImGui.TreePop()
        elif isinstance(something, list):
            if ImGui.TreeNode(f"{key if key else '[list]'}: {len(something)}"):
                ImGui.Indent(0)
                self.render_array(key, something)
                ImGui.Unindent(0)
                ImGui.TreePop()
        else:
            if key:
                ImGui.Indent(0)
                ImGui.Text(f"{key}:")
                ImGui.Unindent(0)
                ImGui.SameLine()

            self.render_value(something)

    def render_object(self, name, o):
        for key, value in o.items():
            ImGui.PushID(key)
            self.render_something(key, value)
            ImGui.PopID()

    def render_array(self, name, a):
        for i in range(len(a)):
            ImGui.PushID(i)
            self.render_something(None, a[i])
            ImGui.PopID()

    def render_value(self, v):
        if isinstance(v, str):
            ImGui.TextColored(ImGui.ImVec4(1.00,0.56,0.64,1.00), f"\"{v}\"")
        else:
            ImGui.TextColored(ImGui.ImVec4(1.00,0.64,0.00,1.00), str(v))

    def render_json(self):
        """ Render JSON document """
        self.render_something(self.json_filename, self.json_doc)

    def render_menu(self):
        """ Main menu panel """

        # Get the ImGui IO state
        io = ImGui.GetIO()

        # Calculate the frame time and fps values, rounded to 1dp
        ms = round(1000.0 / io.Framerate, 1)
        fps = round(io.Framerate, 1)

        # Add some text 
        ImGui.Text(f"FrameTime: {ms}ms")
        ImGui.SameLine(150)
        ImGui.Text(f"FrameRate: {fps}")

        if ImGui.Button('Open File'):
            self.browse_file()

        if self.json_filename:
            ImGui.SameLine()
            if ImGui.Button('Close File'):
                self.json_filename = None
                self.json_doc = None

        if self.json_filename:
            if ImGui.BeginChildFrame("JsonViewer", ImGui.ImVec2(0, 0), 0):
                self.render_json()
            ImGui.EndChildFrame()

if __name__ == '__main__':
    """ Start the program """
    ImGui.main(JsonViewer())
