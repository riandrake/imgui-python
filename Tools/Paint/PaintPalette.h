#pragma once

#include <functional>

#include <Common/ImGui/ImGuiEx.h>
#include "PaintShapes.h"

namespace Paint
{
	enum class Mode
	{
		Text,
		Brush,
		Line,
		Circle,
		Rectangle,
		NumModes
	};

	const char* ModeToString( const Mode mode );
	void ForEachMode( std::function<void( Mode )> callback );

	Shapes ModeToShape( const Mode mode );

	class PaintPalette
	{
		void RenderRadialMenu();

	public:
		void Render();

		ImU32 colour = 0xFFFFFFFF;
		ImU32 background_colour = 0xFF222222;
		ImU32 canvas_colour = 0xFF000000;
		float weight = 10.0f;
		Mode edit_mode = Mode::Brush;
		ImVec2 radial_open_pos;
		bool filled = false;
		bool radial_open = false;
	};
}