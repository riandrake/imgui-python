#include "PaintAction.h"
#include <assert.h>

namespace Paint
{
	void Action::Do()
	{
		assert( !done );
		if( !done )
			do_action();
		done = true;
	}

	void Action::Undo()
	{
		assert( done );
		if( done )
			undo_action();
		done = false;
	}
}