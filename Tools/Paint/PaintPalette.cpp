#include "PaintPalette.h"

#include <Windows.h>

#include <Common/Utility/Exception.h>
#include <Common/ImGui/ImGuiEx.h>

#include "Common/ImGui/DragOperation.h"
#include "PaintProgram.h"

namespace Paint
{
	const char* ModeToString( const Mode mode )
	{
		switch( mode )
		{
		case Mode::Text: return "Text";
		case Mode::Brush: return "Brush";
		case Mode::Line: return "Line";
		case Mode::Circle: return "Circle";
		case Mode::Rectangle: return "Rectangle";
		}

		throw RuntimeError( "Invalid mode" );
	}

	Paint::Shapes ModeToShape( const Mode mode )
	{
		switch( mode )
		{
		case Mode::Text: return Shapes::Text;
		case Mode::Brush: return Shapes::Brush;
		case Mode::Line: return Shapes::Line;
		case Mode::Circle: return Shapes::Circle;
		case Mode::Rectangle: return Shapes::Rectangle;
		}
		return Shapes::NumShapes;
	}

	void ForEachMode( std::function<void( Mode )> callback )
	{
		for( int i = 0; i < (int)Mode::NumModes; ++i )
			callback( static_cast<Mode>( i ) );
	}

	void PaintPalette::RenderRadialMenu()
	{
		const auto mouse_pos = ImGui::GetMousePos();

		if( !radial_open && ImGui::IsMouseDown( (int)Buttons::RMB ) )
		{
			ImGui::OpenPopup( "RadialPalette" );
			radial_open_pos = mouse_pos;
			radial_open = true;
		}

		const bool closing = !ImGui::IsMouseDown( (int)Buttons::RMB );

		const auto flags = ImGuiWindowFlags_NoDecoration
			| ImGuiWindowFlags_NoBackground
			| ImGuiWindowFlags_NoMove;

		auto* vp = ImGui::GetMainViewport();
		ImGui::SetNextWindowViewport( vp->ID );

		const ImVec2 radial_size = vp->Size * 10;

		ImGui::SetNextWindowPos( radial_open_pos - radial_size / 2 );
		ImGui::SetNextWindowSize( radial_size );

		auto lock = ImGuiEx::BeginPopup( "RadialPalette", flags );
		if( !lock )
		{
			radial_open = false;
			return;
		}

		auto* draw_list = ImGui::GetWindowDrawList();

		if( ImGui::IsKeyDown( VK_SHIFT ) )
		{
			// Weight tool
			weight = ( mouse_pos - radial_open_pos ).len();
			draw_list->AddCircleFilled( radial_open_pos, weight, colour, static_cast<int>( weight * 2 ) );
		}
		else
		{
			const auto& IO = ImGui::GetIO();

			const float wheel_thickness = 27.0f;
			const float wheel_r_outer = 92.0f;
			const float wheel_r_inner = 64.0f;
			const float inner_ring = 125.0f;
			const float outer_ring = inner_ring + 50.0f;
			const float sv_picker_size = 185.0f;

			// Note: the triangle is displayed rotated with triangle_pa pointing to Hue, but most coordinates stays unrotated for logic.
			const float triangle_r = wheel_r_inner - (int)( sv_picker_size * 0.027f );
			const ImVec2 triangle_pa = ImVec2( triangle_r, 0.0f ); // Hue point.
			const ImVec2 triangle_pb = ImVec2( triangle_r * -0.5f, triangle_r * -0.866025f ); // Black point.
			const ImVec2 triangle_pc = ImVec2( triangle_r * -0.5f, triangle_r * +0.866025f ); // White point.

			auto [R,G,B,A] = ImGui::ColorConvertU32ToFloat4( colour );
			float H, S, V;
			ImGui::ColorConvertRGBtoHSV( R, G, B, H, S, V );

			const float backup_H = H;
			const float backup_S = S;
			const float backup_V = V;

			bool value_changed = false;
			bool value_changed_h = false;
			bool value_changed_sv = false;

			ImVec2 initial_off = radial_open_pos;
			ImVec2 current_off = IO.MousePos - radial_open_pos;
			float initial_dist2 = ImLengthSqr( current_off );
			if( initial_dist2 >= ( wheel_r_inner - 1 ) * ( wheel_r_inner - 1 ) && initial_dist2 <= ( inner_ring - 1 ) * ( inner_ring - 1 ) )
			{
				// Interactive with Hue wheel
				H = ImAtan2( current_off.y, current_off.x ) / IM_PI * 0.5f;
				if( H < 0.0f )
					H += 1.0f;
				value_changed = value_changed_h = true;
			}
			float cos_hue_angle = ImCos( -H * 2.0f * IM_PI );
			float sin_hue_angle = ImSin( -H * 2.0f * IM_PI );

			//if( ImTriangleContainsPoint( triangle_pa, triangle_pb, triangle_pc, ImRotate( current_off, cos_hue_angle, sin_hue_angle ) ) )
			if( initial_dist2 < ( wheel_r_inner - 1 ) * ( wheel_r_inner - 1 ) )
			{
				// Interacting with SV triangle
				ImVec2 current_off_unrotated = ImRotate( current_off, cos_hue_angle, sin_hue_angle );
				if( !ImTriangleContainsPoint( triangle_pa, triangle_pb, triangle_pc, current_off_unrotated ) )
					current_off_unrotated = ImTriangleClosestPoint( triangle_pa, triangle_pb, triangle_pc, current_off_unrotated );
				float uu, vv, ww;
				ImTriangleBarycentricCoords( triangle_pa, triangle_pb, triangle_pc, current_off_unrotated, uu, vv, ww );
				V = ImClamp( 1.0f - vv, 0.0001f, 1.0f );
				S = ImClamp( uu / V, 0.0001f, 1.0f );
				value_changed = value_changed_sv = true;
			}

			ImGui::ColorConvertHSVtoRGB( H, S, V, R, G, B );
			auto temp_colour = ImGui::ColorConvertFloat4ToU32( ImVec4( R, G, B, A ) );

			if( value_changed && closing )
			{
				if( !value_changed_sv )
				{
					S = 1.0f;
					V = 1.0f;
				}
				if( !value_changed_h )
				{
					H = backup_H;
				}

				ImGui::ColorConvertHSVtoRGB( H, S, V, R, G, B );
				colour = ImGui::ColorConvertFloat4ToU32( ImVec4( R, G, B, A ) );
			}

			const ImU32 hue_colors[6 + 1] = { IM_COL32( 255,0,0,255 ), IM_COL32( 255,255,0,255 ), IM_COL32( 0,255,0,255 ), IM_COL32( 0,255,255,255 ), IM_COL32( 0,0,255,255 ), IM_COL32( 255,0,255,255 ), IM_COL32( 255,0,0,255 ) };

			ImVec4 hue_color_f( 1, 1, 1, 1 ); ImGui::ColorConvertHSVtoRGB( H, 1, 1, hue_color_f.x, hue_color_f.y, hue_color_f.z );
			ImU32 hue_color32 = ImGui::ColorConvertFloat4ToU32( hue_color_f );

			ImVec2 sv_cursor_pos;

			// Render Hue Wheel
			const float aeps = 1.5f / wheel_r_outer; // Half a pixel arc length in radians (2pi cancels out).
			const int segment_per_arc = ImMax( 4, (int)wheel_r_outer / 12 );
			for( int n = 0; n < 6; n++ )
			{
				const float a0 = ( n ) / 6.0f * 2.0f * IM_PI - aeps;
				const float a1 = ( n + 1.0f ) / 6.0f * 2.0f * IM_PI + aeps;
				const int vert_start_idx = draw_list->VtxBuffer.Size;
				draw_list->PathArcTo( radial_open_pos, ( wheel_r_inner + wheel_r_outer ) * 0.5f, a0, a1, segment_per_arc );
				draw_list->PathStroke( IM_COL32_WHITE, false, wheel_thickness );
				const int vert_end_idx = draw_list->VtxBuffer.Size;

				// Paint colors over existing vertices
				ImVec2 gradient_p0( radial_open_pos.x + ImCos( a0 ) * wheel_r_inner, radial_open_pos.y + ImSin( a0 ) * wheel_r_inner );
				ImVec2 gradient_p1( radial_open_pos.x + ImCos( a1 ) * wheel_r_inner, radial_open_pos.y + ImSin( a1 ) * wheel_r_inner );
				ImGui::ShadeVertsLinearColorGradientKeepAlpha( draw_list, vert_start_idx, vert_end_idx, gradient_p0, gradient_p1, hue_colors[n], hue_colors[n + 1] );
			}

			// Render Cursor + preview on Hue Wheel
			cos_hue_angle = ImCos( H * 2.0f * IM_PI );
			sin_hue_angle = ImSin( H * 2.0f * IM_PI );
			ImVec2 hue_cursor_pos( radial_open_pos.x + cos_hue_angle * ( wheel_r_inner + wheel_r_outer ) * 0.5f, radial_open_pos.y + sin_hue_angle * ( wheel_r_inner + wheel_r_outer ) * 0.5f );
			float hue_cursor_rad = value_changed_h ? wheel_thickness * 0.65f : wheel_thickness * 0.55f;
			int hue_cursor_segments = ImClamp( (int)( hue_cursor_rad / 1.4f ), 9, 32 );

			if( value_changed_h )
			{
				draw_list->AddCircleFilled( hue_cursor_pos, hue_cursor_rad, hue_color32, hue_cursor_segments );
				draw_list->AddCircle( hue_cursor_pos, hue_cursor_rad + 1, IM_COL32( 128, 128, 128, 255 ), hue_cursor_segments );
				draw_list->AddCircle( hue_cursor_pos, hue_cursor_rad, IM_COL32_WHITE, hue_cursor_segments );
			}

			// Render SV triangle (rotated according to hue)
			ImVec2 tra = radial_open_pos + ImRotate( triangle_pa, cos_hue_angle, sin_hue_angle );
			ImVec2 trb = radial_open_pos + ImRotate( triangle_pb, cos_hue_angle, sin_hue_angle );
			ImVec2 trc = radial_open_pos + ImRotate( triangle_pc, cos_hue_angle, sin_hue_angle );
			ImVec2 uv_white = ImGui::GetFontTexUvWhitePixel();
			draw_list->PrimReserve( 6, 6 );
			draw_list->PrimVtx( tra, uv_white, hue_color32 );
			draw_list->PrimVtx( trb, uv_white, hue_color32 );
			draw_list->PrimVtx( trc, uv_white, IM_COL32_WHITE );
			draw_list->PrimVtx( tra, uv_white, IM_COL32_BLACK_TRANS );
			draw_list->PrimVtx( trb, uv_white, IM_COL32_BLACK );
			draw_list->PrimVtx( trc, uv_white, IM_COL32_BLACK_TRANS );
			draw_list->AddTriangle( tra, trb, trc, IM_COL32( 128, 128, 128, 255 ), 1.5f );
			sv_cursor_pos = ImLerp( ImLerp( trc, tra, ImSaturate( S ) ), trb, ImSaturate( 1 - V ) );

		    // Render cursor/preview circle (clamp S/V within 0..1 range because floating points colors may lead HSV values to be out of range)

			if( value_changed_sv )
			{
				const float sv_cursor_rad = 10.0f;
				draw_list->AddCircleFilled( sv_cursor_pos, sv_cursor_rad, temp_colour, 12 );
				draw_list->AddCircle( sv_cursor_pos, sv_cursor_rad + 1, IM_COL32( 128, 128, 128, 255 ), 12 );
				draw_list->AddCircle( sv_cursor_pos, sv_cursor_rad, IM_COL32_WHITE, 12 );
			}

			const auto button_size = ImVec2( 100, 0 );
			const int num_modes = static_cast<int>( Mode::NumModes );

			const float between_angle = 2.0f * IM_PI / num_modes;
			const float button_length = inner_ring + ( outer_ring - inner_ring ) * 0.25f;

			for( int i = 0; i < num_modes; ++i )
			{
				const Mode mode = static_cast<Mode>( i );

				const float angle = i * between_angle;

				const float left = angle - between_angle * 0.5f;
				const float right = angle + between_angle * 0.5f;

				auto dir = ImVec2( sin( angle ), -cos( angle ) );
				auto left_dir = ImVec2( sin( left ), -cos( left ) );
				auto right_dir = ImVec2( sin( right ), -cos( right ) );

				const float dot = dir.dot( left_dir );

				const auto mouse_offset = mouse_pos - radial_open_pos;
				const auto mouse_dir = mouse_offset.normalized();
				const float mouse_dot = dir.dot( mouse_dir );

				const bool highlighted = mouse_dot > dot
					&& mouse_offset.len() >= inner_ring
					&& mouse_offset.len() <= outer_ring;

				const auto segment_colour = highlighted ? 0xCC33FF33 : 0xCC333333;
				if( highlighted && closing )
					edit_mode = mode;

				ImVec2 points[] =
				{
					radial_open_pos + left_dir * inner_ring,
					radial_open_pos + dir * inner_ring,
					radial_open_pos + right_dir * inner_ring,
					radial_open_pos + right_dir * outer_ring,
					radial_open_pos + dir * outer_ring,
					radial_open_pos + left_dir * outer_ring,
					radial_open_pos + left_dir * inner_ring,
				};

				draw_list->AddConvexPolyFilled( points, std::size( points ), segment_colour );

				ImGui::SetCursorPos( radial_size / 2 + dir * button_length - button_size * 0.5f );
				ImGui::Button( ModeToString( mode ), button_size );
			}

			if( value_changed_h )
				draw_list->AddLine( radial_open_pos, hue_cursor_pos, 0xFFFFFFFF, 2.0f );
			else if( value_changed_sv )
				draw_list->AddLine( radial_open_pos, sv_cursor_pos, 0xFFFFFFFF, 2.0f );
			else
				draw_list->AddLine( radial_open_pos, mouse_pos, 0xFFFFFFFF, 2.0f );
		}

		if( closing )
		{
			ImGui::CloseCurrentPopup();
			radial_open = false;
		}
	}

	void PaintPalette::Render()
	{
		auto scoped_palette = ImGuiEx::Begin( "Palette" );
		if( !scoped_palette )
			return;

		if( ImGuiExScope::BeginCombo( "Mode", ModeToString( edit_mode ) ) )
		{
			ForEachMode( [&]( const Mode mode )
			{
				if( ImGui::Selectable( ModeToString( mode ), edit_mode == mode ) )
					edit_mode = mode;
			} );
		}

		auto colour_widget = []( const char* label, ImU32& colour )
		{
			auto colour4 = ImGui::ColorConvertU32ToFloat4( colour );
			if( ImGui::ColorEdit4( label, (float*)& colour4 ) )
				colour = ImGui::ColorConvertFloat4ToU32( colour4 );
		};

		colour_widget( "Background Colour", background_colour );
		colour_widget( "Canvas Colour", canvas_colour );
		colour_widget( "Colour", colour );

		ImGui::InputFloat( "Weight", &weight, 0.25f, 1.0f, 2 );
		ImGui::Checkbox( "Filled", &filled );

		RenderRadialMenu();

		if( !radial_open )
		{
			if( ImGui::IsKeyDown( VK_MENU ) )
			{
				auto* instance = Paint::GetInstance();
				const auto window_pos = instance->GetCanvas().GetTL();
				const auto mouse_pos = ImGui::GetMousePos();
				ImGui::SetTooltip( "%f %f", mouse_pos.x - window_pos.x, mouse_pos.y - window_pos.x );
			}

			if( ImGui::IsKeyPressed( VK_ESCAPE ) )
				edit_mode = Mode::Brush;
		}
	}
}