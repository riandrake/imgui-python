#pragma once

#include <Common/ImGui/ImGuiEx.h>
#include <Common/Include/nlohmann/json.hpp>

using json = nlohmann::json;

namespace Paint
{
	enum class Shapes
	{
		None = -1,
		Text,
		Line,
		Circle,
		Rectangle,
		Brush,
		NumShapes
	};

	struct Shape
	{
	protected:
		Shape( const Shapes type );
		Shape( const Shapes type, const json& shape );

	public:
		unsigned GetColour( float opacity ) const;

		static std::unique_ptr<Shape> make_shape( const Shapes shape );
		std::unique_ptr<Shape> clone() const;

		template<typename T>       T* As()       { return T::GetType() == type ? static_cast<      T*>( this ) : nullptr; }
		template<typename T> const T* As() const { return T::GetType() == type ? static_cast<const T*>( this ) : nullptr; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) {}
		virtual json Serialise();

		static std::unique_ptr<Shape> Deserialise( const json& shape );
		static const char* ShapeToString( const Shapes shape );
		static Shapes StringToShape(const std::string& shape );

		Shapes type = Shapes::None;
		ImVec2 start;
		unsigned colour;
		float weight;
	};

	struct Text : public Shape
	{
		Text() : Shape( GetType() ) {}
		Text( const json& text );

		static Shapes GetType() { return Shapes::Text; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) override;
		virtual json Serialise() override;

		std::string text;
	};

	struct Line : public Shape
	{
		Line() : Shape( GetType() ) {}
		Line( const json& line );

		static Shapes GetType() { return Shapes::Line; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) override;
		virtual json Serialise() override;

		ImVec2 end;
	};

	struct Circle : public Shape
	{
		Circle() : Shape( GetType() ) {}
		Circle( const json& circle );

		static Shapes GetType() { return Shapes::Circle; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) override;
		virtual json Serialise() override;

		float radius;
		bool filled;
	};

	struct Rectangle : public Shape
	{
		Rectangle() : Shape( GetType() ) {}
		Rectangle( const json& rectangle );

		static Shapes GetType() { return Shapes::Rectangle; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) override;
		virtual json Serialise() override;

		ImVec2 end;
		bool filled;
	};

	struct Brush : public Shape
	{
		Brush() : Shape( GetType() ) {}
		Brush( const json& line );

		static Shapes GetType() { return Shapes::Brush; }

		virtual void Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity ) override;
		virtual json Serialise() override;

		std::vector<ImVec2> points;
	};
}