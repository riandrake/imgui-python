mod
	.def("Clear", &Clear)
	.def("DrawLine", &DrawLine, py::arg("p0"), py::arg("p1"))
	.def("DrawRectangle", &DrawRectangle, py::arg("p0"), py::arg("p1"))
	.def("DrawCircle", &DrawCircle, py::arg("p"), py::arg("r"))
	.def("GetCanvasSize", &GetCanvasSize)
	.def("SetCanvasSize", &SetCanvasSize, py::arg("size"))
	.def("SetCanvasSize", &SetCanvasSize_2, py::arg("x"), py::arg("y"))
	.def("GetPalette", &GetPalette, py::return_value_policy::reference)
;
