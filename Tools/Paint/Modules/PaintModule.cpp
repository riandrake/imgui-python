#include "PaintModule.h"

#include <Tools/Paint/PaintInstance.h>
#include <Tools/Paint/PaintProgram.h>
#include <Tools/Paint/PaintPalette.h>

namespace Python::Modules::PaintModule
{
	GENERATE void Clear()
	{
		if( auto * program = Paint::GetProgram() )
			program->New();
	}

	GENERATE void DrawLine( const ImVec2& p0, const ImVec2& p1 )
	{
		if( auto * instance = Paint::GetInstance() )
			instance->DrawLine( p0, p1 );
	}

	GENERATE void DrawRectangle( const ImVec2& p0, const ImVec2& p1 )
	{
		if( auto * instance = Paint::GetInstance() )
			instance->DrawRectangle( p0, p1 );
	}

	GENERATE void DrawCircle( const ImVec2& p, float r )
	{
		if( auto * instance = Paint::GetInstance() )
			instance->DrawCircle( p, r );
	}

	GENERATE ImVec2 GetCanvasSize()
	{
		if( auto * instance = Paint::GetInstance() )
			return instance->GetCanvas().GetSize();

		return ImVec2( 0, 0 );
	}

	GENERATE void SetCanvasSize( const ImVec2& size )
	{
		if( auto * instance = Paint::GetInstance() )
			return instance->SetCanvasSize( size );
	}

	GENERATE void SetCanvasSize_2( const float x, const float y )
	{
		if( auto * instance = Paint::GetInstance() )
			return instance->SetCanvasSize( ImVec2( x, y ) );
	}

	GENERATE REFERENCE Paint::PaintPalette* GetPalette()
	{
		if( auto * program = Paint::GetProgram() )
			return &program->GetPalette();

		return nullptr;
	}

	void Initialise( py::module& mod )
	{
		using namespace Paint;

		py_class1( PaintPalette )
			.py_write( PaintPalette, colour )
			.py_write( PaintPalette, background_colour )
			.py_write( PaintPalette, canvas_colour )
			.py_write( PaintPalette, weight )
			.py_write( PaintPalette, filled );

	#include "PaintModule.inc"
	}
}