#pragma once

#include <Common/Python/Module.h>

namespace Python::Modules::PaintModule
{
	void Initialise( py::module& mod );
}