#include "PaintInstance.h"

#include <windows.h>
#include <fstream>
#include <winuser.h>
#include <assert.h>
#include <algorithm>
#include <utility>
#include <iomanip>

#include <fmt/format.h>
#include <nlohmann/json.hpp>

#include <Common/Utility/Enumerate.h>
#include <Common/Utility/FileIO.h>
#include <Common/Utility/Exception.h>
#include <Common/Utility/ScopedValueChange.h>

#include "PaintPalette.h"
#include "PaintProgram.h"

namespace Paint
{
	PaintInstance::PaintInstance( const PaintPalette& palette )
		: palette( palette )
	{
		if( auto default_layer = std::make_unique<PaintLayer>( palette, *this ) )
		{
			default_layer->SetName( "Background" );
			layers.emplace_back( std::move( default_layer ) );
		}

		Initialise();
	}

	PaintInstance::PaintInstance( const std::string& _filename, const PaintPalette& palette )
		: palette( palette )
		, filename( _filename )
	{
		if( filename.front() == '"' )
			filename.erase( 0, 1 );
		if( filename.back() == '"' )
			filename.pop_back();

		std::ifstream i( filename );
		json doc;
		i >> doc;

		const auto& s = doc["size"];
		ImVec2 size;
		size.x = s[0];
		size.y = s[1];
		canvas.Expand( size - canvas.GetSize() );

		for( auto& l : doc["layers"] )
		{
			auto new_layer = std::make_unique<PaintLayer>( palette, *this );
			new_layer->Deserialise( l );
			layers.emplace_back( std::move( new_layer ) );
		}

		Initialise();
	}

	void PaintInstance::Initialise()
	{
		move_op.SetButton( Buttons::MMB );
		move_op.SetChangedCallback( [&]() { canvas.Translate( move_op.end - move_op.last ); } );

		drag_op.SetButton( Buttons::LMB );
		drag_op.SetStartedCallback( [&]() { DragStarted(); } );
		drag_op.SetFinishedCallback( [&]() { DragFinished(); } );
		drag_op.SetChangedCallback( [&]() { DragChanged(); } );
	}

	void PaintInstance::Save()
	{
		if( !HasFilename() )
		{
			SaveAs();
			return;
		}

		using json = nlohmann::json;

		json doc;
		doc["version"] = PROGRAM_VERSION;

		const auto size = canvas.GetSize();
		doc["size"] = { size.x, size.y };

		json layer_array = json::array();
		for( auto& layer : layers )
			layer_array.push_back( layer->Serialise() );

		doc["layers"] = std::move( layer_array );

		std::ofstream o( filename );
		o << std::setw( 4 ) << doc << std::endl;
	}

	void PaintInstance::SaveAs()
	{
		filename = File::OpenFileDialog();
		if( filename.empty() )
			return;

		Save();
	}

	void PaintInstance::Render()
	{
		if( first_frame )
		{
			first_frame = false;
			ResetCanvasView();
		}

		if( drag_op.Dragging() || !( ImGui::IsAnyWindowHovered() || ImGui::IsAnyWindowFocused() ) )
			drag_op.Tick();

		auto* vp = ImGui::GetMainViewport();
		ImGui::SetNextWindowViewport( vp->ID );
		ImGui::SetNextWindowPos( vp->Pos + canvas.GetTL() );

		const auto flags = ImGuiWindowFlags_NoTitleBar
			| ImGuiWindowFlags_NoMove
			| ImGuiWindowFlags_NoResize
			| ImGuiWindowFlags_NoInputs
			| ImGuiWindowFlags_NoScrollbar
			| ImGuiWindowFlags_NoCollapse;

		const ImVec2 offset = canvas.GetTL() + ImGui::GetWindowPos();

		auto scoped_colour = ImGuiEx::PushStyleColour( ImGuiCol_ChildWindowBg, palette.canvas_colour );
		if( ImGuiExScope::BeginChild( HasFilename() ? filename.c_str() : "New Image", canvas.GetSize() * zoom, true, flags ) )
		{
			unsigned idx = layers.size();
			std::for_each( layers.rbegin(), layers.rend(), [&]( const auto& layer )
			{
				--idx;
				layer->Render( offset );

				if( idx == active_layer )
					RenderPreview();
			} );
		}
		scoped_colour = nullptr;

		RenderLayers();

		move_op.Tick();
		UpdateZoom();

		HandleLayerOperation();
	}

	void PaintInstance::HandleLayerOperation()
	{
		if( layer_op == LayerOp::None )
			return;

		const auto flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize;
		const auto button_size = ImVec2( 50, 0 );

		switch( layer_op )
		{
		case LayerOp::Merge:
			{
				MergeActiveLayer();
				layer_op = LayerOp::None;
			}
			break;
		case LayerOp::Delete:
			{
				DeleteActiveLayer();
				layer_op = LayerOp::None;
			}
			break;
		case LayerOp::Rename:
			{
				ImGui::OpenPopup( "Rename" );

				if( ImGuiExScope::BeginPopupModal( "Rename", nullptr, flags ) )
				{
					const bool done = ImGui::InputText( "New Name", scratches[LayerName], MaxNameLength, ImGuiInputTextFlags_EnterReturnsTrue );

					ImGui::NewLine();
					ImGui::SameLine( ImGui::GetContentRegionAvailWidth() / 2.0f - button_size.x );

					if( ImGui::Button( "Ok", button_size ) || done )
					{
						layers[active_layer]->SetName( scratches[LayerName] );
						layer_op = LayerOp::None;
						ImGui::CloseCurrentPopup();
					}

					ImGui::SameLine();

					if( ImGui::Button( "Cancel", button_size ) )
					{
						layer_op = LayerOp::None;
						ImGui::CloseCurrentPopup();
					}
				}
			}
			break;
		}
	}

	void PaintInstance::Undo()
	{
		if( !done_stack.empty() )
		{
			done_stack.top()->Undo();
			undone_stack.push( done_stack.top() );
			done_stack.pop();
		}
	}

	void PaintInstance::Redo()
	{
		if( !undone_stack.empty() )
		{
			undone_stack.top()->Do();
			done_stack.push( undone_stack.top() );
			undone_stack.pop();
		}
	}


	void PaintInstance::DrawFreeline( const ImVec2& start, std::vector<ImVec2>&& points )
	{
		Brush line;
		line.colour = palette.colour;
		line.weight = palette.weight;
		line.start = start;
		line.points = std::move( points );
		DoAddShape( line );
	}

	void PaintInstance::DrawLine( const ImVec2& start, const ImVec2& end )
	{
		Line line;
		line.colour = palette.colour;
		line.weight = palette.weight;
		line.start = start;
		line.end = end;
		DoAddShape( line );
	}

	void PaintInstance::DrawCircle( const ImVec2& start, const float radius )
	{
		Circle circle;
		circle.colour = palette.colour;
		circle.weight = palette.weight;
		circle.start = start;
		circle.radius = radius;
		circle.filled = palette.filled;
		DoAddShape( circle );
	}

	void PaintInstance::DrawRectangle( const ImVec2& start, const ImVec2& end )
	{
		Rectangle rectangle;
		rectangle.colour = palette.colour;
		rectangle.weight = palette.weight;
		rectangle.start = start;
		rectangle.end = end;
		rectangle.filled = palette.filled;
		DoAddShape( rectangle );
	}

	void PaintInstance::RenderLayers()
	{
		auto scoped_window = ImGuiEx::Begin( "Layers", nullptr );
		if( !scoped_window )
			return;

		if( ImGui::Button( "New" ) )
		{
			auto new_layer = std::make_unique<PaintLayer>( palette, *this );
			new_layer->SetName( fmt::format( "Layer {}", layers.size() ) );
			layers.emplace_back( std::move( new_layer ) );
			active_layer = layers.size() - 1;
		}

		ImGui::SameLine();

		if( ImGui::Button( "Rename" ) )
		{
			ClearScratch( LayerName );
			layers[active_layer]->GetName().copy( scratches[LayerName], MaxNameLength );
			layer_op = LayerOp::Rename;
		}

		ImGui::SameLine();

		if( ImGuiEx::Button( "Delete", layers.size() > 1 ) )
		{
			layer_op = LayerOp::Delete;
		}

		ImGui::SameLine();

		if( ImGuiEx::Button( "Merge Down", active_layer + 1 < layers.size() ) )
		{
			layer_op = LayerOp::Merge;
		}

		ImGui::Separator();

		unsigned move( -1 ), to( -1 );

		for( auto [idx, layer] : enumerate( layers ) )
		{
			auto id = ImGuiEx::PushID( idx );

			if( ImGuiExScope::BeginGroup() )
			{
				if( ImGuiEx::Checkbox( "##Visible", layer->IsVisible() ) )
					layer->SetVisible( !layer->IsVisible() );

				ImGui::SameLine();

				if( ImGui::Selectable( layer->GetName().c_str(), idx == active_layer ) )
					active_layer = idx;

				if( ImGuiExScope::Indent() )
				{
					float f = layer->GetOpacity();
					if( ImGui::SliderFloat( "Opacity", &f, 0.0f, 1.0f ) )
						layer->SetOpacity( f );
				}
			}

			if( ImGuiExScope::BeginDragDropSource( ImGuiDragDropFlags_SourceAllowNullID ) )
			{
				ImGui::SetDragDropPayload( "INDEX", &idx, sizeof( unsigned ), ImGuiCond_Always );

				// Preview
				ImGui::Selectable( layer->GetName().c_str() );
				ImGuiEx::Checkbox( "Visible", layer->IsVisible() );
			}

			if( ImGuiExScope::BeginDragDropTarget() )
			{
				if( auto* payload = ImGui::AcceptDragDropPayload( "INDEX" ) )
				{
					move = *(unsigned*)payload->Data;
					to = idx;
				}
			}
		}

		if( move != -1 )
		{
			active_layer = to;

			if( to < move )
			{
				// Rotate Up
				to = layers.size() - to - 1;
				move = layers.size() - move - 1;

				std::rotate( layers.rbegin() + move, layers.rbegin() + move + 1, layers.rbegin() + to + 1 );
			}
			else
			{
				// Rotate Down
				std::rotate( layers.begin() + move, layers.begin() + move + 1, layers.begin() + to + 1 );
			}
		}
	}

	void PaintInstance::RenderPreview()
	{
		if( !preview )
		{
			if( palette.edit_mode != Mode::Text )
				return;
			preview = std::make_unique<Text>();
			preview->As<Text>()->text = scratches[TextObject];
		}

		if( preview->type != ModeToShape( palette.edit_mode ) )
		{
			preview = nullptr;
			return;
		}

		auto* draw_list = ImGui::GetWindowDrawList();
		const float weight = palette.weight * zoom;

		const auto pos = ( ImGui::GetWindowPos() );
		const auto end = ( drag_op.end - pos ) / zoom;

		switch( preview->type )
		{
		case Shapes::Brush:
			break;
		case Shapes::Line:
			if( auto * line = preview->As<Line>() )
			{
				line->end = end;
			}
			break;
		case Shapes::Circle:
			if( auto * circle = preview->As<Circle>() )
			{
				circle->radius = ( end - circle->start ).len();
				circle->filled = palette.filled;
			}
			break;
		case Shapes::Rectangle:
			if( auto * rect = preview->As<Rectangle>() )
			{
				rect->end = end;
				rect->filled = palette.filled;
			}
			break;
		case Shapes::Text:
			if( auto* text = preview->As<Text>() )
			{
				const auto flags = ImGuiWindowFlags_NoResize
					| ImGuiWindowFlags_AlwaysAutoResize
					| ImGuiWindowFlags_NoMove
					| ImGuiWindowFlags_NoBackground
					| ImGuiWindowFlags_NoNav
					| ImGuiWindowFlags_NoMouseInputs
					| ImGuiWindowFlags_NoDecoration;

				auto* vp = ImGui::GetMainViewport();

				const float width = 600;
				ImGui::SetNextWindowPos( vp->Pos + ImVec2( vp->Size.x / 2, vp->Size.y - 200 ) - ImVec2( width / 2, 0 ), ImGuiCond_Always );
				ImGui::SetNextWindowSize( ImVec2( width, 100 ), ImGuiCond_Always );

				if( ImGuiExScope::Begin( "##InvisibleWindow", nullptr, flags ) )
				{
					auto lock = ImGuiEx::PushItemWidth( -1 );

					const auto flags = ImGuiInputTextFlags_NoUndoRedo;

					auto& io = ImGui::GetIO();

					// Temporarily pretend that the left mouse never gets clicked to avoid dumb highlighting behaviour I don't want
					if( auto lock_lmb = ScopedValueChange( io.MouseDown[int( Buttons::LMB )], false ) )
					{
						if( ImGui::InputTextMultiline( "##InvisibleInput", scratches[TextObject], MaxNameLength, ImVec2{ 0,0 }, flags ) )
							text->text = scratches[TextObject];
					}

					if( !ImGui::IsMouseDown( (int)Buttons::RMB ) && !ImGui::IsAnyWindowHovered() && !ImGui::IsItemFocused() )
					{
						ImGui::SetKeyboardFocusHere();
					}

					text->start = ( ImGui::GetMousePos() - pos ) / zoom;
					//text->start = ( ImGui::GetMousePos() - pos - ImVec2( 0, 2 * palette.weight * zoom ) ) / zoom;

					// Temporarily increase the repeat delay
					auto repeat_delay = ScopedValueChange( io.KeyRepeatDelay, 0.5f );
					auto repeat_rate = ScopedValueChange( io.KeyRepeatRate, 0.1f );

					if( text->text.empty() )
						ImGui::SetTooltip( "Start typing.." );
					else if( ImGui::IsMouseClicked( (int)Buttons::LMB, true ) )
						DoAddShape( *text );
				}
			}
			break;
		}

		preview->weight = palette.weight;
		preview->colour = palette.colour;
		preview->Render( draw_list, pos, zoom, layers[active_layer]->GetOpacity() );
	}

	void PaintInstance::Do( Action::Callback_t do_action, Action::Callback_t undo_action )
	{
		if( auto new_action = std::make_shared<Action>( do_action, undo_action ) )
		{
			new_action->Do();
			done_stack.push( std::move( new_action ) );
		}
	}

	void PaintInstance::ResetCanvasView()
	{
		const auto size = canvas.GetSize();
		const auto display_size = ImGui::GetIO().DisplaySize;

		const auto center = canvas.GetTL() + size / 2;
		const auto display_center = display_size / 2;

		const auto diff = display_center - center;
		canvas.Translate( diff );

		SetCanvasZoom( 1.0f );
	}

	void PaintInstance::SetCanvasPosition( const ImVec2& pos )
	{
		canvas.Translate( pos - canvas.GetTL() );
	}

	void PaintInstance::SetCanvasZoom( const float zoom )
	{
		this->zoom = zoom;
	}

	void PaintInstance::SetCanvasSize( const ImVec2& size )
	{
		const auto old = canvas.Max;

		Do( [=]()
		{
			const auto movement = size - canvas.GetSize();
			canvas.Max += movement;
		},
		[=]()
		{
			canvas.Max = old;
		} );
	}

	void PaintInstance::UpdateZoom()
	{
		if( !ImGui::IsMousePosValid() )
			return;

		auto& io = ImGui::GetIO();
		if( !io.MouseWheel )
			return;

		auto* vp = ImGui::GetMainViewport();

		const auto centre = vp->Pos + canvas.GetTL();
		const auto offset = ( ImGui::GetMousePos() - centre ) / zoom;

		zoom += io.MouseWheel * 0.1f;
		zoom = std::max( 0.1f, zoom );
		zoom = std::min( 10.0f, zoom );

		const auto goal_offset = offset * zoom;
		canvas.Translate( ImGui::GetMousePos() - goal_offset - vp->Pos - canvas.GetTL() );
	}

	void PaintInstance::DragStarted()
	{
		if( palette.edit_mode == Mode::Text )
			return;

		const auto palette_shape = ModeToShape( palette.edit_mode );
		preview = Shape::make_shape( palette_shape );
		zoom = zoom;

		const auto pos = ( ImGui::GetWindowPos() + canvas.GetTL() );
		const auto start = ( drag_op.begin - pos ) / zoom;
		preview->start = start;
	}

	void PaintInstance::DragChanged()
	{
		if( !preview )
			return;

		if( auto* freeline = preview->As<Brush>() )
		{
			const auto pos = ( ImGui::GetWindowPos() + canvas.GetTL() );
			const auto start = ( drag_op.begin - pos ) / zoom;
			const auto end = ( drag_op.end - pos ) / zoom;

			const auto& last = freeline->points.empty() ? start : freeline->points.back();
			const auto diff = end - last;
			const auto req_dist = palette.weight / 2.0f;

			if( diff.sqrlen() > req_dist * req_dist )
				freeline->points.emplace_back( end );
		}
	}

	void PaintInstance::DragFinished()
	{
		if( !preview )
			return;

		if( preview->type == Shapes::Text )
			return;

		const auto pos = ( ImGui::GetWindowPos() + canvas.GetTL() );

		const auto scaled_canvas = ImRect( pos, pos + canvas.GetSize() * zoom );
		if( !scaled_canvas.Contains( drag_op.begin ) && !scaled_canvas.Contains( drag_op.end ) )
			return;

		switch( preview->type )
		{
		case Shapes::Rectangle:
			DoAddShape( *preview->As<Rectangle>() );
			break;
		case Shapes::Circle:
			DoAddShape( *preview->As<Circle>() );
			break;
		case Shapes::Line:
			DoAddShape( *preview->As<Line>() );
			break;
		case Shapes::Brush:
			DoAddShape( *preview->As<Brush>() );
			break;
		default:
			throw RuntimeError( "Shape not implemented: {}", preview->type );
		}

		preview = nullptr;
	}

	Paint::PaintLayer* PaintInstance::GetLayerById( const unsigned id )
	{
		for( auto& layer : layers )
			if( layer->GetId() == id )
				return layer.get();

		return nullptr;
	}

	void PaintInstance::ClearScratch( Scratch scratch_type )
	{
		memset( scratches[scratch_type], '\0', MaxNameLength );
	}

	void PaintInstance::DeleteActiveLayer()
	{
		auto layer = active_layer;
		auto A = *layers[active_layer];

		Do( [=]()
		{
			layers.erase( layers.begin() + active_layer );
			active_layer = layer ? layer - 1 : 0;
		},
			[=]()
		{
			layers.insert( layers.begin() + layer, std::make_unique<PaintLayer>( A ) );
			active_layer = layer;
		} );
	}

	void PaintInstance::MergeActiveLayer()
	{
		assert( active_layer + 1 < layers.size() );
		if( active_layer + 1 >= layers.size() )
			return;

		unsigned from_layer = active_layer;
		unsigned to_layer = active_layer + 1;

		PaintLayer A = *layers[active_layer];
		PaintLayer B = *layers[active_layer + 1];

		Do( [=]()
		{
			layers[to_layer]->MergeWith( layers[from_layer] );
			DeleteActiveLayer();
		},
			[=]()
		{
			layers[to_layer] = std::make_unique<PaintLayer>( B );
		} );
	}
}