#pragma once

#include <string>
#include <vector>
#include <stack>

#include <Common/ImGui/ImGuiEx.h>
#include <Common/ImGui/DragOperation.h>

#include "PaintLayer.h"

namespace Paint
{
	template<int Button>
	struct Drag
	{
		ImVec2 begin;
	};

	class PaintPalette;

	class PaintInstance
	{
	public:
		PaintInstance( const PaintPalette& palette );
		PaintInstance( const std::string& filename, const PaintPalette& palette );

		void Initialise();

		const std::string& GetFilename() const { return filename; }
		void SetFilename( const std::string& name ) { filename = name; }
		bool HasFilename() const { return !filename.empty(); }

		void Save();
		void SaveAs();

		void Render();

		float GetZoom() const { return zoom; }

		void ResetCanvasView();
		void SetCanvasPosition( const ImVec2& pos );
		void SetCanvasZoom( const float zoom );
		void SetCanvasSize( const ImVec2& size );

		void Undo();
		void Redo();

		void DrawFreeline( const ImVec2& start, std::vector<ImVec2>&& points );
		void DrawLine( const ImVec2& start, const ImVec2& end );
		void DrawCircle( const ImVec2& start, const float radius );
		void DrawRectangle( const ImVec2& start, const ImVec2& end );

		const ImRect& GetCanvas() const { return canvas; }

	private:
		void RenderLayers();
		void RenderPreview();
		void HandleLayerOperation();

		void Do( Action::Callback_t do_action, Action::Callback_t undo_action );
		void UpdateZoom();

		void DragStarted();
		void DragChanged();
		void DragFinished();

		PaintLayer* GetLayerById( const unsigned id );

		enum Scratch { LayerName, TextObject, NumScratches };
		void ClearScratch( Scratch scratch_type );

		void DeleteActiveLayer();
		void MergeActiveLayer();

		template<class ShapeType>
		void DoAddShape( const ShapeType& shape )
		{
			const auto id = layers[active_layer]->GetId();

			Do( [=]() {
				if( auto* layer = GetLayerById( id ) )
					layer->AddShape( shape );
			}, [=]() {
				if( auto* layer = GetLayerById( id ) )
					layer->RemoveShape();
			} );
		}

		enum class LayerOp
		{
			None,
			Delete,
			Rename,
			Merge,
		};

		enum
		{
			MaxNameLength = 512
		};

		const PaintPalette& palette;

		std::string filename;

		char scratches[NumScratches][MaxNameLength];

		std::vector<std::unique_ptr<PaintLayer>> layers;
		LayerOp layer_op = LayerOp::None;
		unsigned active_layer = 0;

		std::stack<std::shared_ptr<Action>> done_stack;
		std::stack<std::shared_ptr<Action>> undone_stack;

		ImRect canvas = ImRect( 0, 0, 1024, 768 );
		float zoom = 1.0f;

		DragOperation move_op;
		DragOperation drag_op;

		std::unique_ptr<Shape> preview;
		bool zoom_lock = 1.0f;

		bool first_frame = true;
	};
}