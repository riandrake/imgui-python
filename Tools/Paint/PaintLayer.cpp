#include "PaintLayer.h"

#include <Common/ImGui/ImGuiEx.h>
#include <Common/Utility/Enumerate.h>

#include "PaintInstance.h"
#include "PaintPalette.h"

namespace Paint
{
	PaintLayer::PaintLayer( const PaintPalette& palette, const PaintInstance& instance )
		: palette( palette )
		, instance( instance )
		, id( next_id++ )
	{
	}

	PaintLayer::PaintLayer( const PaintLayer& other )
		: palette( other.palette )
		, instance( other.instance )
		, name( other.name )
		, visible( other.visible )
		, id( next_id++ )
	{
		shapes.resize( other.shapes.size() );
		for( auto [idx, shape] : enumerate( other.shapes ) )
			shapes[idx] = shape->clone();
	}

	void PaintLayer::Render( const ImVec2& offset )
	{
		if( !visible )
			return;
		if( opacity == 0.0f )
			return;

		auto* draw_list = ImGui::GetWindowDrawList();
		if( !draw_list )
			return;

		const float zoom = instance.GetZoom();

		for( auto& shape : shapes )
			shape->Render( draw_list, offset, zoom, opacity );
	}

	void PaintLayer::AddShape( const Shape& shape )
	{
		shapes.emplace_back( shape.clone() );
	}

	void PaintLayer::MergeWith( const std::unique_ptr<PaintLayer>& layer )
	{
		for( auto [idx, shape] : enumerate( layer->shapes ) )
			shapes.insert( shapes.begin() + idx, shape->clone() );
	}

	json PaintLayer::Serialise()
	{
		json out;

		out["name"] = name;
		out["opacity"] = opacity;
		out["visible"] = visible;

		auto shape_json = json::array();
		for( auto& shape : shapes )
			shape_json.push_back( shape->Serialise() );

		out["shapes"] = std::move( shape_json );

		return out;
	}

	void PaintLayer::Deserialise( const json& layer )
	{
		name = layer["name"];
		opacity = layer["opacity"].get<float>();
		visible = layer["visible"].get<bool>();

		for( auto& shape : layer["shapes"] )
			shapes.emplace_back( Shape::Deserialise( shape ) );
	}
}