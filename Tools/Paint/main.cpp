#include <PaintProgram.h>

#include <Common/Program/ProgramMain.h>

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int showCmd )
{
	Paint::PaintProgram app;
	Main::DoMain( app, hInstance, hPrevInstance, args, showCmd, true );
}