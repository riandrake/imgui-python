#include "PaintProgram.h"

#include <fstream>
#include <stdlib.h>
#include <filesystem>

#include <Common/ImGui/ImGuiEx.h>
#include <Common/Utility/Finally.h>
#include <Common/Utility/FileIO.h>
#include <Common/Utility/Logging.h>

#include "Modules/PaintModule.h"

namespace Paint
{
	PaintProgram* program = nullptr;

	void PaintProgram::OnDestroy()
	{
		interpreter = nullptr;
		program = nullptr;
	}

	void PaintProgram::HandleArguments( const std::string_view& args )
	{
		if( args.empty() )
			return;

		Open( args );
	}

	void PaintProgram::OnInitialise( const std::string_view& args )
	{
		Log::Info( "Launching program" );

		if( std::ifstream env( "environment_variables.ini" ); env.is_open() )
		{
			std::string line;
			while( std::getline( env, line ) )
			{
				Log::Info( "Setting environment variable: {}", line );
				_putenv( line.c_str() );
			}
		}
		size_t req;
		char buffer[MAX_PATH];
		if( getenv_s( &req, buffer, MAX_PATH, "PYTHONHOME" ) == 0 )
		{
			std::string&& path = std::string() + buffer + "\\python37.dll";
			if( std::filesystem::exists( path ) )
			{
				interpreter = std::make_unique<py::scoped_interpreter>();
			}
		}

		if( interpreter )
		{
			Log::Info( "Python enabled" );
			py::exec( "from PaintContext import *" );
		}
		else
		{
			Log::Info( "Python disabled" );
		}

		assert( !program );
		program = this;

		HandleArguments( args );

		if( !instance )
			New();
	}

	void PaintProgram::OnRender()
	{
		RenderCommandLine();

		if( show_demo )
			ImGui::ShowDemoWindow( &show_demo );

		const auto& io = ImGui::GetIO();

		auto* vp = ImGui::GetMainViewport();

		ImGui::SetNextWindowViewport( vp->ID );
		ImGui::SetNextWindowPos( vp->Pos, ImGuiCond_Always );
		ImGui::SetNextWindowSize( vp->Size, ImGuiCond_Always );

		const auto flags = ImGuiWindowFlags_NoTitleBar
			| ImGuiWindowFlags_NoMove
			| ImGuiWindowFlags_NoResize
			| ImGuiWindowFlags_NoInputs
			| ImGuiWindowFlags_NoScrollbar
			| ImGuiWindowFlags_MenuBar
			| ImGuiWindowFlags_NoCollapse;

		auto scoped_colour = ImGuiEx::PushStyleColour( ImGuiCol_WindowBg, palette.background_colour );

		if( ImGuiExScope::Begin( PROGRAM_NAME, nullptr, flags ) )
		{
			scoped_colour = nullptr;

			if( ImGuiExScope::BeginMainMenuBar() )
				RenderMainMenuBar();

			if( instance )
				instance->Render();

			palette.Render();
		}

		popup_handler.Render();
	}

	void PaintProgram::New()
	{
		instance = std::make_unique<PaintInstance>( palette );
	}

	void PaintProgram::Open()
	{
		Open( File::OpenFileDialog( true, nullptr, "Open Workspace", "JSON Files\0.*pfp" ) );
	}

	void PaintProgram::Open( const std::string_view& filename )
	{
		if( filename.empty() )
			return;

		if( filename[0] == '"' )
		{
			Open( filename.substr( 1, filename.size() - 2 ) );
			return;
		}

		if( filename.ends_with( ".pfp" ) )
		{
			instance = std::make_unique<PaintInstance>( std::string( filename ), palette );
		}
		else if( filename.ends_with( ".pfpy" ) )
		{
			if( !instance )
				New();

			if( interpreter )
			{

				std::ifstream file( filename );
				std::string content( ( std::istreambuf_iterator<char>( file ) ), ( std::istreambuf_iterator<char>() ) );

				try
				{
					py::exec( content );
				}
				catch( const py::error_already_set& e )
				{
					Log::Error( "Error while executing python script \"{}\". Error message:\n{}", e.what() );
				}
			}
			else
			{
				Log::Warn( "Python is not available right now, try installing Python 3.7 or setting the PYTHONHOME environment variable" );
			}
		}
	}

	void PaintProgram::Save()
	{
		if( instance )
			instance->Save();
	}

	void PaintProgram::SaveAs()
	{
		if( instance )
			instance->SaveAs();
	}

	void PaintProgram::SaveAs( const std::string& filename )
	{
		if( instance )
		{
			instance->SetFilename( filename );
			instance->Save();
		}
	}

	void PaintProgram::Close()
	{
		instance = nullptr;
	}

	void PaintProgram::Quit()
	{
		instance = nullptr;
		quit = true;
	}

	void PaintProgram::Undo()
	{
		if( instance )
			instance->Undo();
	}

	void PaintProgram::Redo()
	{
		if( instance )
			instance->Redo();
	}

	void PaintProgram::SetCanvasPosition( const ImVec2& pos )
	{
		if( instance )
			instance->SetCanvasPosition( pos );
	}

	void PaintProgram::SetCanvasZoom( const float zoom )
	{
		if( instance )
			instance->SetCanvasZoom( zoom );
	}

	void PaintProgram::ResetCanvasView()
	{
		instance->ResetCanvasView();
	}

	void PaintProgram::ResizeCanvas()
	{
		if( !instance )
			return;

		class ResizePopup : public ImGuiEx::Popup
		{
		public:
			ResizePopup( PaintProgram& program )
				: Popup( "Resize Canvas" )
				, program( program )
			{
				if( auto* instance = program.GetInstance() )
				{
					const auto current_size = instance->GetCanvas().GetSize();
					size[0] = static_cast<int>( current_size.x );
					size[1] = static_cast<int>( current_size.y );
				}
			}

			bool Render()
			{
				auto* instance = program.GetInstance();
				if( !instance )
					return true;

				const auto flags = ImGuiWindowFlags_AlwaysAutoResize
					| ImGuiWindowFlags_NoCollapse
					| ImGuiWindowFlags_NoResize;

				if( ImGuiExScope::BeginPopupModal( GetName(), nullptr, flags ) )
				{
					const auto button_size = ImVec2( 40, 0 );

					ImGui::TextUnformatted( "New Canvas Size" );
					ImGui::DragInt2( "##NewCanvasSizeInput", size );

					ImGui::NewLine();
					ImGui::SameLine( ImGui::GetContentRegionAvailWidth() / 2 - button_size.x );

					if( ImGui::Button( "Ok", button_size ) || ImGui::IsKeyPressed( VK_RETURN ) )
					{
						instance->SetCanvasSize( ImVec2( static_cast<float>( size[0] ), static_cast<float>( size[1] ) ) );
						ImGui::CloseCurrentPopup();
						return true;
					}

					ImGui::SameLine();

					if( ImGui::Button( "Cancel", button_size ) || ImGui::IsKeyPressed( VK_ESCAPE ) )
					{
						ImGui::CloseCurrentPopup();
						return true;
					}

					return false;
				}

				return true;
			}

			PaintProgram& program;
			int size[2];
		};

		popup_handler.AddPopup( std::make_unique<ResizePopup>( *this ) );
	}

	void PaintProgram::RenderMainMenuBar()
	{
		if( ImGuiExScope::BeginMenu( "File" ) )
		{
			if( ImGui::Selectable( "New (Ctrl-N)" ) )
				New();
			if( ImGui::Selectable( "Open (Ctrl-O)" ) )
				Save();
			if( ImGui::Selectable( "Save (Ctrl-S)" ) )
				Save();
			if( ImGui::Selectable( "Save As (Ctrl-Shift-S)" ) )
				SaveAs();
			if( ImGui::Selectable( "Close (Ctrl-Q)" ) )
				Close();
			if( ImGui::Selectable( "Quit (Ctrl-Shift-Q)" ) )
				Quit();
		}
		if( ImGuiExScope::BeginMenu( "Edit" ) )
		{
			if( ImGui::Selectable( "Undo (Ctrl-Z)" ) )
				Undo();
			if( ImGui::Selectable( "Redo (Ctrl-Shift-Z)" ) )
				Redo();
			if( ImGui::Selectable( "Resize Canvas (Ctrl-R)" ) )
				ResizeCanvas();
		}
		if( ImGuiExScope::BeginMenu( "View" ) )
		{
			if( ImGui::Selectable( "Reset View (Ctrl-F)" ) )
				ResetCanvasView();
		}

		ImGui::SameLine( ImGui::GetContentRegionMax().x - 40.0f );
		const auto frame_rate = ImGui::GetIO().Framerate;
		if( frame_rate > 100 )
			ImGui::TextUnformatted( "100+" );
		else
			ImGui::Text( "%.0f", frame_rate );
	}

	void PaintProgram::RenderCommandLine()
	{
		bool new_input = false;
		if( !commandline )
		{
			if( !ImGui::IsKeyPressed( VK_RETURN ) )
				return;
			if( ImGui::GetIO().WantCaptureKeyboard )
				return;

			memset( input, '\0', 1024 );

			commandline = true;
			new_input = true;
		}

		const auto flags = ImGuiWindowFlags_NoResize
			| ImGuiWindowFlags_AlwaysAutoResize
			| ImGuiWindowFlags_NoMove
			| ImGuiWindowFlags_NoBackground
			| ImGuiWindowFlags_NoDecoration;

		auto* vp = ImGui::GetMainViewport();

		const float width = 600.0f;
		ImGui::SetNextWindowPos( vp->Pos + ImVec2( vp->Size.x / 2, 50 ) - ImVec2( width / 2, 0 ), ImGuiCond_Always );
		ImGui::SetNextWindowSize( ImVec2( width, 0 ), ImGuiCond_Always );

		auto scope = ImGuiEx::Begin( "##CommandWindow", nullptr, flags );
		if( !scope )
			return;

		const auto text_flags = ImGuiInputTextFlags_EnterReturnsTrue;

		ImGui::PushItemWidth( width );
		if( ImGui::InputText( "##Command", input, 1024, text_flags ) )
		{
			std::string_view cmd( input );	

			if( cmd == "demo" )
			{
				show_demo = true;
			}
			else try
			{
				py::exec( input );
			}
			catch( const py::error_already_set& e )
			{
				Log::Error( e.what() );
			}

			commandline = false;
		}
		ImGui::PopItemWidth();

		if( new_input )
			ImGui::SetKeyboardFocusHere();
	}

	bool PaintProgram::MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
	{
		if( msg == WM_KEYDOWN )
		{
			if( ImGui::IsKeyDown( VK_CONTROL ) && ImGui::IsKeyDown( VK_SHIFT ) )
			{
				switch( wParam )
				{
				case 'S':
					SaveAs();
					break;
				case 'Q':
					Quit();
					break;
				case 'Z':
					Redo();
					break;
				}
			}
			else if( ImGui::IsKeyDown( VK_CONTROL ) )
			{
				switch( wParam )
				{
				case 'N':
					New();
					break;
				case 'S':
					Save();
					break;
				case 'R':
					ResizeCanvas();
					break;
				case 'O':
					Open();
					break;
				case 'Q':
					Close();
					break;
				case 'Z':
					Undo();
					break;
				case 'F':
					ResetCanvasView();
					break;
				}
			}
		}
		else if( msg == WM_COPYDATA )
		{
			COPYDATASTRUCT* pcds;
			pcds = (COPYDATASTRUCT*)lParam;

			if( pcds->dwData == 1 )
				HandleArguments( (char*)pcds->lpData );

			return 0;
		}

		return false;
	}

	PaintProgram* GetProgram()
	{
		return program;
	}

	PaintInstance* GetInstance()
	{
		return program ? program->GetInstance() : nullptr;
	}
}

PYBIND11_EMBEDDED_MODULE( PaintContext, mod )
{
	Python::Modules::PaintModule::Initialise( mod );
}