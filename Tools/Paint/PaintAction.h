#pragma once

#include <functional>

namespace Paint
{
	class Action
	{
	public:
		using Callback_t = std::function<void()>;

		Action( Callback_t do_action, Callback_t undo_action )
			: do_action( do_action ), undo_action( undo_action ) {}

		void Do();
		void Undo();

	private:
		Callback_t do_action, undo_action;
		bool done = false;
	};
}