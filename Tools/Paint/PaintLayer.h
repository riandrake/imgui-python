#pragma once

#include <vector>
#include <memory>

#include <nlohmann/json.hpp>

#include "PaintAction.h"
#include "PaintShapes.h"
#include <Common/ImGui/DragOperation.h>

using json = nlohmann::json;

namespace Paint
{
	class PaintInstance;
	class PaintPalette;

	class PaintLayer
	{
	public:
		PaintLayer( const PaintPalette& palette, const PaintInstance& instance );
		PaintLayer( const PaintLayer& other );

		void Update();
		void Render( const ImVec2& offset );

		void AddShape( const Shape& shape );

		void RemoveShape() { shapes.pop_back(); }

		bool IsVisible() const { return visible; }
		void SetVisible( const bool visible ) { this->visible = visible; }

		const std::string& GetName() const { return name; }
		void SetName( const std::string& name ) { this->name = name; }

		float GetOpacity() const { return opacity; }
		void SetOpacity( const float opacity ) { this->opacity = opacity; }

		unsigned GetId() const { return id; }

		void MergeWith( const std::unique_ptr<PaintLayer>& layer );

		json Serialise();
		void Deserialise( const json& layer );

	private:
		void DragStarted();
		void DragChanged();
		void DragFinished();

		inline static unsigned next_id = 0;

		std::vector<std::unique_ptr<Shape>> shapes;

		std::string name;
		const PaintPalette& palette;
		const PaintInstance& instance;

		unsigned id = 0;
		float opacity = 1.0f;
		bool visible = true;
	};
}