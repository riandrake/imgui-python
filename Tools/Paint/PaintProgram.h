#pragma once

#include <dxgi1_4.h>
#include <string>

#include "PaintPalette.h"
#include "PaintInstance.h"

#define PROGRAM_NAME "Paint for Programmers"
#define PROGRAM_VERSION "0.0"

#include <pybind11/complex.h>
#include <pybind11/functional.h>
#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/embed.h>

#include <Common/ImGui/Popup.h>

#include <Common/Program/BaseApp.h>

namespace py = pybind11;

namespace Paint
{
	class PaintProgram : public App::Base
	{
	public:
		virtual const char* ProgramName() const { return "Paint"; }
		virtual const char* WindowName() const { return "Paint"; }

		void HandleArguments( const std::string_view& args );

		virtual bool MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) override;
		virtual void OnRender() override;
		virtual void OnInitialise( const std::string_view& args ) override;
		virtual void OnDestroy() override;

		void New();
		void Open( const std::string_view& filename );
		void Open();
		void Save();
		void SaveAs();
		void SaveAs( const std::string& filename );
		void Close();
		void Quit();
		void Undo();
		void Redo();

		void SetCanvasPosition( const ImVec2& pos );
		void SetCanvasZoom( const float zoom );

		void ResetCanvasView();
		void ResizeCanvas();

		bool UserQuit() const { return quit; }

		PaintInstance* GetInstance() { return instance.get(); }
		PaintPalette& GetPalette() { return palette; }

	private:
		void RenderMainMenuBar();
		void RenderCommandLine();

		std::unique_ptr<py::scoped_interpreter> interpreter;
		std::unique_ptr<PaintInstance> instance;
		ImGuiEx::PopupHandler popup_handler;
		PaintPalette palette;
		char input[1024];
		bool quit = false;
		bool commandline = false;
		bool show_demo = false;
	};

	PaintProgram* GetProgram();
	PaintInstance* GetInstance();
}