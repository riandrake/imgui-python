#include "PaintShapes.h"

#include <Common/Utility/Exception.h>

namespace Paint
{
	Shape::Shape( const Shapes type )
		: type( type )
	{

	}

	Shape::Shape( const Shapes type, const json& shape )
		: type( type )
	{
		const auto& s = shape["start"];
		start.x = s[0].get<float>();
		start.y = s[1].get<float>();

		colour = shape["colour"].get<unsigned>();
		weight = shape["weight"].get<float>();
	}

	unsigned Shape::GetColour( float opacity ) const
	{
		const unsigned a = static_cast<unsigned char>( ( colour >> 24 ) * opacity ) << 24;
		if( a == 0 )
			return 0;

		return a | ( colour & 0x00ffffff );
	}

	std::unique_ptr<Paint::Shape> Shape::make_shape( const Shapes shape )
	{
		switch( shape )
		{
		case Shapes::Text: return std::make_unique<Text>();
		case Shapes::Line: return std::make_unique<Line>();
		case Shapes::Circle: return std::make_unique<Circle>();
		case Shapes::Rectangle: return std::make_unique<Rectangle>();
		case Shapes::Brush: return std::make_unique<Brush>();
		}

		throw RuntimeError( "Shape not implemented: {}", shape );
	}

	std::unique_ptr<Paint::Shape> Shape::clone() const
	{
		switch( type )
		{
		case Shapes::Text: return std::make_unique<Text>( *As<Text>() );
		case Shapes::Line: return std::make_unique<Line>( *As<Line>() );
		case Shapes::Circle: return std::make_unique<Circle>( *As<Circle>() );
		case Shapes::Rectangle: return std::make_unique<Rectangle>( *As<Rectangle>() );
		case Shapes::Brush: return std::make_unique<Brush>( *As<Brush>() );
		}

		throw RuntimeError( "Shape not implemented: {}", type );
	}

	json Shape::Serialise()
	{
		json out;
		out["type"] = ShapeToString( type );
		out["start"] = { start.x, start.y };
		out["colour"] = colour;
		out["weight"] = weight;
		return out;
	}

	std::unique_ptr<Paint::Shape> Shape::Deserialise( const json& shape )
	{
		switch( StringToShape( shape["type"] ) )
		{
		case Shapes::Line: return std::make_unique<Line>( shape );
		case Shapes::Circle: return std::make_unique<Circle>( shape );
		case Shapes::Rectangle: return std::make_unique<Rectangle>( shape );
		case Shapes::Brush: return std::make_unique<Brush>( shape );
		case Shapes::Text: return std::make_unique<Text>( shape );
		}

		throw RuntimeError( "Failed to deserialise shape: {}", shape["type"] );
	}

	const char* Shape::ShapeToString( const Shapes shape )
	{
		switch( shape )
		{
		case Shapes::Line: return "Line";
		case Shapes::Circle: return "Circle";
		case Shapes::Rectangle: return "Rectangle";
		case Shapes::Brush: return "Brush";
		case Shapes::Text: return "Text";
		}

		throw RuntimeError( "Shape not implemented: {}", (int)shape );
	}

	Shapes Shape::StringToShape( const std::string& shape )
	{
		for( int i = 0; i < (int)Shapes::NumShapes; ++i )
		{
			const auto shape_id = static_cast<Shapes>( i );
			if( ShapeToString( shape_id ) == shape )
				return shape_id;
		}

		throw RuntimeError( "Not a shape: {}", shape );
	}

	Text::Text( const json& text )
		: Shape( text )
	{
		this->text = text["text"];
	}

	void Text::Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity )
	{
		const auto p = offset + start * zoom;
		draw_list->AddText( nullptr, 2 * weight * zoom, p, GetColour( opacity ), text.c_str(), text.c_str() + text.size() );
	}

	json Text::Serialise()
	{
		json obj = Shape::Serialise();
		obj["text"] = text;
		return obj;
	}

	Line::Line( const json& line )
		: Shape( GetType(), line )
	{
		const auto& e = line["end"];
		end.x = e[0].get<float>();
		end.y = e[1].get<float>();
	}

	json Line::Serialise()
	{
		json out = Shape::Serialise();
		out["end"] = { end.x, end.y };
		return out;
	}

	void Line::Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity )
	{
		const auto b = offset + start * zoom;
		const auto e = offset + end * zoom;

		draw_list->AddLine( b, e, GetColour( opacity ), weight * zoom );
	}

	Circle::Circle( const json& circle )
		: Shape( GetType(), circle )
	{
		radius = circle["radius"].get<float>();
		filled = circle["filled"].get<bool>();
	}

	json Circle::Serialise()
	{
		json out = Shape::Serialise();
		out["radius"] = radius;
		out["filled"] = filled;
		return out;
	}

	void Circle::Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity )
	{
		const auto b = offset + start * zoom;
		const auto r = radius * zoom;

		if( filled ) draw_list->AddCircleFilled( b, r, GetColour( opacity ), static_cast<int>( r ) * 2 );
		else         draw_list->AddCircle( b, r, GetColour( opacity ), static_cast<int>( r ) * 2, weight * zoom );
	}

	Rectangle::Rectangle( const json& rectangle )
		: Shape( GetType(), rectangle )
	{
		filled = rectangle["filled"].get<bool>();

		const auto& e = rectangle["end"];
		end.x = e[0].get<float>();
		end.y = e[1].get<float>();
	}

	json Rectangle::Serialise()
	{
		json out = Shape::Serialise();
		out["end"] = { end.x, end.y };
		out["filled"] = filled;
		return out;
	}

	void Rectangle::Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity )
	{
		const auto b = offset + start * zoom;
		const auto e = offset + end * zoom;

		if( filled ) draw_list->AddRectFilled( b, e, GetColour( opacity ) );
		else         draw_list->AddRect( b, e, GetColour( opacity ), 0, 0, weight * zoom );
	}

	Brush::Brush( const json& line )
		: Shape( GetType(), line )
	{
		for( auto& point : line["points"] )
			points.emplace_back( point[0].get<float>(), point[1].get<float>() );
	}

	void Brush::Render( ImDrawList* draw_list, const ImVec2& offset, float zoom, float opacity )
	{
		const auto colour = GetColour( opacity );

		ImVec2 last = start;
		for( auto& point : points )
		{
			const auto A = offset + last * zoom;
			const auto B = offset + point * zoom;

			draw_list->AddCircleFilled( A, weight * 0.5f * zoom, colour, (int)( weight * zoom ) );
			draw_list->AddLine( A, B, colour, weight * zoom );
			last = point;
		}

		draw_list->AddCircleFilled( offset + last * zoom, weight * 0.5f * zoom, colour, (int)( weight * zoom ) );
	}

	json Brush::Serialise()
	{
		auto object = Shape::Serialise();

		json points_o = json::array();
		for( auto& point : points )
			points_o.emplace_back( std::vector<float>{ point.x, point.y } );
		object["points"] = std::move( points_o );
		return object;
	}
}