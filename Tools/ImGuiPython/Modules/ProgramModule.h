#pragma once

#include <Common/Python/Module.h>

namespace Python::Modules::ProgramModule
{
	void Initialise( pybind11::module& mod );
}
