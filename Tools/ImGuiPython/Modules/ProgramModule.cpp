#include "ProgramModule.h"

#include <pybind11/pybind11.h>

#include <Common/ImGui/Modules/ImGuiModule.h>
#include <Common/Program/BaseApp.h>
#include <Common/Program/ProgramMain.h>

namespace py = pybind11;

namespace App
{
	class Program : public Base
	{
	public:
		virtual const char* ProgramName() const
			py_implements( const char*, Base, ProgramName );
		virtual const char* WindowName() const
			py_implements( const char*, Base, WindowName );
		virtual void OnInitialise( const std::string_view& args )
			py_implements( void, Base, OnInitialise, args );
		virtual void OnDestroy()
			py_implements( void, Base, OnDestroy );
		virtual void OnFrameMove( const float elapsed_time )
			py_implements( void, Base, OnFrameMove, elapsed_time );
		virtual void OnRender()
			py_implements( void, Base, OnRender );
		virtual bool MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
			py_implements( bool, Base, MsgProc, hWnd, msg, wParam, lParam );
	};
}

int main( App::Base* program )
{
	std::string args;
	return Main::DoMain( *program, nullptr, nullptr, args.data(), 0, false );
}

PYBIND11_MODULE( ImGui, mod )
{
	using namespace App;

	py_class2( Base, Program )
	.py_method2( "program_name", Base, ProgramName )
	.py_method2( "window_name", Base, WindowName )
	.py_method2( "initialise", Base, OnInitialise )
	.py_method2( "destroy", Base, OnDestroy )
	.py_method2( "frame_move", Base, OnFrameMove )
	.py_method2( "render", Base, OnRender )
	.py_method2( "msg_proc", Base, MsgProc );

	mod
	.py_func( main )
	.py_func( exit );

	Python::Modules::ImGuiModule::Initialise( mod );
}