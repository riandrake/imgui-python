# USER GUIDE

## Cloning Repository:

	[Command Line Git for Windows](https://gitforwindows.org/)
		> git clone https://bitbucket.org/riandrake/imgui-python.git imgui-python

	[SourceTree GUI](https://www.sourcetreeapp.com/)
		clone using this link: https://bitbucket.org/riandrake/imgui-python.git
	
## Implementing
	To start making apps the only thing you need is the ImGui.pyd module,
	and your own .py application module. See apps/example_application.py for
	an example on how to start.

	Documentation doesn't really exist, but outside of example_application.py
	the best place to look is at https://github.com/ocornut/imgui or in
	imgui_demo.cpp.

# PROGRAMMING GUIDE
	Required environment variable to compile:
		PYTHONHOME: C:/Program Files (x86)/Python37-32 (or similar)

	Unfortunately, because Python doesn't support operator overloading some
	functions needed to be reimplemented /handled manually. See imgui_module.cpp
	for an example on how to use those.